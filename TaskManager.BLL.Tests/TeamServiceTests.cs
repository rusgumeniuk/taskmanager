﻿using AutoMapper;

using FakeItEasy;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Services;
using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;
using TaskManager.DAL.UnitOfWorks;

using Xunit;

namespace TaskManager.BLL.Tests
{
    public class TeamServiceTests : IDisposable
    {
        private readonly TeamService _teamService;
        private readonly TaskUserProjectTeamUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IMapper _mapper;

        public TeamServiceTests()
        {
            _mapper = A.Fake<IMapper>();

            _teamRepository = A.Fake<ITeamRepository>();
            _userRepository = A.Fake<IUserRepository>();
            _taskRepository = A.Fake<ITaskRepository>();
            _projectRepository = A.Fake<IProjectRepository>();

            _unitOfWork = new TaskUserProjectTeamUnitOfWork(_projectRepository, _taskRepository, _userRepository, _teamRepository);
            _teamService = new TeamService(_unitOfWork, _mapper);
        }

        public void Dispose()
        {
            _teamService.Dispose();
            _unitOfWork.Dispose();
        }

        [Fact]
        public async Task GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTime_WhenNoOneMemberOlder_ReturnsEmptyCollection()
        {
            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(new[] { new User() { TeamId = 1, BirthDay = new DateTime(2020, 2, 2) } });

            A.CallTo(() => _teamRepository.RetrieveAllAsync(null))
                .Returns(new List<Team>() { new Team() { Id = 1 } });

            var teams = await _teamService.GetAllAsync();
            var result = await _teamService.GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTimeAsync();

            Assert.NotEmpty(teams);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTime_WhenNoOneTeam_ReturnsEmptyCollection()
        {
            var result = await _teamService.GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTimeAsync();

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTime_When2MemberOlder_ReturnsNotEmpty()
        {
            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(new User[]
                {
                    new User() { TeamId = 1, BirthDay = new DateTime(2008, 2, 2), RegisteredAt = new DateTime(2020, 2, 2)},
                    new User() { TeamId = 1, BirthDay = new DateTime(2002, 2, 2), RegisteredAt = new DateTime(2020, 3, 2)},
                });

            A.CallTo(() => _teamRepository.RetrieveAllAsync(null))
                .Returns(new List<Team>() { new Team() { Id = 1 } });

            var result = await _teamService.GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTimeAsync();

            Assert.NotEmpty(result);
            Assert.Equal(2, result.ElementAt(0).Members.Count());
            Assert.True(result.ElementAt(0).Members.ElementAt(0).RegisteredAt >=
                        result.ElementAt(0).Members.ElementAt(1).RegisteredAt);
        }
    }
}
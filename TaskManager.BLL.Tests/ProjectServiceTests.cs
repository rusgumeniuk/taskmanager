﻿using AutoMapper;

using FakeItEasy;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Exceptions;
using TaskManager.BLL.Services;
using TaskManager.Common.DTO;
using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;
using TaskManager.DAL.UnitOfWorks;

using Xunit;

namespace TaskManager.BLL.Tests
{
    public class ProjectServiceTests : IDisposable
    {
        #region Set up / tear down

        private readonly ProjectService _projectService;
        private readonly TaskUserProjectTeamUnitOfWork _unitOfWork;
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IMapper _mapper;

        public ProjectServiceTests()
        {
            _mapper = A.Fake<IMapper>();

            _projectRepository = A.Fake<IProjectRepository>();
            _userRepository = A.Fake<IUserRepository>();
            _taskRepository = A.Fake<ITaskRepository>();
            _teamRepository = A.Fake<ITeamRepository>();

            _unitOfWork = new TaskUserProjectTeamUnitOfWork(_projectRepository, _taskRepository, _userRepository, _teamRepository);
            _projectService = new ProjectService(_mapper, _unitOfWork);
        }

        public void Dispose()
        {
            _projectService.Dispose();
            _unitOfWork.Dispose();
        }

        #endregion Set up / tear down

        #region GetNumberOfTasksOfUserAsync

        [Fact]
        public async Task GetNumberOfTasksOfUser_WhenNotExistingId_ThrowsNotFound()
        {
            const int id = 13;

            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(false);

            await Assert.ThrowsAsync<NotFoundException>(() => _projectService.GetNumberOfTasksOfUserAsync(id));
        }

        [Fact]
        public async Task GetNumberOfTasksOfUser_WhenUserHasNoProject_ReturnsEmpty()
        {
            const int id = 13;

            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new[] { new ProjectTask() });

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new Project[0]);

            var result = await _projectService.GetNumberOfTasksOfUserAsync(id);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetNumberOfTasksOfUser_WhenNoOneTask_ReturnsEmpty()
        {
            const int id = 23;

            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new ProjectTask[0]);

            var project = new Project() { Id = 32, AuthorId = id };
            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new[] { project });

            A.CallTo(() => _mapper.Map<ProjectDTO>(project))
                .Returns(new ProjectDTO() { Id = project.Id, AuthorId = project.AuthorId });

            var result = await _projectService.GetNumberOfTasksOfUserAsync(id);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetNumberOfTasksOfUser_When2ProjectsAnd5Tasks_ReturnsNotEmpty()
        {
            const int id = 11;

            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new ProjectTask[0]);

            var firstProject = new Project() { Id = 23, AuthorId = id };
            var secondProject = new Project() { Id = 22, AuthorId = 10 };
            var thirdProject = new Project() { Id = 32, AuthorId = id };

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new[] { firstProject, secondProject, thirdProject });

            A.CallTo(() => _mapper.Map<ProjectDTO>(firstProject))
                .Returns(new ProjectDTO() { Id = firstProject.Id, AuthorId = firstProject.AuthorId });

            A.CallTo(() => _mapper.Map<ProjectDTO>(thirdProject))
                .Returns(new ProjectDTO() { Id = thirdProject.Id, AuthorId = thirdProject.AuthorId });

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, ProjectId = firstProject.Id},
                new ProjectTask() {Id = 2, ProjectId = secondProject.Id},
                new ProjectTask() {Id = 3, ProjectId = firstProject.Id},
                new ProjectTask() {Id = 4, ProjectId = thirdProject.Id},
                new ProjectTask() {Id = 5, ProjectId = firstProject.Id},
                new ProjectTask() {Id = 6, ProjectId = thirdProject.Id}
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            var result = await _projectService.GetNumberOfTasksOfUserAsync(id);

            Assert.Equal(2, result.Count);

            Assert.Equal(result[result.Keys.First(key => key.Id == firstProject.Id)],
                tasks.Count(task => task.ProjectId == firstProject.Id));

            Assert.Equal(result[result.Keys.First(key => key.Id == thirdProject.Id)],
                tasks.Count(task => task.ProjectId == thirdProject.Id));
        }

        #endregion GetNumberOfTasksOfUserAsync

        #region GetProjectInfoAsync

        [Fact]
        public async Task GetProjectInfo_WhenNoOneProject_ReturnsEmpty()
        {
            var result = await _projectService.GetProjectInfoAsync();

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetProjectInfo_When1ProjectAndNoOneTask_ReturnsProjectAndNullInTasks()
        {
            var project = new Project() { Id = 66, TeamId = 77 };

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new[] { project });

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new List<ProjectTask>());

            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(new List<User>());

            A.CallTo(() => _mapper.Map<ProjectDTO>(project))
                .Returns(new ProjectDTO() { Id = project.Id, TeamId = project.TeamId });

            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(null))
                .Returns(null);

            var result = await _projectService.GetProjectInfoAsync();

            Assert.NotEmpty(result);
            Assert.Null(result.ElementAt(0).TaskWithLongestDescription);
            Assert.Null(result.ElementAt(0).TaskWithShortestName);
        }

        [Fact]
        public async Task GetProjectInfo_WhenProjectHas2Tasks_ReturnsProjectAndNotNullTasks()
        {
            var project = new Project() { Id = 66, TeamId = 77 };

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new[] { project });

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, ProjectId = project.Id, Name = "n", Description = "1"},
                new ProjectTask() {Id = 2, ProjectId = project.Id, Name = "na", Description = "12"},
                new ProjectTask() {Id = 3, ProjectId = project.Id, Name = "nam", Description = "123"},
                new ProjectTask() {Id = 4, ProjectId = project.Id, Name = "name", Description = "1234"},
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(new List<User>());

            A.CallTo(() => _mapper.Map<ProjectDTO>(project))
                .Returns(new ProjectDTO() { Id = project.Id, TeamId = project.TeamId });

            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(null))
                .Returns(null);

            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(tasks[0]))
                .Returns(new ProjectTaskDTO() { Id = tasks[0].Id, Name = tasks[0].Name });

            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(tasks[3]))
                .Returns(new ProjectTaskDTO() { Id = tasks[3].Id, Description = tasks[3].Description });

            var result = await _projectService.GetProjectInfoAsync();

            Assert.Equal(tasks[3].Description, result.ElementAt(0).TaskWithLongestDescription.Description);
            Assert.Equal(tasks[3].Id, result.ElementAt(0).TaskWithLongestDescription.Id);

            Assert.Equal(tasks[0].Name, result.ElementAt(0).TaskWithShortestName.Name);
            Assert.Equal(tasks[0].Id, result.ElementAt(0).TaskWithShortestName.Id);
        }

        [Fact]
        public async Task GetProjectInfo_WhenProjectDescriptionMoreThan20_ReturnsCountOfTeamMember()
        {
            var project = new Project() { Id = 66, TeamId = 77, Description = "123456789012345678901234567890" };

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new[] { project });

            var users = new[]
            {
                new User() {Id = 1, TeamId = project.TeamId + 1},
                new User() {Id = 2, TeamId = project.TeamId},
                new User() {Id = 3, TeamId = null},
                new User() {Id = 4, TeamId = project.TeamId},
                new User() {Id = 5, TeamId = project.TeamId},
            };

            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(users);

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, ProjectId = project.Id},
                new ProjectTask() {Id = 2, ProjectId = 20},
                new ProjectTask() {Id = 3, ProjectId = project.Id},
                new ProjectTask() {Id = 4, ProjectId = 30},
                new ProjectTask() {Id = 5, ProjectId = project.Id},
                new ProjectTask() {Id = 6, ProjectId = project.Id}
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            A.CallTo(() => _mapper.Map<ProjectDTO>(project))
                .Returns(new ProjectDTO() { Id = project.Id, TeamId = project.TeamId });

            var result = await _projectService.GetProjectInfoAsync();

            Assert.Equal(users.Count(user => user.TeamId == project.TeamId), result.ElementAt(0).CountOfStaff);
        }

        [Fact]
        public async Task GetProjectInfo_WhenProjectHas2Tasks_ReturnsCountOfTeamMember()
        {
            var project = new Project() { Id = 66, TeamId = 77, Description = "Word" };

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new[] { project });

            var users = new[]
            {
                new User() {Id = 1, TeamId = project.TeamId + 1},
                new User() {Id = 2, TeamId = project.TeamId},
                new User() {Id = 3, TeamId = null},
                new User() {Id = 4, TeamId = project.TeamId},
                new User() {Id = 5, TeamId = project.TeamId},
            };

            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(users);

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, ProjectId = project.Id},
                new ProjectTask() {Id = 2, ProjectId = 20},
                new ProjectTask() {Id = 3, ProjectId = project.Id}
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            A.CallTo(() => _mapper.Map<ProjectDTO>(project))
                .Returns(new ProjectDTO() { Id = project.Id, TeamId = project.TeamId });

            var result = await _projectService.GetProjectInfoAsync();

            Assert.Equal(users.Count(user => user.TeamId == project.TeamId), result.ElementAt(0).CountOfStaff);
        }

        [Fact]
        public async Task GetProjectInfo_When3ProjectTasksAndShortDescription_Returns0AsCountOfTeamMember()
        {
            var project = new Project() { Id = 66, TeamId = 77, Description = "Word" };

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new[] { project });

            var users = new[]
            {
                new User() {Id = 1, TeamId = project.TeamId + 1},
                new User() {Id = 2, TeamId = project.TeamId},
                new User() {Id = 3, TeamId = null},
                new User() {Id = 4, TeamId = project.TeamId},
                new User() {Id = 5, TeamId = project.TeamId},
            };

            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(users);

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, ProjectId = project.Id},
                new ProjectTask() {Id = 2, ProjectId = project.Id},
                new ProjectTask() {Id = 3, ProjectId = project.Id}
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            A.CallTo(() => _mapper.Map<ProjectDTO>(project))
                .Returns(new ProjectDTO() { Id = project.Id, TeamId = project.TeamId });

            var result = await _projectService.GetProjectInfoAsync();

            Assert.Equal(0, result.ElementAt(0).CountOfStaff);
        }

        #endregion GetProjectInfoAsync
    }
}
﻿using AutoMapper;

using FakeItEasy;

using System;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Exceptions;
using TaskManager.BLL.Services;
using TaskManager.Common.DTO;
using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;
using TaskManager.DAL.UnitOfWorks;

using Xunit;

namespace TaskManager.BLL.Tests
{
    public class UserServiceTests : IDisposable
    {
        #region Set up / tear down

        private readonly UserService _userService;
        private readonly TaskUserProjectTeamUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IMapper _mapper;

        public UserServiceTests()
        {
            _mapper = A.Fake<IMapper>();

            _userRepository = A.Fake<IUserRepository>();
            _teamRepository = A.Fake<ITeamRepository>();
            _taskRepository = A.Fake<ITaskRepository>();
            _projectRepository = A.Fake<IProjectRepository>();

            _unitOfWork = new TaskUserProjectTeamUnitOfWork(_projectRepository, _taskRepository, _userRepository, _teamRepository);
            _userService = new UserService(_unitOfWork, _mapper);
        }

        public void Dispose()
        {
            _userService.Dispose();
            _unitOfWork.Dispose();
        }

        #endregion Set up / tear down

        #region AddAsync

        [Fact]
        public async Task AddAsync_WhenNull_ReturnsArgumentNullException()
        {
            await Assert.ThrowsAnyAsync<Exception>(() => _userService.AddAsync(null));
            A.CallTo(() => _userRepository.CreateAsync(A<User>._)).MustNotHaveHappened();
        }

        [Fact]
        public async Task AddAsync_WhenNotExistingTeamID_ReturnsInvalidOperationException()
        {
            var user = new UserCreateDTO() { TeamId = 100 };

            A.CallTo(() => _mapper.Map<User>(user)).Returns(new User() { TeamId = 100 });
            A.CallTo(() => _teamRepository.ExistAsync(user.TeamId.Value)).Returns(false);

            await Assert.ThrowsAnyAsync<InvalidOperationException>(() => _userService.AddAsync(user));
            A.CallTo(() => _userRepository.CreateAsync(A<User>._)).MustNotHaveHappened();
        }

        [Fact]
        public async Task AddAsync_WhenValidUserWithExistingTeam_ReturnsCreatedUser()
        {
            var newUser = new UserCreateDTO() { TeamId = 200 };

            A.CallTo(() => _mapper.Map<User>(newUser)).Returns(new User() { TeamId = 200 });
            A.CallTo(() => _teamRepository.ExistAsync(newUser.TeamId.Value)).Returns(true);

            await _userService.AddAsync(newUser);
            A.CallTo(() => _userRepository.CreateAsync(A<User>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task AddAsync_WhenValidUserWithoutTeam_ReturnsCreatedUser()
        {
            var newUser = new UserCreateDTO()
            {
                FirstName = "Best",
                LastName = "User",
                BirthDay = new DateTime(),
                Email = "s@s.c"
            };

            await _userService.AddAsync(newUser);

            A.CallTo(() => _userRepository.CreateAsync(A<User>._)).MustHaveHappenedOnceExactly();
        }

        #endregion AddAsync

        #region UpdateAsync (change team)

        [Fact]
        public async Task UpdateAsync_WhenDtoIsNull_ThrowsArgumentNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _userService.UpdateAsync(null));
        }

        [Fact]
        public async Task UpdateAsync_WhenNotExistingUserId_ThrowsNotFound()
        {
            var dto = new UserDTO() { Id = 19 };

            A.CallTo(() => _userRepository.ExistAsync(dto.Id))
                .Returns(false);

            await Assert.ThrowsAsync<NotFoundException>(() => _userService.UpdateAsync(dto));
        }

        [Fact]
        public async Task UpdateAsync_WhenNewTeamNotExist_ThrowsInvalidOperation()
        {
            var entityDto = new UserDTO() { Id = 84, TeamId = 10 };
            var entity = new User() { Id = entityDto.Id, TeamId = entityDto.TeamId };

            var oldEntity = new User() { Id = entityDto.Id, TeamId = 5 };
            var oldEntityDto = new UserDTO() { Id = entityDto.Id, TeamId = oldEntity.TeamId };

            A.CallTo(() => _userRepository.ExistAsync(entityDto.Id))
                .Returns(true);

            A.CallTo(() => _mapper.Map<User>(entityDto))
                .Returns(entity);

            A.CallTo(() => _userRepository.RetrieveAsync(entityDto.Id, true))
                .Returns(oldEntity);
            A.CallTo(() => _mapper.Map<UserDTO>(oldEntity))
                .Returns(oldEntityDto);
            A.CallTo(() => _mapper.Map<User>(oldEntityDto))
                .Returns(oldEntity);

            A.CallTo(() => _teamRepository.ExistAsync(entity.TeamId.Value))
                .Returns(false);

            await Assert.ThrowsAsync<InvalidOperationException>(() => _userService.UpdateAsync(entityDto));
        }

        [Fact]
        public async Task UpdateAsync_WhenNewTeamExist_ReturnsUpdated()
        {
            var entityDto = new UserDTO() { Id = 84, TeamId = 10 };
            var entity = new User() { Id = entityDto.Id, TeamId = entityDto.TeamId };

            var oldEntity = new User() { Id = entityDto.Id, TeamId = 5 };
            var oldEntityDto = new UserDTO() { Id = entityDto.Id, TeamId = oldEntity.TeamId };

            A.CallTo(() => _userRepository.ExistAsync(entityDto.Id))
                .Returns(true);

            A.CallTo(() => _mapper.Map<User>(entityDto))
                .Returns(entity);

            A.CallTo(() => _userRepository.RetrieveAsync(entityDto.Id, true))
                .Returns(oldEntity);
            A.CallTo(() => _mapper.Map<UserDTO>(oldEntity))
                .Returns(oldEntityDto);
            A.CallTo(() => _mapper.Map<User>(oldEntityDto))
                .Returns(oldEntity);

            A.CallTo(() => _teamRepository.ExistAsync(entity.TeamId.Value))
                .Returns(true);

            await _userService.UpdateAsync(entityDto);

            A.CallTo(() => _userRepository.UpdateAsync(entity)).MustHaveHappenedOnceExactly();
            A.CallTo(() => _userRepository.SaveChangesAsync()).MustHaveHappenedOnceExactly();
        }

        #endregion UpdateAsync (change team)

        #region GetUsersOrderedByFirstNameSortedByTaskNameAsync

        [Fact]
        public async Task GetUsersOrderedByFirstNameSortedByTaskName_WhenNoOneUsers_ReturnsEmpty()
        {
            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(new User[0]);
            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new ProjectTask[] { new ProjectTask() { PerformerId = 2 } });

            var result = await _userService.GetUsersOrderedByFirstNameSortedByTaskNameAsync();

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetUsersOrderedByFirstNameSortedByTaskName_WhenNoOneTask_ReturnsNotEmpty()
        {
            var user = new User() { Id = 13 };
            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(new[] { user });
            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new ProjectTask[0]);

            A.CallTo(() => _mapper.Map<UserDTO>(user))
                .Returns(new UserDTO() { Id = user.Id });

            var result = await _userService.GetUsersOrderedByFirstNameSortedByTaskNameAsync();

            Assert.NotEmpty(result);
            Assert.Equal(user.Id, result.ElementAt(0).User.Id);
            Assert.Empty(result.ElementAt(0).Tasks);
        }

        [Fact]
        public async Task GetUsersOrderedByFirstNameSortedByTaskName_When2UserAnd3Tasks_ReturnsNotEmptyAndOrdered()
        {
            var firstUser = new User() { Id = 19, FirstName = "Tom" };
            var firstUserDto = new UserDTO() { Id = firstUser.Id, FirstName = "Tom" };

            var secondUser = new User() { Id = 84, FirstName = "Adam" };
            var secondUserDto = new UserDTO() { Id = secondUser.Id, FirstName = "Adam" };

            A.CallTo(() => _userRepository.RetrieveAllAsync(null))
                .Returns(new[] { firstUser, secondUser });

            var tasks = new[]
            {
                new ProjectTask() {Name = "short name", PerformerId = firstUser.Id},
                new ProjectTask() {Name = "2 long", PerformerId = secondUser.Id},
                new ProjectTask() {Name = "some name", PerformerId = firstUser.Id + secondUser.Id},
                new ProjectTask() {Name = "somme", PerformerId = secondUser.Id + (firstUser.Id * 2)},
                new ProjectTask() {Name = "looong name", PerformerId = firstUser.Id},
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            A.CallTo(() => _mapper.Map<UserDTO>(firstUser))
                .Returns(firstUserDto);

            A.CallTo(() => _mapper.Map<UserDTO>(secondUser))
                .Returns(secondUserDto);

            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(tasks[4]))
                .Returns(new ProjectTaskDTO() { Name = tasks[4].Name });

            var result = await _userService.GetUsersOrderedByFirstNameSortedByTaskNameAsync();

            Assert.Equal(2, result.Count());

            Assert.Contains(result, item => item.User.Id == secondUser.Id);

            Assert.Equal(tasks.Count(task => task.PerformerId == firstUser.Id),
                result.First(item => item.User.Id == firstUser.Id).Tasks.Count());

            Assert.Equal(3, result.Sum(item => item.Tasks.Count()));

            Assert.True(result.ElementAt(0).User.FirstName.Equals(secondUser.FirstName));

            Assert.True(result.ElementAt(1).Tasks.ElementAt(0).Name.Equals(tasks[4].Name));
        }

        #endregion GetUsersOrderedByFirstNameSortedByTaskNameAsync
    }
}
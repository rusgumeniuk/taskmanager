﻿using AutoMapper;

using FakeItEasy;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Exceptions;
using TaskManager.BLL.Services;
using TaskManager.Common.DTO;
using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;
using TaskManager.DAL.UnitOfWorks;

using Xunit;

namespace TaskManager.BLL.Tests
{
    public class ProjectTaskServiceTests : IDisposable
    {
        #region Set up / tear down

        private readonly ProjectTaskService _taskService;
        private readonly TaskUserProjectTeamUnitOfWork _unitOfWork;
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IMapper _mapper;

        public ProjectTaskServiceTests()
        {
            _mapper = A.Fake<IMapper>();

            _projectRepository = A.Fake<IProjectRepository>();
            _userRepository = A.Fake<IUserRepository>();
            _taskRepository = A.Fake<ITaskRepository>();
            _teamRepository = A.Fake<ITeamRepository>();

            _unitOfWork = new TaskUserProjectTeamUnitOfWork(_projectRepository, _taskRepository, _userRepository, _teamRepository);
            _taskService = new ProjectTaskService(_unitOfWork, _mapper);
        }

        public void Dispose()
        {
            _taskService.Dispose();
            _unitOfWork.Dispose();
        }

        #endregion Set up / tear down

        #region UpdateAsync

        #region Change only task state

        [Theory]
        [InlineData(TaskState.Created)]
        [InlineData(TaskState.Started)]
        public async Task UpdateAsync_WhenTryToSetIncorrectState_ThrowsInvalidOperationException(TaskState newState)
        {
            var entityDto = new ProjectTaskDTO() { Id = 10, State = (int)newState };
            var entity = new ProjectTask() { State = newState };
            A.CallTo(() => _mapper.Map<ProjectTask>(entityDto))
                .Returns(entity);

            var oldEntityDTO = new ProjectTaskDTO() { State = (int)TaskState.Started };
            var oldEntity = new ProjectTask() { State = TaskState.Started };

            A.CallTo(() => _taskRepository.RetrieveAsync(entityDto.Id, true))
                .Returns(oldEntity);
            A.CallTo(() => _mapper.Map<ProjectTask>(oldEntityDTO))
                .Returns(oldEntity);
            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(oldEntity))
                .Returns(oldEntityDTO);

            A.CallTo(() => _userRepository.ExistAsync(A<int>._))
                .Returns(true);
            A.CallTo(() => _projectRepository.ExistAsync(A<int>._))
                .Returns(true);

            await Assert.ThrowsAnyAsync<InvalidOperationException>(() => _taskService.UpdateAsync(entityDto));
        }

        [Fact]
        public async Task UpdateAsync_WhenSetCorrectState_ReturnsSuccess()
        {
            var entityDto = new ProjectTaskDTO() { Id = 10, State = (int)TaskState.Finished };
            var entity = new ProjectTask() { State = TaskState.Finished };
            A.CallTo(() => _mapper.Map<ProjectTask>(entityDto))
                .Returns(entity);

            var oldEntityDTO = new ProjectTaskDTO() { State = (int)TaskState.Started };
            var oldEntity = new ProjectTask() { State = TaskState.Started };

            A.CallTo(() => _taskRepository.RetrieveAsync(entityDto.Id, true))
                .Returns(oldEntity);
            A.CallTo(() => _mapper.Map<ProjectTask>(oldEntityDTO))
                .Returns(oldEntity);
            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(oldEntity))
                .Returns(oldEntityDTO);

            A.CallTo(() => _userRepository.ExistAsync(A<int>._))
                .Returns(true);
            A.CallTo(() => _projectRepository.ExistAsync(A<int>._))
                .Returns(true);

            await _taskService.UpdateAsync(entityDto);

            A.CallTo(() => _taskRepository.UpdateAsync(entity)).MustHaveHappenedOnceExactly();
            A.CallTo(() => _taskRepository.SaveChangesAsync()).MustHaveHappenedOnceExactly();
        }

        #endregion Change only task state

        #endregion UpdateAsync

        #region GetTasksOfUserWithTaskNameLessThan45Async

        [Fact]
        public async Task GetTasksOfUserWithTaskNameLessThan45_WhenNotExistingId_Returns()
        {
            A.CallTo(() => _userRepository.ExistAsync(1)).Returns(false);

            await Assert.ThrowsAnyAsync<NotFoundException>(() => _taskService.GetTasksOfUserWithTaskNameLessThan45Async(1));
        }

        [Fact]
        public async Task GetTasksOfUserWithTaskNameLessThan45_When2Tasks_ReturnsNotEmpty()
        {
            A.CallTo(() => _userRepository.ExistAsync(1)).Returns(true);

            var tasks = new[]
            {
                new ProjectTask() {PerformerId = 1, Name = "aa"},
                new ProjectTask()
                {
                    PerformerId = 1,
                    Name = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
                },
                new ProjectTask() {PerformerId = 1, Name = "cccc"},
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(tasks[0]))
                .Returns(new ProjectTaskDTO() { Name = tasks[0].Name });

            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(tasks[2]))
                .Returns(new ProjectTaskDTO() { Name = tasks[2].Name });

            var result = await _taskService.GetTasksOfUserWithTaskNameLessThan45Async(1);

            Assert.Equal(2, result.Count());
            Assert.Equal(tasks[0].Name, result.ElementAt(0).Name);
        }

        [Fact]
        public async Task GetTasksOfUserWithTaskNameLessThan45_When0Tasks_ReturnsEmpty()
        {
            A.CallTo(() => _userRepository.ExistAsync(1)).Returns(true);

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new List<ProjectTask>());

            var result = await _taskService.GetTasksOfUserWithTaskNameLessThan45Async(1);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetTasksOfUserWithTaskNameLessThan45_WhenAllTaskNamesLonger_ReturnsEmpty()
        {
            A.CallTo(() => _userRepository.ExistAsync(1)).Returns(true);

            var tasks = new[]
            {
                new ProjectTask()
                {
                    PerformerId = 1,
                    Name = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                },
                new ProjectTask()
                {
                    PerformerId = 1,
                    Name = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
                },
                new ProjectTask()
                {
                    PerformerId = 1,
                    Name = "ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc"
                },
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            var result = await _taskService.GetTasksOfUserWithTaskNameLessThan45Async(1);

            Assert.Empty(result);
        }

        #endregion GetTasksOfUserWithTaskNameLessThan45Async

        #region GetFinishedTasksInTheCurrentYearByUserAsync

        [Fact]
        public async Task GetFinishedTasksInTheCurrentYearByUser_WhenNotExistingId_ThrowsNotFound()
        {
            A.CallTo(() => _userRepository.ExistAsync(1)).Returns(false);

            await Assert.ThrowsAsync<NotFoundException>(() => _taskService.GetFinishedTasksInTheCurrentYearByUserAsync(1));
        }

        [Fact]
        public async Task GetFinishedTasksInTheCurrentYearByUser_WhenNoOneTaskOfThisUserFinished_ReturnsEmpty()
        {
            const int id = 30;
            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);

            var notFinishedTasksOrFinishedInPreviousYear = new[]
            {
                new ProjectTask() {PerformerId = id, State = TaskState.Created},
                new ProjectTask() {PerformerId = id, State = TaskState.Canceled},
                new ProjectTask() {PerformerId = id, State = TaskState.Finished, FinishedAt = new DateTime(2018, 1,1)}
            };
            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(notFinishedTasksOrFinishedInPreviousYear);

            var result = await _taskService.GetFinishedTasksInTheCurrentYearByUserAsync(id);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetFinishedTasksInTheCurrentYearByUser_When2FinishedAnd1CancelledTasks_ReturnsNotEmptyWith2Tasks()
        {
            const int id = 40;
            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, PerformerId = id, State = TaskState.Finished, FinishedAt = DateTime.Now},
                new ProjectTask() {Id = 2, PerformerId = id, State = TaskState.Finished, FinishedAt = new DateTime(DateTime.Now.Year, 1,1)},
                new ProjectTask() {Id = 3, PerformerId = id, State = TaskState.Canceled, FinishedAt = DateTime.Now}
            };
            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            var result = await _taskService.GetFinishedTasksInTheCurrentYearByUserAsync(id);

            Assert.Equal(2, result.Count());
            Assert.DoesNotContain(result, task => task.TaskId == 3);
        }

        #endregion GetFinishedTasksInTheCurrentYearByUserAsync

        #region GetUserInfoAsync

        [Fact]
        public async Task GetUserInfo_WhenNotExistingId_ThrowsNotFound()
        {
            const int id = 44;
            A.CallTo(() => _userRepository.ExistAsync(id)).Returns(false);

            await Assert.ThrowsAsync<NotFoundException>(() => _taskService.GetUserInfoAsync(id));
        }

        [Fact]
        public async Task GetUserInfo_WhenDoesNotHaveAuthoredProject_ThrowsInvalidOperation()
        {
            const int id = 55;
            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);
            A.CallTo(() => _userRepository.RetrieveAsync(id, true))
                .Returns(new User() { Id = id });

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new List<Project>());

            await Assert.ThrowsAsync<InvalidOperationException>(() => _taskService.GetUserInfoAsync(id));
        }

        [Fact]
        public async Task GetUserInfo_When3AuthoredProjectsAndNoOneTask_ReturnsNewestProjectAndZeroCountAndNullLongest()
        {
            const int id = 55;
            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);
            A.CallTo(() => _userRepository.RetrieveAsync(id, false))
                .Returns(new User() { Id = id });

            var projects = new[]
            {
                new Project() {Id = 1, AuthorId = id, CreatedAt = new DateTime(2010, 1, 1)},
                new Project() {Id = 2, AuthorId = id, CreatedAt = DateTime.Now},
                new Project() {Id = 3, AuthorId = id, CreatedAt = new DateTime(2020, 07, 19)},
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new List<ProjectTask>());

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(projects);
            A.CallTo(() => _mapper.Map<ProjectDTO>(projects[1]))
                .Returns(new ProjectDTO() { AuthorId = id, Id = projects[1].Id });

            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(null))
                .Returns(null);

            var result = await _taskService.GetUserInfoAsync(id);

            Assert.Equal(projects[1].Id, result.LastProject.Id);
            Assert.Equal(0, result.CountOfTasksInLastProject);
            Assert.Equal(0, result.CountOfUnfinishedTasks);
            Assert.Null(result.LongestTask);
        }

        [Fact]
        public async Task GetUserInfo_When2TasksInLastProject_ReturnsNewestProjectAnd2AsCountOfTasksInLastProject()
        {
            const int id = 55;
            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);
            A.CallTo(() => _userRepository.RetrieveAsync(id, false))
                .Returns(new User() { Id = id });

            var projects = new[]
            {
                new Project() {Id = 1, AuthorId = id, CreatedAt = new DateTime(2010, 1, 1)},
                new Project() {Id = 2, AuthorId = id, CreatedAt = DateTime.Now},
                new Project() {Id = 3, AuthorId = id, CreatedAt = new DateTime(2020, 07, 19)},
            };

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, ProjectId = 1},
                new ProjectTask() {Id = 2, ProjectId = 3},
                new ProjectTask() {Id = 3, ProjectId = 2},
                new ProjectTask() {Id = 4, ProjectId = 2}
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(projects);
            A.CallTo(() => _mapper.Map<ProjectDTO>(projects[1]))
                .Returns(new ProjectDTO() { AuthorId = id, Id = projects[1].Id });

            var result = await _taskService.GetUserInfoAsync(id);

            Assert.Equal(tasks.Count(task => task.ProjectId == result.LastProject.Id),
                result.CountOfTasksInLastProject);
        }

        [Fact]
        public async Task GetUserInfo_When2UnfinishedTasks_Returns2AsCountOfUnfinishedTasks()
        {
            const int id = 55;
            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);
            A.CallTo(() => _userRepository.RetrieveAsync(id, false))
                .Returns(new User() { Id = id });

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, PerformerId = id, State = TaskState.Finished},
                new ProjectTask() {Id = 2, PerformerId = id, State = TaskState.Created},
                new ProjectTask() {Id = 3, PerformerId = id, State = TaskState.Finished},
                new ProjectTask() {Id = 4, PerformerId = id, State = TaskState.Started}
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new List<Project>() { new Project() { AuthorId = id } });

            var result = await _taskService.GetUserInfoAsync(id);

            Assert.Equal(tasks.Count(task => task.PerformerId == id && task.State == TaskState.Finished),
                result.CountOfUnfinishedTasks);
        }

        [Fact]
        public async Task GetUserInfo_When3PerformedTasks_ReturnsLongestTask()
        {
            const int id = 55;
            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);
            A.CallTo(() => _userRepository.RetrieveAsync(id, false))
                .Returns(new User() { Id = id });

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, PerformerId = id, CreatedAt = new DateTime(2020, 7,20), FinishedAt = DateTime.Now},
                new ProjectTask() {Id = 2, PerformerId = id, CreatedAt = new DateTime(2020, 1,1), FinishedAt = new DateTime(2020, 6,6)},
                new ProjectTask() {Id = 3, PerformerId = id, CreatedAt = new DateTime(2000,1,1), FinishedAt = new DateTime(2020, 7,20)},
                new ProjectTask() {Id = 4, PerformerId = id, CreatedAt = new DateTime(2018, 1,25), FinishedAt = DateTime.Now}
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            A.CallTo(() => _projectRepository.RetrieveAllAsync(null))
                .Returns(new List<Project>() { new Project() { AuthorId = id } });
            A.CallTo(() => _mapper.Map<ProjectTaskDTO>(tasks[2]))
                .Returns(new ProjectTaskDTO() { Id = tasks[2].Id });

            var result = await _taskService.GetUserInfoAsync(id);

            Assert.Equal(tasks[2].Id, result.LongestTask.Id);
        }

        #endregion GetUserInfoAsync

        #region GetActiveUserTasksAsync

        [Fact]
        public async Task GetActiveUserTasks_WhenNotExistingId_ThrowsNotFound()
        {
            A.CallTo(() => _userRepository.ExistAsync(13)).Returns(false);

            await Assert.ThrowsAsync<NotFoundException>(() => _taskService.GetActiveUserTasksAsync(13));
        }

        [Fact]
        public async Task GetActiveUserTasks_WhenNoOneActiveTask_ReturnsEmpty()
        {
            const int id = 37;

            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);

            var notActiveTasks = new[]
            {
                new ProjectTask() {PerformerId = id, State = TaskState.Finished},
                new ProjectTask() {PerformerId = id, State = TaskState.Canceled},
                new ProjectTask() {PerformerId = id + id, State = TaskState.Started}
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(notActiveTasks);

            var result = await _taskService.GetActiveUserTasksAsync(id);

            Assert.Empty(result.Tasks);
        }

        [Fact]
        public async Task GetActiveUserTasks_When2ActiveAnd1FinishedTasks_ReturnsNotEmpty()
        {
            const int id = 14;
            A.CallTo(() => _userRepository.ExistAsync(id))
                .Returns(true);

            var tasks = new[]
            {
                new ProjectTask() {Id = 1, PerformerId = id, State = TaskState.Created},
                new ProjectTask() {Id = 2, PerformerId = id, State = TaskState.Canceled},
                new ProjectTask() {Id = 3, PerformerId = id, State = TaskState.Started},
                new ProjectTask() {Id = 4, PerformerId = id+id, State = TaskState.Started}
            };

            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(tasks);

            var result = await _taskService.GetActiveUserTasksAsync(id);

            Assert.Equal(
                tasks.Count(prTask => prTask.State < TaskState.Finished && prTask.PerformerId == id),
                result.Tasks.Count());
            Assert.DoesNotContain(result.Tasks, prTask => prTask.Id == 4 || prTask.Id == 2);
        }

        #endregion GetActiveUserTasksAsync

        #region FinishRandomTask

        [Fact]
        public async Task FinishRandomTask_WhenTaskAlreadyFinished_ThrowsInvalidOperation()
        {
            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new[] { new ProjectTask() { State = TaskState.Finished } });

            await Assert.ThrowsAsync<InvalidOperationException>(() => _taskService.FinishRandomTask());
        }

        [Fact]
        public async Task FinishRandomTask_WhenNoOneTask_ThrowsInvalidOperation()
        {
            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new ProjectTask[0]);

            await Assert.ThrowsAsync<InvalidOperationException>(() => _taskService.FinishRandomTask());
        }

        [Fact]
        public async Task FinishRandomTask_WhenOneCreatedTask_Returns1()
        {
            const int taskId = 1;
            A.CallTo(() => _taskRepository.RetrieveAllAsync(null))
                .Returns(new[] { new ProjectTask() { State = TaskState.Created, Id = taskId } });

            var finishedRandomTaskdId = await _taskService.FinishRandomTask();

            Assert.Equal(taskId, finishedRandomTaskdId);
        }

        #endregion FinishRandomTask
    }
}
﻿using TaskManager.DAL.Data;
using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.DAL.Repositories
{
    public sealed class TaskRepository : AsyncRepository<ProjectTask>, ITaskRepository
    {
        public TaskRepository(TaskManagerContext context) : base(context)
        {
        }
    }
}

﻿using TaskManager.DAL.Models;

namespace TaskManager.DAL.Repositories.Abstract
{
    public interface IUserRepository : IAsyncCrudRepository<User, int>
    {
    }
}

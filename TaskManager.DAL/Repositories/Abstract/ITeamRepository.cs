﻿using TaskManager.DAL.Models;

namespace TaskManager.DAL.Repositories.Abstract
{
    public interface ITeamRepository : IAsyncCrudRepository<Team, int>
    {
    }
}

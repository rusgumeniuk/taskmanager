﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TaskManager.DAL.Repositories.Abstract
{
    public interface ICrudRepository<TEntity, in TKey> : IDisposable
        where TEntity : class
        where TKey : IEquatable<TKey>
    {
        void Create(TEntity entity);
        TEntity Update(TEntity entity);
        void Remove(TEntity entity);

        TEntity Retrieve(TKey id);
        IEnumerable<TEntity> RetrieveAll(Expression<Func<TEntity, bool>> expression = null);

        void SaveChanges();
    }
}

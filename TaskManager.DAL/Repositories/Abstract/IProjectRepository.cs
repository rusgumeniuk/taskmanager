﻿using TaskManager.DAL.Models;

namespace TaskManager.DAL.Repositories.Abstract
{
    public interface IProjectRepository : IAsyncCrudRepository<Project, int>
    {
    }
}

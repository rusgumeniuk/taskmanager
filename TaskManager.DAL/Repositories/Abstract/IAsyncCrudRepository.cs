﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TaskManager.DAL.Repositories.Abstract
{
    public interface IAsyncCrudRepository<TEntity, in TKey> : IDisposable
    where TEntity : class
    where TKey : IEquatable<TKey>
    {
        Task<TEntity> CreateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task RemoveAsync(TEntity entity);

        Task<TEntity> RetrieveAsync(TKey id, bool isNoTracking = false);
        Task<IEnumerable<TEntity>> RetrieveAllAsync(Expression<Func<TEntity, bool>> expression = null);

        Task<bool> ExistAsync(TKey id);
        Task DisposeAsync();

        Task SaveChangesAsync();
    }
}

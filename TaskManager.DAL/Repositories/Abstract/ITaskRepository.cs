﻿using TaskManager.DAL.Models;

namespace TaskManager.DAL.Repositories.Abstract
{
    public interface ITaskRepository : IAsyncCrudRepository<ProjectTask, int>
    {
    }
}

﻿using TaskManager.DAL.Data;
using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.DAL.Repositories
{
    public sealed class TeamRepository : AsyncRepository<Team>, ITeamRepository
    {
        public TeamRepository(TaskManagerContext context) : base(context)
        {
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;

using System.Linq;
using System.Threading.Tasks;

using TaskManager.DAL.Data;
using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.DAL.Repositories
{
    public sealed class UserRepository : AsyncRepository<User>, IUserRepository
    {
        private TaskManagerContext _context => Context as TaskManagerContext;
        public UserRepository(TaskManagerContext context) : base(context)
        {
        }

        public override async Task RemoveAsync(User entity)
        {
            var tasks = await _context
                .Tasks
                .Where(task => task.PerformerId == entity.Id)
                .ToListAsync();
            foreach (var task in tasks)
            {
                _context.Tasks.Remove(task);
            }
            await base.RemoveAsync(entity);
        }
    }
}

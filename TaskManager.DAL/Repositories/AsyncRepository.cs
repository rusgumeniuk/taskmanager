﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.DAL.Repositories
{
    public abstract class AsyncRepository<TEntity> : IAsyncCrudRepository<TEntity, int>
        where TEntity : IdentifiedEntity
    {
        protected readonly DbContext Context;

        protected AsyncRepository(DbContext context)
        {
            Context = context;
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            var res = await Context.Set<TEntity>().AddAsync(entity);
            return await Context.Set<TEntity>().FindAsync(res.Entity.Id);
        }

        public Task UpdateAsync(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            return Task.CompletedTask;
        }

        public virtual Task RemoveAsync(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
            return Task.CompletedTask;
        }

        /// <summary />
        /// <exception cref="InvalidOperationException">When 2+ entity with id</exception>
        public async Task<TEntity> RetrieveAsync(int id, bool isNoTracking = false)
        {
            return isNoTracking ?
                await Context.Set<TEntity>().AsNoTracking().SingleAsync(entity => entity.Id == id) :
                await Context.Set<TEntity>().FindAsync(id);
        }

        public async Task<IEnumerable<TEntity>> RetrieveAllAsync(Expression<Func<TEntity, bool>> expression = null)
        {
            var set = await Context.Set<TEntity>().AsNoTracking().ToListAsync();
            if (!(expression is null))
            {
                set = set.Where(expression.Compile()).ToList();
            }

            return set;
        }

        /// <summary />
        /// <param name="id">Identifier to check</param>
        /// <exception cref="InvalidOperationException">When 2+ entity with id</exception>
        /// <returns>Indicator of whether an entity with an id already exists</returns>
        public async Task<bool> ExistAsync(int id)
        {
            return await Context.Set<TEntity>().AsNoTracking().SingleOrDefaultAsync(entity => entity.Id == id) != null;
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public async Task DisposeAsync()
        {
            await Context.DisposeAsync();
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }
    }
}

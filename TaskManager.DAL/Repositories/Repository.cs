﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using TaskManager.API.Data;
using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.DAL.Repositories
{
    public abstract class Repository<TEntity> : ICrudRepository<TEntity, int>
        where TEntity : IdentifiedEntity
    {
        private readonly IDataSource<TEntity> _source;

        protected Repository(IDataSource<TEntity> source)
        {
            if (source?.Set == null)
                throw new ArgumentNullException(nameof(source));
            _source = source;
        }


        public void Dispose()
        {
            _source.Dispose();
        }

        public void Create(TEntity entity)
        {
            entity.Id = GenerateNewId();
            _source.Set.Add(entity);
        }

        public TEntity Update(TEntity entity)
        {
            var oldEntity = _source.Set.First(item => item.Id == entity.Id);
            _source.Set.Remove(oldEntity);
            _source.Set.Add(entity);
            return _source.Set.First(item => item.Id == entity.Id);
        }

        public void Remove(TEntity entity)
        {
            var entityToRemove = _source.Set.First(item => item.Id == entity.Id);
            _source.Set.Remove(entityToRemove);
        }

        public TEntity Retrieve(int id)
        {
            return _source.Set.FirstOrDefault(item => item.Id == id);
        }

        public IEnumerable<TEntity> RetrieveAll(Expression<Func<TEntity, bool>> expression = null)
        {
            var set = _source.Set;
            if (expression != null)
            {
                set = set.Where(expression.Compile()).ToList();
            }

            return set;
        }

        public void SaveChanges()
        {

        }

        private int GenerateNewId()
        {
            if (_source.Set.Count == 0)
                return 0;

            var newId = _source.Set.Last().Id + 1;
            while (_source.Set.Any(item => item.Id == newId))
            {
                ++newId;
            }

            return newId;
        }
    }
}

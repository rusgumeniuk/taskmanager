﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Diagnostics;

using TaskManager.DAL.Models;

namespace TaskManager.DAL.Data
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            #region Validation

            #region Team
            modelBuilder.Entity<Team>().Property(team => team.Name).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<Team>().Property(team => team.CreatedAt).IsRequired();
            #endregion

            #region User
            modelBuilder.Entity<User>().Property(user => user.FirstName).HasMaxLength(40).IsRequired();
            modelBuilder.Entity<User>().Property(user => user.LastName).HasMaxLength(40);
            modelBuilder.Entity<User>().Property(user => user.Email).HasMaxLength(320).IsRequired();
            modelBuilder.Entity<User>().Property(user => user.BirthDay).IsRequired();
            modelBuilder.Entity<User>().Property(user => user.RegisteredAt).IsRequired();
            #endregion

            #region Project
            modelBuilder.Entity<Project>().Property(project => project.Name).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<Project>().Property(project => project.Description).IsRequired();
            modelBuilder.Entity<Project>().Property(project => project.CreatedAt).IsRequired();
            modelBuilder.Entity<Project>().Property(project => project.Deadline).IsRequired();
            modelBuilder.Entity<Project>().Property(project => project.AuthorId).IsRequired();
            modelBuilder.Entity<Project>().Property(project => project.TeamId).IsRequired();
            #endregion

            #region Task
            modelBuilder.Entity<ProjectTask>().Property(task => task.Name).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(task => task.Description).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(task => task.CreatedAt).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(task => task.FinishedAt).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(task => task.State).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(task => task.ProjectId).IsRequired();
            modelBuilder.Entity<ProjectTask>().Property(task => task.PerformerId).IsRequired();
            #endregion

            #region TaskStateModel
            modelBuilder.Entity<TaskStateModel>().ToTable("TaskStates");
            modelBuilder.Entity<TaskStateModel>().Property(taskState => taskState.Value).IsRequired();
            #endregion

            #endregion

            #region Relations
            modelBuilder.Entity<Project>()
                .HasMany(project => project.Tasks)
                .WithOne(task => task.Project)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Team>()
                .HasMany(team => team.Projects)
                .WithOne(project => project.Team)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Team>()
                .HasMany(team => team.Users)
                .WithOne(user => user.Team)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>()
                .HasMany(user => user.AuthoredProjects)
                .WithOne(project => project.Author)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>()
                .HasMany(user => user.Tasks)
                .WithOne(task => task.Performer)
                .OnDelete(DeleteBehavior.NoAction);
            #endregion
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            try
            {
                var teams = new JsonDataSource<Team>().Set;
                var users = new JsonDataSource<User>().Set;
                var projects = new JsonDataSource<Project>().Set;
                var tasks = new JsonDataSource<ProjectTask>("task").Set;

                modelBuilder.Entity<Team>().HasData(teams);
                modelBuilder.Entity<User>().HasData(users);
                modelBuilder.Entity<Project>().HasData(projects);
                modelBuilder.Entity<ProjectTask>().HasData(tasks);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

        }
    }
}

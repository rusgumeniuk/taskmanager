﻿using Newtonsoft.Json;

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using TaskManager.API.Data;
using TaskManager.DAL.Models;

namespace TaskManager.DAL.Data
{
    public class JsonDataSource<TEntity> : IDataSource<TEntity>
        where TEntity : IdentifiedEntity
    {
        public ICollection<TEntity> Set { get; private set; }

        public JsonDataSource(string fileName = null)
        {
            LoadJsonAsync(fileName).ConfigureAwait(true);
        }

        private async Task LoadJsonAsync(string fileName = null)
        {
            using (var r =
                new StreamReader($"{Directory.GetCurrentDirectory()}\\Resources\\{fileName ?? typeof(TEntity).Name}.json"))
            {
                var json = await r.ReadToEndAsync();
                Set = JsonConvert.DeserializeObject<ICollection<TEntity>>(json);
            }
        }

        public void Dispose()
        {
            Set.GetEnumerator().Dispose();
        }
    }
}
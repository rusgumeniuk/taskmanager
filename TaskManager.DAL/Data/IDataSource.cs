﻿using System;
using System.Collections.Generic;

using TaskManager.DAL.Models;

namespace TaskManager.API.Data
{
    public interface IDataSource<TEntity> : IDisposable
        where TEntity : IdentifiedEntity
    {
        ICollection<TEntity> Set { get; }
    }
}

﻿namespace TaskManager.DAL.Models
{
    public class IdentifiedEntity
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
﻿namespace TaskManager.DAL.Models
{
    public class TaskStateModel : IdentifiedEntity
    {
        public string Value { get; set; }
    }
}

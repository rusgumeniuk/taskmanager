﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaskManager.DAL.Models
{
    public class Project : IdentifiedEntity
    {
        [MinLength(3)]
        public string Name { get; set; }
        [MinLength(3)]
        public string Description { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }

        public IEnumerable<ProjectTask> Tasks { get; set; }
    }
}

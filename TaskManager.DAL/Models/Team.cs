﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaskManager.DAL.Models
{
    public class Team : IdentifiedEntity
    {
        [MinLength(3)]
        public string Name { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }

        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Project> Projects { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaskManager.DAL.Models
{
    public class User : IdentifiedEntity
    {
        [MinLength(2)]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }
        [DataType(DataType.Date)]
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public IEnumerable<Project> AuthoredProjects { get; set; }
        public IEnumerable<ProjectTask> Tasks { get; set; }
    }
}

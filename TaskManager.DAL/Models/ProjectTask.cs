﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TaskManager.DAL.Models
{
    public class ProjectTask : IdentifiedEntity
    {
        [MinLength(3)]
        public string Name { get; set; }
        [MinLength(3)]
        public string Description { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
    }
}
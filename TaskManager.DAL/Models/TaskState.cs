﻿namespace TaskManager.DAL.Models
{
    public enum TaskState : int
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}

﻿using System;

using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.DAL.UnitOfWorks.Abstract
{
    public interface ITaskUserProjectTeamUnitOfWork : IDisposable
    {
        IProjectRepository Projects { get; }
        ITaskRepository Tasks { get; }
        IUserRepository Users { get; }
        ITeamRepository Teams { get; }
    }
}

﻿using TaskManager.DAL.Repositories.Abstract;
using TaskManager.DAL.UnitOfWorks.Abstract;

namespace TaskManager.DAL.UnitOfWorks
{
    public sealed class TaskUserProjectTeamUnitOfWork : ITaskUserProjectTeamUnitOfWork
    {
        public IProjectRepository Projects { get; }
        public ITaskRepository Tasks { get; }
        public IUserRepository Users { get; }
        public ITeamRepository Teams { get; }

        public TaskUserProjectTeamUnitOfWork(IProjectRepository projects, ITaskRepository tasks, IUserRepository users, ITeamRepository teams)
        {
            Projects = projects;
            Tasks = tasks;
            Users = users;
            Teams = teams;
        }

        public void Dispose()
        {
            Tasks?.Dispose();
            Users?.Dispose();
            Projects?.Dispose();
            Teams?.Dispose();
        }
    }
}

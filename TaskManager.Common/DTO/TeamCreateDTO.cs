﻿namespace TaskManager.Common.DTO
{
    public sealed class TeamCreateDTO
    {
        public string Name { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace TaskManager.Common.DTO
{
    public sealed class TeamWithMembersDTO
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<UserDTO> Members { get; set; }
    }
}

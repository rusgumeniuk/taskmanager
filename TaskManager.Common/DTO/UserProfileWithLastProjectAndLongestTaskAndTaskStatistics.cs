﻿namespace TaskManager.Common.DTO
{
    public class UserProfileWithLastProjectAndLongestTaskAndTaskStatistics
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int CountOfUnfinishedTasks { get; set; }
        public int CountOfTasksInLastProject { get; set; }
        public ProjectTaskDTO LongestTask { get; set; }
    }
}

﻿namespace TaskManager.Common.DTO
{
    public class TaskShortInfo
    {
        public int TaskId { get; set; }
        public string TaskName { get; set; }
    }
}

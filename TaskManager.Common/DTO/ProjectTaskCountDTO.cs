﻿namespace TaskManager.Common.DTO
{
    public sealed class ProjectTaskCountDTO
    {
        public ProjectDTO Project { get; set; }
        public int CountOfTasks { get; set; }
    }
}

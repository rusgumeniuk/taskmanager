﻿using System.Collections.Generic;

namespace TaskManager.Common.DTO
{
    public class UserTasksProfile
    {
        public UserDTO User { get; set; }
        public IEnumerable<ProjectTaskDTO> Tasks { get; set; }
    }
}

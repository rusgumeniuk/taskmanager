﻿using System;

namespace TaskManager.Common.DTO
{
    public sealed class UserCreateDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public int? TeamId { get; set; }
        public string Email { get; set; }
    }
}

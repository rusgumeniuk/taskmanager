﻿namespace TaskManager.Common.DTO
{
    public class ProjectTasksStaffInfo
    {
        public ProjectDTO Project { get; set; }
        public ProjectTaskDTO TaskWithLongestDescription { get; set; }
        public ProjectTaskDTO TaskWithShortestName { get; set; }
        public int CountOfStaff { get; set; }
    }
}

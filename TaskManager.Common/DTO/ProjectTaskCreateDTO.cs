﻿namespace TaskManager.Common.DTO
{
    public sealed class ProjectTaskCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}

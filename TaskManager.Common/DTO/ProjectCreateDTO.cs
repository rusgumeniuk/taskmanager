﻿using System;

namespace TaskManager.Common.DTO
{
    public sealed class ProjectCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public int TeamId { get; set; }
        public int AuthorId { get; set; }
    }
}

﻿namespace TaskManager.BLL.Models
{
    public enum ErrorCode
    {
        General = 1,
        NotFound
    }
}

﻿using AutoMapper;

using TaskManager.Common.DTO;
using TaskManager.DAL.Models;

namespace TaskManager.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();
            CreateMap<ProjectCreateDTO, Project>();
        }
    }
}

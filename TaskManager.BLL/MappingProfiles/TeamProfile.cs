﻿using AutoMapper;

using TaskManager.Common.DTO;
using TaskManager.DAL.Models;

namespace TaskManager.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
            CreateMap<TeamCreateDTO, Team>();
        }
    }
}

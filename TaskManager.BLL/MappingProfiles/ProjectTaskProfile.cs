﻿using AutoMapper;

using TaskManager.Common.DTO;
using TaskManager.DAL.Models;

namespace TaskManager.BLL.MappingProfiles
{
    public sealed class ProjectTaskProfile : Profile
    {
        public ProjectTaskProfile()
        {
            CreateMap<ProjectTask, ProjectTaskDTO>();
            CreateMap<ProjectTaskDTO, ProjectTask>();
            CreateMap<ProjectTaskCreateDTO, ProjectTask>();
        }
    }
}

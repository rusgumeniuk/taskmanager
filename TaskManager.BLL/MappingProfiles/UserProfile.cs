﻿using AutoMapper;

using TaskManager.Common.DTO;
using TaskManager.DAL.Models;

namespace TaskManager.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
            CreateMap<UserCreateDTO, User>();
        }
    }
}

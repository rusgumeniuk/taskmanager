﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Exceptions;
using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;
using TaskManager.DAL.Models;
using TaskManager.DAL.UnitOfWorks.Abstract;

namespace TaskManager.BLL.Services
{
    public sealed class TeamService : CrudService<Team, TeamDTO, TeamCreateDTO>, ITeamService
    {
        private readonly ITaskUserProjectTeamUnitOfWork _unitOfWork;

        public TeamService(ITaskUserProjectTeamUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork.Teams, mapper)
        {
            _unitOfWork = unitOfWork;
        }

        public override async Task<TeamDTO> AddAsync(TeamCreateDTO dto)
        {
            if (dto is null)
                throw new ArgumentNullException(nameof(dto));

            var entity = Mapper.Map<Team>(dto);

            entity.CreatedAt = DateTime.Now;

            await _unitOfWork.Teams.CreateAsync(entity);

            await _unitOfWork.Teams.SaveChangesAsync();

            return Mapper.Map<TeamDTO>(entity);
        }

        public override async Task RemoveAsync(int id)
        {
            if (!await _unitOfWork.Teams.ExistAsync(id))
                throw new NotFoundException("Team", id);

            var team = await _unitOfWork.Teams.RetrieveAsync(id, true);
            var projects = await _unitOfWork
                .Projects
                .RetrieveAllAsync(project => project.TeamId == team.Id);
            foreach (var project in projects)
            {
                await _unitOfWork.Projects.RemoveAsync(project);
            }

            await _unitOfWork.Teams.RemoveAsync(team);
            await _unitOfWork.Teams.SaveChangesAsync();
        }

        public async Task<IEnumerable<TeamWithMembersDTO>> GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTimeAsync()
        {
            var users = await _unitOfWork.Users.RetrieveAllAsync();
            var teams = await _unitOfWork.Teams.RetrieveAllAsync();

            return teams
                .GroupJoin(users
                        .Where(user => user.BirthDay.AddYears(10) <= DateTime.Now)
                        .OrderByDescending(user => user.RegisteredAt),
                    t => t.Id,
                    u => u.TeamId,
                    (team, teamMembers) =>
                        new TeamWithMembersDTO
                        {
                            TeamId = team.Id,
                            TeamName = team.Name,
                            Members = teamMembers.Select(member => Mapper.Map<UserDTO>(member))
                        })
                .Where(team => team.Members.Any());
        }
    }
}

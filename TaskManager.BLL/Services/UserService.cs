﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Exceptions;
using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;
using TaskManager.DAL.Models;
using TaskManager.DAL.UnitOfWorks.Abstract;

namespace TaskManager.BLL.Services
{
    public sealed class UserService : CrudService<User, UserDTO, UserCreateDTO>, IUserService
    {
        private readonly ITaskUserProjectTeamUnitOfWork _unitOfWork;
        public UserService(ITaskUserProjectTeamUnitOfWork unitOfWork, IMapper mapper)
            : base(unitOfWork.Users, mapper)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<bool> ExistUserAsync(int userId)
        {
            return _unitOfWork.Users.ExistAsync(userId);
        }

        public override async Task<UserDTO> AddAsync(UserCreateDTO dto)
        {
            if (dto is null)
                throw new ArgumentNullException(nameof(dto));

            var entity = Mapper.Map<User>(dto);

            entity.RegisteredAt = DateTime.Now;

            if (entity.TeamId != null && !await _unitOfWork.Teams.ExistAsync(entity.TeamId.Value))
                throw new InvalidOperationException($"Incorrect Team ID [{entity.TeamId}]");

            await _unitOfWork.Users.CreateAsync(entity);

            await _unitOfWork.Users.SaveChangesAsync();

            return Mapper.Map<UserDTO>(entity);
        }

        public override async Task UpdateAsync(UserDTO dto)
        {
            if (dto is null)
                throw new ArgumentNullException(nameof(dto));

            if (!await _unitOfWork.Users.ExistAsync(dto.Id))
                throw new NotFoundException(nameof(User), dto.Id);

            var entity = Mapper.Map<User>(dto);
            var oldEntity = Mapper.Map<User>(await GetAsync(dto.Id, true));

            if (entity.TeamId != null && !await _unitOfWork.Teams.ExistAsync(entity.TeamId.Value))
                throw new InvalidOperationException($"Incorrect Team ID [{entity.TeamId}]");

            entity.RegisteredAt = oldEntity.RegisteredAt;

            await _unitOfWork.Users.UpdateAsync(entity);
            await _unitOfWork.Users.SaveChangesAsync();
        }

        public override async Task RemoveAsync(int id)
        {
            if (!await _unitOfWork.Users.ExistAsync(id))
                throw new NotFoundException("User", id);

            var user = await _unitOfWork.Users.RetrieveAsync(id, true);
            var authoredProjects = await _unitOfWork
                .Projects
                .RetrieveAllAsync(project => project.AuthorId == user.Id);
            foreach (var project in authoredProjects)
            {
                await _unitOfWork.Projects.RemoveAsync(project);
            }

            await _unitOfWork.Users.RemoveAsync(user);
            await _unitOfWork.Users.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserTasksProfile>> GetUsersOrderedByFirstNameSortedByTaskNameAsync()
        {
            var users = await _unitOfWork.Users.RetrieveAllAsync();
            var tasks = await _unitOfWork.Tasks.RetrieveAllAsync();
            return
                users
                    .OrderBy(user => user.FirstName)
                    .GroupJoin(tasks.OrderByDescending(task => task.Name.Length),
                        user => user.Id,
                        task => task.PerformerId,
                        (user, taskList) => new UserTasksProfile()
                        {
                            User = Mapper.Map<UserDTO>(user),
                            Tasks = taskList.Select(task => Mapper.Map<ProjectTaskDTO>(task))
                        }
                    );
        }

        public async Task<IEnumerable<UserDTO>> GetUsersByProjectId(int projectId)
        {
            if (!await _unitOfWork.Projects.ExistAsync(projectId))
                throw new NotFoundException(nameof(Project), projectId);

            var project = await _unitOfWork.Projects.RetrieveAsync(projectId);
            var users = await _unitOfWork.Users.RetrieveAllAsync(user => user.TeamId == project.TeamId);
            if (!users.Any())
                throw new InvalidOperationException("No users");
            return users
                .Select(user => Mapper.Map<UserDTO>(user));


        }
    }
}

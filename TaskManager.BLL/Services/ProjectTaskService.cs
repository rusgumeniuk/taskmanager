﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Exceptions;
using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;
using TaskManager.DAL.Models;
using TaskManager.DAL.UnitOfWorks.Abstract;

namespace TaskManager.BLL.Services
{
    public sealed class ProjectTaskService : CrudService<ProjectTask, ProjectTaskDTO, ProjectTaskCreateDTO>, IProjectTaskService
    {
        private readonly ITaskUserProjectTeamUnitOfWork _unitOfWork;

        public ProjectTaskService(ITaskUserProjectTeamUnitOfWork unitOfWork, IMapper mapper)
            : base(unitOfWork.Tasks, mapper)
        {
            _unitOfWork = unitOfWork;
        }

        public override async Task<ProjectTaskDTO> AddAsync(ProjectTaskCreateDTO dto)
        {
            if (dto is null)
                throw new ArgumentNullException(nameof(dto));

            var entity = Mapper.Map<ProjectTask>(dto);

            entity.CreatedAt = DateTime.Now;
            entity.State = TaskState.Created;

            if (!await _unitOfWork.Users.ExistAsync(entity.PerformerId))
                throw new InvalidOperationException($"Incorrect Performer ID [{entity.PerformerId}]");

            if (!await _unitOfWork.Projects.ExistAsync(entity.ProjectId))
                throw new InvalidOperationException($"Incorrect Project ID [{entity.ProjectId}]");

            await _unitOfWork.Tasks.CreateAsync(entity);

            await _unitOfWork.Tasks.SaveChangesAsync();

            return Mapper.Map<ProjectTaskDTO>(entity);
        }

        public override async Task UpdateAsync(ProjectTaskDTO dto)
        {
            var entity = Mapper.Map<ProjectTask>(dto);
            var oldEntity = Mapper.Map<ProjectTask>(await GetAsync(dto.Id, true));

            if (!await _unitOfWork.Users.ExistAsync(entity.PerformerId))
                throw new InvalidOperationException($"Incorrect Performer ID [{entity.PerformerId}]");

            if (!await _unitOfWork.Projects.ExistAsync(entity.ProjectId))
                throw new InvalidOperationException($"Incorrect Project ID [{entity.ProjectId}]");

            if (oldEntity.State > entity.State)
                throw new InvalidOperationException($"System can't set state {entity.State} when current state is {oldEntity.State}");

            entity.CreatedAt = oldEntity.CreatedAt;

            await _unitOfWork.Tasks.UpdateAsync(entity);
            await _unitOfWork.Tasks.SaveChangesAsync();
        }

        public async Task<IEnumerable<ProjectTaskDTO>> GetTasksOfUserWithTaskNameLessThan45Async(int userId)
        {
            if (!await _unitOfWork.Users.ExistAsync(userId))
                throw new NotFoundException(nameof(User), userId);

            var tasks = await _unitOfWork.Tasks.RetrieveAllAsync();
            return tasks
                .Where(task => task.PerformerId == userId && task.Name.Length < 45)
                .Select(task => Mapper.Map<ProjectTaskDTO>(task));
        }

        public async Task<IEnumerable<TaskShortInfo>> GetFinishedTasksInTheCurrentYearByUserAsync(int userId)
        {
            if (!await _unitOfWork.Users.ExistAsync(userId))
                throw new NotFoundException(nameof(User), userId);

            var tasks = await _unitOfWork.Tasks.RetrieveAllAsync();
            return tasks
                .Where(task => task.PerformerId == userId
                               && task.State == TaskState.Finished
                               && task.FinishedAt.Year == DateTime.Now.Year)
                .Select(task => new TaskShortInfo() { TaskId = task.Id, TaskName = task.Name });
        }

        public async Task<UserTasksProfile> GetActiveUserTasksAsync(int userId)
        {
            if (!await _unitOfWork.Users.ExistAsync(userId))
                throw new NotFoundException(nameof(User), userId);

            var tasks = await _unitOfWork.Tasks.RetrieveAllAsync();
            return new UserTasksProfile()
            {
                User = Mapper.Map<UserDTO>(await _unitOfWork.Users.RetrieveAsync(userId, true)),
                Tasks = tasks
                    .Where(task => task.PerformerId == userId && task.State < TaskState.Finished)
                    .Select(task => Mapper.Map<ProjectTaskDTO>(task))
            };
        }

        public async Task<UserProfileWithLastProjectAndLongestTaskAndTaskStatistics> GetUserInfoAsync(int userId)
        {
            if (!await _unitOfWork.Users.ExistAsync(userId))
                throw new NotFoundException(nameof(User), userId);

            var user = await _unitOfWork.Users.RetrieveAsync(userId);
            var projects = await _unitOfWork.Projects.RetrieveAllAsync();
            var tasks = await _unitOfWork.Tasks.RetrieveAllAsync();

            var lastProject = projects
                .Where(project => project.AuthorId == user.Id)
                .OrderByDescending(project => project.CreatedAt)
                .FirstOrDefault();
            if (lastProject == null)
                throw new InvalidOperationException($"User with ID: {userId} has no one project!");
            var numberOfTasksInLastProject = tasks
                .Count(task => task.ProjectId == lastProject.Id);
            var numberOfNotFinishedUserTasks = tasks
                .Count(task => task.PerformerId == userId && task.State != TaskState.Finished);
            var longestByTimeTask = tasks
                .Where(task => task.PerformerId == userId)
                .DefaultIfEmpty()
                .Aggregate((task1, task2) =>
                    (task2.FinishedAt - task2.CreatedAt) >= (task1.FinishedAt - task1.CreatedAt) ? task2 : task1);

            return new UserProfileWithLastProjectAndLongestTaskAndTaskStatistics()
            {
                User = Mapper.Map<UserDTO>(user),
                LastProject = Mapper.Map<ProjectDTO>(lastProject),
                CountOfTasksInLastProject = numberOfTasksInLastProject,
                CountOfUnfinishedTasks = numberOfNotFinishedUserTasks,
                LongestTask = Mapper.Map<ProjectTaskDTO>(longestByTimeTask)
            };
        }

        public async Task<int> FinishRandomTask()
        {
            var allTasks = await _unitOfWork.Tasks.RetrieveAllAsync();
            if (!allTasks.Any())
                throw new InvalidOperationException("No tasks");

            var randomTask = allTasks.ElementAt(new Random().Next(0, allTasks.Count() - 1));

            if (randomTask.State >= TaskState.Finished)
                throw new InvalidOperationException($"Problem finishing a task with ID [{randomTask.Id}]");

            randomTask.State = TaskState.Finished;

            await _unitOfWork.Tasks.UpdateAsync(randomTask);
            await _unitOfWork.Tasks.SaveChangesAsync();

            return randomTask.Id;
        }
    }
}
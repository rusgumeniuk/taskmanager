﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaskManager.BLL.Services.Abstract
{
    public interface ICrudService<TEntity, TEntityCreate, in TKey> : IDisposable
        where TEntity : class
        where TEntityCreate : class
        where TKey : IEquatable<TKey>
    {
        Task<TEntity> AddAsync(TEntityCreate dto);
        Task UpdateAsync(TEntity dto);
        Task RemoveAsync(TKey id);
        Task<TEntity> GetAsync(TKey id, bool isNoTracking = false);
        Task<IEnumerable<TEntity>> GetAllAsync();
    }
}

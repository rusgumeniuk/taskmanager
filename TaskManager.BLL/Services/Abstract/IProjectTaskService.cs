﻿using System.Collections.Generic;
using System.Threading.Tasks;

using TaskManager.Common.DTO;

namespace TaskManager.BLL.Services.Abstract
{
    public interface IProjectTaskService : ICrudService<ProjectTaskDTO, ProjectTaskCreateDTO, int>
    {
        Task<IEnumerable<ProjectTaskDTO>> GetTasksOfUserWithTaskNameLessThan45Async(int userId);

        Task<IEnumerable<TaskShortInfo>> GetFinishedTasksInTheCurrentYearByUserAsync(int userId);

        Task<UserTasksProfile> GetActiveUserTasksAsync(int userId);

        Task<UserProfileWithLastProjectAndLongestTaskAndTaskStatistics> GetUserInfoAsync(int userId);

        Task<int> FinishRandomTask();
    }
}
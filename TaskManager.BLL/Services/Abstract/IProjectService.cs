﻿using System.Collections.Generic;
using System.Threading.Tasks;

using TaskManager.Common.DTO;

namespace TaskManager.BLL.Services.Abstract
{
    public interface IProjectService : ICrudService<ProjectDTO, ProjectCreateDTO, int>
    {
        Task<Dictionary<ProjectDTO, int>> GetNumberOfTasksOfUserAsync(int userId);
        Task<IEnumerable<ProjectTasksStaffInfo>> GetProjectInfoAsync();
    }
}

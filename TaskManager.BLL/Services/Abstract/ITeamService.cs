﻿using System.Collections.Generic;
using System.Threading.Tasks;

using TaskManager.Common.DTO;

namespace TaskManager.BLL.Services.Abstract
{
    public interface ITeamService : ICrudService<TeamDTO, TeamCreateDTO, int>
    {
        Task<IEnumerable<TeamWithMembersDTO>> GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTimeAsync();
    }
}

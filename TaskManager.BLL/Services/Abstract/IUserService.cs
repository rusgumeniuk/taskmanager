﻿using System.Collections.Generic;
using System.Threading.Tasks;

using TaskManager.Common.DTO;

namespace TaskManager.BLL.Services.Abstract
{
    public interface IUserService : ICrudService<UserDTO, UserCreateDTO, int>
    {
        Task<bool> ExistUserAsync(int userId);
        Task<IEnumerable<UserTasksProfile>> GetUsersOrderedByFirstNameSortedByTaskNameAsync();
        Task<IEnumerable<UserDTO>> GetUsersByProjectId(int projectId);
    }
}

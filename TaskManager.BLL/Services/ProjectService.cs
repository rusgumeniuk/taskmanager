﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Exceptions;
using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;
using TaskManager.DAL.Models;
using TaskManager.DAL.UnitOfWorks.Abstract;

namespace TaskManager.BLL.Services
{
    public sealed class ProjectService : CrudService<Project, ProjectDTO, ProjectCreateDTO>, IProjectService
    {
        private readonly ITaskUserProjectTeamUnitOfWork _unitOfWork;
        public ProjectService(IMapper mapper, ITaskUserProjectTeamUnitOfWork unitOfWork)
            : base(unitOfWork.Projects, mapper)
        {
            _unitOfWork = unitOfWork;
        }

        public override async Task<ProjectDTO> AddAsync(ProjectCreateDTO dto)
        {
            if (dto is null)
                throw new ArgumentNullException(nameof(dto));

            var entity = Mapper.Map<Project>(dto);

            entity.CreatedAt = DateTime.Now;

            if (!await _unitOfWork.Users.ExistAsync(entity.AuthorId))
                throw new InvalidOperationException($"Incorrect author ID [{entity.AuthorId}]");
            if (!await _unitOfWork.Teams.ExistAsync(entity.TeamId))
                throw new InvalidOperationException($"Incorrect team ID [{entity.TeamId}]");

            await _unitOfWork.Projects.CreateAsync(entity);

            await _unitOfWork.Projects.SaveChangesAsync();

            return Mapper.Map<ProjectDTO>(entity);
        }

        public override async Task UpdateAsync(ProjectDTO dto)
        {
            var entity = Mapper.Map<Project>(dto);
            var oldEntity = Mapper.Map<Project>(await GetAsync(dto.Id, true));

            if (!await _unitOfWork.Users.ExistAsync(entity.AuthorId))
                throw new InvalidOperationException($"Incorrect author ID [{entity.AuthorId}]");
            if (!await _unitOfWork.Teams.ExistAsync(entity.TeamId))
                throw new InvalidOperationException($"Incorrect team ID [{entity.TeamId}]");

            entity.CreatedAt = oldEntity.CreatedAt;
            entity.AuthorId = oldEntity.AuthorId;

            await _unitOfWork.Projects.UpdateAsync(entity);
            await _unitOfWork.Projects.SaveChangesAsync();
        }

        public async Task<Dictionary<ProjectDTO, int>> GetNumberOfTasksOfUserAsync(int userId)
        {
            if (!await _unitOfWork.Users.ExistAsync(userId))
                throw new NotFoundException(nameof(User), userId);

            var tasks = await _unitOfWork.Tasks.RetrieveAllAsync();
            var projects = await _unitOfWork.Projects.RetrieveAllAsync();

            return projects
                .Where(project => project.AuthorId == userId)
                .GroupJoin(tasks,
                    project => project.Id,
                    task => task.ProjectId,
                    (project, taskList) => new { Project = project, TasksCount = taskList.Count() })
                .Where(pair => pair.TasksCount != 0)
                .ToDictionary(
                    pair => Mapper.Map<ProjectDTO>(pair.Project),
                    pair => pair.TasksCount);
        }

        public async Task<IEnumerable<ProjectTasksStaffInfo>> GetProjectInfoAsync()
        {
            var projects = await _unitOfWork.Projects.RetrieveAllAsync();
            var tasks = await _unitOfWork.Tasks.RetrieveAllAsync();
            var users = await _unitOfWork.Users.RetrieveAllAsync();

            return projects
                .GroupJoin(tasks.DefaultIfEmpty(),
                    project => project?.Id,
                    task => task?.ProjectId,
                    (project, projectTasks) => new ProjectTasksStaffInfo()
                    {
                        Project = Mapper.Map<ProjectDTO>(project),
                        TaskWithLongestDescription = Mapper.Map<ProjectTaskDTO>(
                            projectTasks
                                .DefaultIfEmpty()
                                .Aggregate((x, y) => x != null && y != null && x.Description?.Length <= y.Description?.Length ? y : x)),
                        TaskWithShortestName = Mapper.Map<ProjectTaskDTO>(
                            projectTasks
                            .DefaultIfEmpty()
                            .Aggregate((x, y) => x != null && y != null && x.Name?.Length >= y.Name?.Length ? y : x)),
                        CountOfStaff = project?.Description?.Length > 20 || projectTasks.DefaultIfEmpty().Count() < 3
                            ? users.DefaultIfEmpty().Count(user => user?.TeamId == project?.TeamId)
                            : 0
                    }
                );
        }
    }
}

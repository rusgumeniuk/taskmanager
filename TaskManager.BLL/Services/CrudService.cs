﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Exceptions;
using TaskManager.BLL.Services.Abstract;
using TaskManager.DAL.Models;
using TaskManager.DAL.Repositories.Abstract;

namespace TaskManager.BLL.Services
{
    public abstract class CrudService<TEntity, TEntityDTO, TEntityCreateDTO> : ICrudService<TEntityDTO, TEntityCreateDTO, int>
        where TEntity : IdentifiedEntity
        where TEntityDTO : class
        where TEntityCreateDTO : class
    {
        private readonly IAsyncCrudRepository<TEntity, int> _repository;
        protected readonly IMapper Mapper;

        protected CrudService(IAsyncCrudRepository<TEntity, int> repository, IMapper mapper)
        {
            _repository = repository;
            Mapper = mapper;
        }

        public virtual async Task<TEntityDTO> AddAsync(TEntityCreateDTO dto)
        {
            if(dto is null)
                throw new ArgumentNullException(nameof(dto));

            var entity = Mapper.Map<TEntity>(dto);
            await _repository.CreateAsync(entity);

            await _repository.SaveChangesAsync();

            return Mapper.Map<TEntityDTO>(entity);
        }

        /// <summary />
        /// <exception cref="NotFoundException">Throws an exception when doesn't exist entity with id</exception>
        public virtual async Task UpdateAsync(TEntityDTO dto)
        {
            var entity = Mapper.Map<TEntity>(dto);
            if (!await _repository.ExistAsync(entity.Id))
            {
                throw new NotFoundException(nameof(TEntity), entity.Id);
            }
            await _repository.UpdateAsync(entity);
            await _repository.SaveChangesAsync();
        }

        /// <summary />
        /// <exception cref="NotFoundException">Throws an exception when doesn't exist entity with id</exception>
        public virtual async Task RemoveAsync(int id)
        {
            if (!await _repository.ExistAsync(id))
                throw new NotFoundException(nameof(TEntity), id);

            var entity = await _repository.RetrieveAsync(id, true);

            await _repository.RemoveAsync(entity);
            await _repository.SaveChangesAsync();
        }

        /// <summary />
        /// <exception cref="NotFoundException">Throws an exception when doesn't exist entity with id</exception>
        /// <exception cref="InvalidOperationException"></exception>
        /// <returns>Not null entity</returns>
        public async Task<TEntityDTO> GetAsync(int id, bool isNoTracking = false)
        {
            var entity = await _repository.RetrieveAsync(id, isNoTracking);
            return entity != null ? Mapper.Map<TEntityDTO>(entity) : throw new NotFoundException(nameof(TEntity), id);
        }

        public async Task<IEnumerable<TEntityDTO>> GetAllAsync()
        {
            var all = await _repository.RetrieveAllAsync();
            return all.Select(entity => Mapper.Map<TEntityDTO>(entity));
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}

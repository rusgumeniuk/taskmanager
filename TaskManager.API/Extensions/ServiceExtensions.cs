﻿using AutoMapper;

using FluentValidation;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System.Linq;
using System.Reflection;

using TaskManager.API.Validators;
using TaskManager.BLL.MappingProfiles;
using TaskManager.BLL.Services;
using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;
using TaskManager.DAL.Data;
using TaskManager.DAL.Repositories;
using TaskManager.DAL.Repositories.Abstract;
using TaskManager.DAL.UnitOfWorks;
using TaskManager.DAL.UnitOfWorks.Abstract;

namespace TaskManager.API.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomDbContext(this IServiceCollection services, IConfiguration configs)
        {
            services.AddDbContext<TaskManagerContext>(options =>
                options.UseSqlServer(configs.GetConnectionString("TaskManagerConnection")));
        }

        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddTransient<ITeamRepository, TeamRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IProjectRepository, ProjectRepository>();
            services.AddTransient<ITaskRepository, TaskRepository>();

            services.AddTransient<ITaskUserProjectTeamUnitOfWork, TaskUserProjectTeamUnitOfWork>();

            services.AddTransient<ITeamService, TeamService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IProjectTaskService, ProjectTaskService>();
        }

        public static void RegisterCustomValidators(this IServiceCollection services)
        {
            services.AddSingleton<IValidator<TeamCreateDTO>, TeamCreateDTOValidator>();
            services.AddSingleton<IValidator<TeamDTO>, TeamDTOValidator>();

            services.AddSingleton<IValidator<UserCreateDTO>, UserCreateDTOValidator>();
            services.AddSingleton<IValidator<UserDTO>, UserDTOValidator>();

            services.AddSingleton<IValidator<ProjectTaskCreateDTO>, ProjectTaskCreateDTOValidator>();
            services.AddSingleton<IValidator<ProjectTaskDTO>, ProjectTaskDTOValidator>();

            services.AddSingleton<IValidator<ProjectCreateDTO>, ProjectCreateDTOValidator>();
            services.AddSingleton<IValidator<ProjectDTO>, ProjectDTOValidator>();
        }

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
                {
                    cfg.AddProfile<TeamProfile>();
                    cfg.AddProfile<ProjectProfile>();
                    cfg.AddProfile<ProjectTaskProfile>();
                    cfg.AddProfile<UserProfile>();
                },
                Assembly.GetExecutingAssembly());
        }

        public static void ConfigureCustomValidationErrors(this IServiceCollection services)
        {
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = (context) =>
                {
                    var errors = context.ModelState.Values.SelectMany(x => x.Errors.Select(p => p.ErrorMessage)).ToList();
                    var result = new
                    {
                        Message = "Validation errors",
                        Errors = errors
                    };

                    return new BadRequestObjectResult(result);
                };
            });
        }
    }
}
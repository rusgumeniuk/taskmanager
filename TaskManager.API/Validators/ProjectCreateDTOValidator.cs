﻿using FluentValidation;

using TaskManager.Common.DTO;

namespace TaskManager.API.Validators
{
    public sealed class ProjectCreateDTOValidator : AbstractValidator<ProjectCreateDTO>
    {
        public ProjectCreateDTOValidator()
        {
            RuleFor(project => project.Name)
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(100);

            RuleFor(project => project.Description)
                .NotEmpty()
                .MinimumLength(3);

            RuleFor(project => project.Deadline)
                .NotEmpty();

            RuleFor(project => project.TeamId)
                .NotEmpty();

            RuleFor(project => project.AuthorId)
                .NotEmpty();
        }
    }
}

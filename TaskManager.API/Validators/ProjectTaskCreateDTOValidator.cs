﻿using FluentValidation;

using TaskManager.Common.DTO;

namespace TaskManager.API.Validators
{
    public sealed class ProjectTaskCreateDTOValidator : AbstractValidator<ProjectTaskCreateDTO>
    {
        public ProjectTaskCreateDTOValidator()
        {
            RuleFor(task => task.Name)
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(100);

            RuleFor(task => task.Description)
                .NotEmpty()
                .MinimumLength(3);

            RuleFor(task => task.PerformerId)
                .NotEmpty();

            RuleFor(task => task.ProjectId)
                .NotEmpty();
        }
    }
}

﻿using FluentValidation;

using TaskManager.Common.DTO;

namespace TaskManager.API.Validators
{
    public class TeamCreateDTOValidator : AbstractValidator<TeamCreateDTO>
    {
        public TeamCreateDTOValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage("Name can't be empty")
                .MinimumLength(3)
                .MaximumLength(100);
        }
    }
}

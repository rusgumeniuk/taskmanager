﻿using FluentValidation;

using TaskManager.Common.DTO;

namespace TaskManager.API.Validators
{
    public sealed class ProjectTaskDTOValidator : AbstractValidator<ProjectTaskDTO>
    {
        public ProjectTaskDTOValidator()
        {
            RuleFor(task => task.Id)
                .NotEmpty();

            RuleFor(task => task.Name)
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(100);

            RuleFor(task => task.Description)
                .NotEmpty()
                .MinimumLength(3);

            RuleFor(task => task.PerformerId)
                .NotEmpty();

            RuleFor(task => task.ProjectId)
                .NotEmpty();

            RuleFor(task => task.State)
                .NotEmpty();

            RuleFor(task => task.FinishedAt)
                .NotEmpty();

            RuleFor(task => task.CreatedAt)
                .NotEmpty();
        }
    }
}

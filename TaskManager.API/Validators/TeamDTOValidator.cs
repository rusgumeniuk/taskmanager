﻿using FluentValidation;

using TaskManager.Common.DTO;

namespace TaskManager.API.Validators
{
    public sealed class TeamDTOValidator : AbstractValidator<TeamDTO>
    {
        public TeamDTOValidator()
        {
            RuleFor(team => team.Id)
                .NotEmpty();

            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage("Name can't be empty")
                .MinimumLength(3)
                .MaximumLength(100);

            RuleFor(x => x.CreatedAt)
                .NotEmpty()
                .WithMessage("Created date can't be empty");
        }
    }
}

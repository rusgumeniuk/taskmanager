﻿using FluentValidation;

using TaskManager.Common.DTO;

namespace TaskManager.API.Validators
{
    public sealed class ProjectDTOValidator : AbstractValidator<ProjectDTO>
    {
        public ProjectDTOValidator()
        {
            RuleFor(project => project.Id)
                .NotEmpty();

            RuleFor(project => project.Name)
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(100);

            RuleFor(project => project.Description)
                .NotEmpty()
                .MinimumLength(3);

            RuleFor(project => project.Deadline)
                .NotEmpty();

            RuleFor(project => project.TeamId)
                .NotEmpty();

            RuleFor(project => project.AuthorId)
                .NotEmpty();
        }
    }
}

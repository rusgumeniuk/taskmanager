﻿using FluentValidation;

using System.Net.Mail;

using TaskManager.Common.DTO;

namespace TaskManager.API.Validators
{
    public sealed class UserCreateDTOValidator : AbstractValidator<UserCreateDTO>
    {
        public UserCreateDTOValidator()
        {
            RuleFor(user => user.FirstName)
                .NotEmpty()
                .WithMessage("First name can't be empty")
                .MinimumLength(2)
                .MaximumLength(40);

            RuleFor(user => user.Email)
                .MaximumLength(320);

            RuleFor(user => new MailAddress(user.Email))
                .NotEmpty()
                .WithMessage("Wrong email");

            RuleFor(user => user.BirthDay)
                .NotEmpty()
                .WithMessage("Birth day can't be empty");
        }
    }
}

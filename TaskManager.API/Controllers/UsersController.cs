﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;

namespace TaskManager.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{id}")]
        public async Task<UserDTO> Get(int id)
        {
            return await _userService.GetAsync(id);
        }

        [HttpGet]
        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            return await _userService.GetAllAsync();
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Create([FromBody] UserCreateDTO dto)
        {
            try
            {
                var createdUser = await _userService.AddAsync(dto);
                return CreatedAtAction(nameof(Get), new { id = createdUser.Id }, createdUser);
            }
            catch (InvalidOperationException exception)
            {
                return BadRequest(exception.Message);
            }
        }

        [HttpPut]
        public async Task Update([FromBody] UserDTO dto)
        {
            await _userService.UpdateAsync(dto);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Remove(int id)
        {
            await _userService.RemoveAsync(id);
            return NoContent();
        }

        [HttpGet("GetOrderedByFirstNameAndSortedByTaskNameUsers")]
        public async Task<IEnumerable<UserTasksProfile>> GetOrderedByFirstNameAndSortedByTaskNameUsers()
        {
            return await _userService.GetUsersOrderedByFirstNameSortedByTaskNameAsync();
        }

        [HttpGet("exist/{id:int}")]
        public async Task<bool> ExistUser(int id)
        {
            return await _userService.ExistUserAsync(id);
        }

        [HttpGet("getUsersByProject/{projectId:int}")]
        public async Task<IEnumerable<UserDTO>> GetUsersByProject(int projectId)
        {
            return await _userService.GetUsersByProjectId(projectId);
        }
    }
}

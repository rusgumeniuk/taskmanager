﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;

namespace TaskManager.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet("{id}")]
        public async Task<ProjectDTO> Get(int id)
        {
            return await _projectService.GetAsync(id);
        }

        [HttpGet]
        public async Task<IEnumerable<ProjectDTO>> GetAll()
        {
            return await _projectService.GetAllAsync();
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Create([FromBody] ProjectCreateDTO dto)
        {
            try
            {
                var createdProject = await _projectService.AddAsync(dto);
                return CreatedAtAction(nameof(Get), new { id = createdProject.Id }, createdProject);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task Update([FromBody] ProjectDTO dto)
        {
            await _projectService.UpdateAsync(dto);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Remove(int id)
        {
            await _projectService.RemoveAsync(id);
            return NoContent();
        }

        [HttpGet("getNumberOfTasks/{userId}")]
        public async Task<IEnumerable<ProjectTaskCountDTO>> GetNumberOfTasksOfUser(int userId)
        {
            var result = await _projectService.GetNumberOfTasksOfUserAsync(userId);
            return result.Select(item => new ProjectTaskCountDTO() { CountOfTasks = item.Value, Project = item.Key });
        }

        [HttpGet("GetProjectInfo")]
        public async Task<IEnumerable<ProjectTasksStaffInfo>> GetProjectInfo()
        {
            return await _projectService.GetProjectInfoAsync();
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;

namespace TaskManager.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IProjectTaskService _projectTaskService;

        public TasksController(IProjectTaskService projectTaskService)
        {
            _projectTaskService = projectTaskService;
        }

        [HttpGet("{id}")]
        public async Task<ProjectTaskDTO> Get(int id)
        {
            return await _projectTaskService.GetAsync(id);
        }

        [HttpGet]
        public async Task<IEnumerable<ProjectTaskDTO>> GetAll()
        {
            return await _projectTaskService.GetAllAsync();
        }

        [HttpPost]
        public async Task<ActionResult<ProjectTaskDTO>> Create([FromBody] ProjectTaskCreateDTO dto)
        {
            try
            {
                var createdTask = await _projectTaskService.AddAsync(dto);
                return CreatedAtAction(nameof(Get), new { id = createdTask.Id }, createdTask);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        public async Task Update([FromBody] ProjectTaskDTO dto)
        {
            await _projectTaskService.UpdateAsync(dto);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Remove(int id)
        {
            await _projectTaskService.RemoveAsync(id);
            return NoContent();
        }

        [HttpGet("getTasksWithSmallName/{userId:int}")]
        public async Task<IEnumerable<ProjectTaskDTO>> GetTasksOfUserWithTaskNameLessThan45(int userId)
        {
            return await _projectTaskService.GetTasksOfUserWithTaskNameLessThan45Async(userId);
        }

        [HttpGet("getFinishedInThisYearByUser/{userId:int}")]
        public async Task<IEnumerable<TaskShortInfo>> GetFinishedTasksInTheCurrentYearByUser(int userId)
        {
            return await _projectTaskService.GetFinishedTasksInTheCurrentYearByUserAsync(userId);
        }

        [HttpGet("getActiveTasks/{userId:int}")]
        public async Task<UserTasksProfile> GetActiveTasks(int userId)
        {
            return await _projectTaskService.GetActiveUserTasksAsync(userId);
        }

        [HttpGet("getUserStatistics/{userId:int}")]
        public async Task<UserProfileWithLastProjectAndLongestTaskAndTaskStatistics> GetUserInfoAboutProjectAndTasks(int userId)
        {
            return await _projectTaskService.GetUserInfoAsync(userId);
        }

        [HttpPut("finishRandomTask")]
        public async Task<int> FinishRandomProjectTask()
        {
            return await _projectTaskService.FinishRandomTask();
        }
    }
}

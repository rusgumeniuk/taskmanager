﻿using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

using TaskManager.API.Filters;
using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;

namespace TaskManager.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [CustomExceptionFilter]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet("{id}")]
        public async Task<TeamDTO> Get(int id)
        {
            return await _teamService.GetAsync(id);
        }

        [HttpGet]
        public async Task<IEnumerable<TeamDTO>> GetAll()
        {
            return await _teamService.GetAllAsync();
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Create([FromBody] TeamCreateDTO dto)
        {
            var createdTeam = await _teamService.AddAsync(dto);
            return CreatedAtAction(nameof(Get), new { id = createdTeam.Id }, createdTeam);
        }

        [HttpPut]
        public async Task Update([FromBody] TeamDTO dto)
        {
            await _teamService.UpdateAsync(dto);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Remove(int id)
        {
            await _teamService.RemoveAsync(id);
            return NoContent();
        }

        [HttpGet("getTeamsWithOldMember")]
        public async Task<IEnumerable<TeamWithMembersDTO>> GetTeamsWithMembersOlderThan10OrderedByRegistrationTime()
        {
            return await _teamService.GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTimeAsync();
        }
    }
}

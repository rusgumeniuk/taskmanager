import { Routes } from '@angular/router';

import { UsersComponent } from './modules/user/components/users/users.component';
import { TeamsComponent } from './modules/team/components/teams/teams.component';
import { TasksComponent } from './modules/task/components/tasks/tasks.component';
import { ProjectsComponent } from './modules/project/components/projects/projects.component';

export const AppRoutes: Routes = [
  { path: 'users', component: UsersComponent, pathMatch: 'full' },
  { path: 'teams', component: TeamsComponent, pathMatch: 'full' },
  { path: 'tasks', component: TasksComponent, pathMatch: 'full' },
  { path: 'projects', component: ProjectsComponent, pathMatch: 'full' },  
  { path: '**', redirectTo: '' },
];

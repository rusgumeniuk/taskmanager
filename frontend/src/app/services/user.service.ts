import { CrudService } from './crud.service';

import { User } from '../models/user/user';
import { NewUser } from '../models/user/new-user';
import { HttpInternalService } from './http-internal.service';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UserService extends CrudService<User, NewUser> {
  constructor(protected httpService: HttpInternalService) {
    super(httpService, 'User');
  }

  public getUsersByProjectId(projectId: number) {
    return this.httpService.getFullRequest<User[]>(
      `${this.route}/getUsersByProject/${projectId}`
    );
  }
}

import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';

@Injectable({ providedIn: 'root' })
export abstract class CrudService<T extends object, TCreate extends object> {
  protected route = '/api';

  constructor(protected httpService: HttpInternalService, protected entityName: string, routePrefix?: string) {
    this.route += routePrefix ?? `/${entityName}s`;
  }

  public getAll() {
    return this.httpService.getFullRequest<T[]>(`${this.route}`);
  }

  public get(id: number) {
    return this.httpService.getFullRequest<T>(`${this.route}/${id}`);
  }

  public create(obj: TCreate) {
    return this.httpService.postFullRequest<T>(`${this.route}`, obj);
  }

  public update(obj: T) {
    return this.httpService.putFullRequest<T>(`${this.route}`, obj);
  }

  public delete(id: number) {
    return this.httpService.deleteFullRequest(`${this.route}/${id}`);
  }
}

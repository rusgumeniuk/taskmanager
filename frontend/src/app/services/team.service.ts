import { CrudService } from './crud.service';

import { Team } from '../models/team/team';
import { NewTeam } from '../models/team/new-team';
import { HttpInternalService } from './http-internal.service';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class TeamService extends CrudService<Team, NewTeam> {
  constructor(protected httpService: HttpInternalService) {
    super(httpService, 'Team');
  }
}

import { CrudService } from './crud.service';

import { Task } from '../models/task/task';
import { NewTask } from '../models/task/new-task';
import { HttpInternalService } from './http-internal.service';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class TaskService extends CrudService<Task, NewTask> {
  constructor(protected httpService: HttpInternalService) {
    super(httpService, 'Task');
  }
}

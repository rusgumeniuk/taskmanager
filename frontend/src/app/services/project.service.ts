import { CrudService } from './crud.service';

import { Project } from '../models/project/project';
import { NewProject } from '../models/project/new-project';
import { HttpInternalService } from './http-internal.service';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ProjectService extends CrudService<Project, NewProject> {
  constructor(protected httpService: HttpInternalService) {
    super(httpService, "Project");
  }
}

import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({ providedIn: 'root' })
export class SnackBarService {  
  public constructor(private snackBar: MatSnackBar) {}

  public showErrorMessage(error: Error) {
    this.snackBar.open(error.message, '', {
      duration: 3000,
      panelClass: 'error-snack-bar',
    });    
    console.log(error);
  }

  public showUsualMessage(message: string) {
    this.snackBar.open(message, '', {
      duration: 3000,
      panelClass: 'warning-snack-bar',
    });    
    console.log(message);
  }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ukrDate',
})
export class UkrainianDatePipe implements PipeTransform {
  transform(value: Date): string {
    let parsedDate = new Date(value);
    if (parsedDate) {
      let month = parsedDate.toLocaleDateString('uk-UA', { month: 'long' });      
      return `${parsedDate.getDate()} ${month} ${parsedDate.getFullYear()}`;
    }

    return value.toLocaleString('uk-UA');
  }
}

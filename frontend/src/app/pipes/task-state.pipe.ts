import { Pipe, PipeTransform } from '@angular/core';
import { TaskState } from '../models/task/task-state';

@Pipe({
  name: 'taskState',
})
export class TaskStatePipe implements PipeTransform {
  transform(value: number): string {    
    if (value <= TaskState.Canceled) {
        return TaskState[value];
    }
    return value.toString();
  }
}

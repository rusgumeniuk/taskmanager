import { Routes } from '@angular/router';

import { UsersComponent } from '../user/components/users/users.component';
import { TeamsComponent } from '../team/components/teams/teams.component';
import { TasksComponent } from '../task/components/tasks/tasks.component';
import { ProjectsComponent } from '../project/components/projects/projects.component';

import { EditUserComponent } from '../user/components/edit-user/edit-user.component';
import { EditTeamComponent } from '../team/components/edit-team/edit-team.component';
import { EditTaskComponent } from '../task/components/edit-task/edit-task.component';
import { EditProjectComponent } from '../project/components/edit-project/edit-project.component';

import { AddTaskComponent } from '../task/components/add-task/add-task.component';
import { AddTeamComponent } from '../team/components/add-team/add-team.component';
import { AddUserComponent } from '../user/components/add-user/add-user.component';
import { AddProjectComponent } from '../project/components/add-project/add-project.component';

import { UserDetailsComponent } from '../user/components/user-details/user-details.component';
import { TeamDetailsComponent } from '../team/components/team-details/team-details.component';
import { TaskDetailsComponent } from '../task/components/task-details/task-details.component';
import { ProjectDetailsComponent } from '../project/components/project-details/project-details.component';
import { ExitAboutGuard } from 'src/app/guards/exit.about.guard';

export const SharedRoutes: Routes = [
  { path: '', redirectTo: 'tasks', pathMatch: 'full' },
  { path: 'users', component: UsersComponent, pathMatch: 'full' },
  {
    path: 'user-details/:id',
    component: UserDetailsComponent,
  },
  {
    path: 'add-user',
    component: AddUserComponent,
    canDeactivate: [ExitAboutGuard],
  },
  {
    path: 'edit-user/:id',
    component: EditUserComponent,
    canDeactivate: [ExitAboutGuard],
  },
  { path: 'teams', component: TeamsComponent, pathMatch: 'full' },
  {
    path: 'team-details/:id',
    component: TeamDetailsComponent,
  },
  {
    path: 'add-team',
    component: AddTeamComponent,
    canDeactivate: [ExitAboutGuard],
  },
  {
    path: 'edit-team/:id',
    component: EditTeamComponent,
    canDeactivate: [ExitAboutGuard],
  },
  { path: 'tasks', component: TasksComponent, pathMatch: 'full' },
  {
    path: 'task-details/:id',
    component: TaskDetailsComponent,
  },
  {
    path: 'add-task',
    component: AddTaskComponent,
    canDeactivate: [ExitAboutGuard],
  },
  {
    path: 'edit-task/:id',
    component: EditTaskComponent,
    canDeactivate: [ExitAboutGuard],
  },
  { path: 'projects', component: ProjectsComponent, pathMatch: 'full' },
  {
    path: 'project-details/:id',
    component: ProjectDetailsComponent,
  },
  {
    path: 'add-project',
    component: AddProjectComponent,
    canDeactivate: [ExitAboutGuard],
  },
  {
    path: 'edit-project/:id',
    component: EditProjectComponent,
    canDeactivate: [ExitAboutGuard],
  },
  { path: '**', redirectTo: '' },
];

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ListComponent } from './list/list.component';
import { RouterModule } from '@angular/router';
import { SharedRoutes } from './shared.routes';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UkrainianDatePipe } from '../../pipes/ukr-date.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExitAboutGuard } from 'src/app/guards/exit.about.guard';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    ListComponent,
    UkrainianDatePipe,
  ],
  exports: [FooterComponent, HeaderComponent, ListComponent, UkrainianDatePipe],
  imports: [
    CommonModule,
    RouterModule.forRoot(SharedRoutes),
    MatListModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [ExitAboutGuard],
})
export class SharedModule {}

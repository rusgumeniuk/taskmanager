import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NamedEntity } from '../../../models/named-entity';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent {
  @Input()
  public items: NamedEntity[];

  @Input()
  public entityName: string;

  @Output()
  public onDelete = new EventEmitter();

  constructor(private router: Router) {}

  public deleteEntity(id: number): void {
    this.onDelete.emit(id);
  }

  public detailsEntity(id: number): void {
    this.router.navigate([`${this.entityName}-details/${id}`]);
  }

  public editEntity(id: number): void {
    this.router.navigate([`edit-${this.entityName}/${id}`]);
  }

  public addEntity(): void {
    this.router.navigate([`add-${this.entityName}`]);
  }
}

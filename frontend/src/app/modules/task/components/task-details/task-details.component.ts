import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task/task';
import { Project } from 'src/app/models/project/project';
import { User } from 'src/app/models/user/user';
import { TaskService } from 'src/app/services/task.service';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { TaskState } from 'src/app/models/task/task-state';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css'],
})
export class TaskDetailsComponent implements OnInit {
  task: Task;
  project: Project;
  performer: User;
  snackBarService: SnackBarService;

  constructor(
    private taskService: TaskService,
    private projectService: ProjectService,
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let taskId: number = Number(this.route.snapshot.paramMap.get('id'));
    this.updateInfo(taskId);           
  }

  updateInfo(taskId: number) {
    this.taskService.get(taskId).subscribe(
      (response) => {
        this.task = response.body;

        this.projectService.get(this.task.projectId).subscribe(
          (response) => {
            this.project = response.body;
          },
          (error) => this.snackBarService.showErrorMessage(error)
        );

        this.userService.get(this.task.performerId).subscribe(
          (response) => {
            this.performer = response.body;
            this.performer.name = `${this.performer.firstName} ${this.performer.lastName}`;
          },
          (error) => this.snackBarService.showErrorMessage(error)
        );
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  getTask(taskId: number) {
    this.taskService.get(taskId).subscribe(
      (response) => {
        this.task = response.body;        
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  getTaskProject(projectId: number) {
    this.projectService.get(projectId).subscribe(
      (response) => {
        this.project = response.body;        
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  getTaskPerformer(performerId: number) {
    this.userService.get(performerId).subscribe(
      (response) => {
        this.performer = response.body;
        this.performer.name = `${this.performer.firstName} ${this.performer.lastName}`;        
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
}

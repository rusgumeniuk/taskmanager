import { Component, OnInit } from '@angular/core';
import { Subject, from } from 'rxjs';
import { NamedEntity } from 'src/app/models/named-entity';
import { takeUntil } from 'rxjs/operators';
import { Task } from '../../../../models/task/task';
import { TaskService } from '../../../../services/task.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
})
export class TasksComponent implements OnInit {
  public tasks: Task[] = [];
  cachedTasks: NamedEntity[] = [];

  public loading = false;
  public loadingTasks = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private snackBarService: SnackBarService,
    private taskService: TaskService
  ) {}

  public ngOnInit(): void {
    this.getTasks();
  }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public getTasks() {
    this.loadingTasks = true;
    this.taskService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.loadingTasks = false;
          this.tasks = this.cachedTasks = resp.body;
        },
        (error) => {
          this.loadingTasks = false;
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  deleteTask(id: number) {
    this.taskService
      .delete(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.snackBarService.showUsualMessage('Task was deleted!');
          this.cachedTasks = this.cachedTasks.filter((item) => item.id !== id);
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }
}

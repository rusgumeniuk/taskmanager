import { Component, OnInit, Input } from '@angular/core';
import { NewTask } from 'src/app/models/task/new-task';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Router } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
import { User } from 'src/app/models/user/user';
import { UserService } from 'src/app/services/user.service';
import { Project } from 'src/app/models/project/project';
import { HttpErrorResponse } from '@angular/common/http';
import { ComponentCanDeactivate } from 'src/app/guards/exit.about.guard';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
})
export class AddTaskComponent implements OnInit, ComponentCanDeactivate {
  @Input()
  task: NewTask = {} as NewTask;

  projects: Project[];
  users: User[];

  isCreated: boolean = false;
  taskForm: FormGroup;

  isProjectSelected: boolean = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private taskService: TaskService,
    private projectService: ProjectService,
    private userService: UserService,
    private snackBarService: SnackBarService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.taskForm = new FormGroup({
      name: new FormControl(this.task.name, [
        Validators.required,
        Validators.minLength(3),
      ]),
      description: new FormControl(this.task.description, [
        Validators.required,
      ]),
      projectId: new FormControl(this.task.projectId, [Validators.required]),
      performerId: new FormControl(this.task.performerId, [
        Validators.required,
      ]),
    });

    this.onChanges();

    this.updateInfo();
  }

  onChanges() {
    const projectFormControl = this.taskForm.get('projectId');

    projectFormControl.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => {
        this.resetSelectedProjectAndUsers();

        const projectId = +value;
        projectFormControl.setValue(projectId);
        this.getUsersByProjectId(projectId);
      });

    const performerFormControl = this.taskForm.get('performerId');

    performerFormControl.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => performerFormControl.setValue(+value));
  }

  updateInfo(): void {
    this.getProjects();
  }

  public getProjects() {
    this.projectService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.projects = resp.body;
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }

  public getUsersByProjectId(projectId: number) {
    this.userService
      .getUsersByProjectId(projectId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          const usersWithoutName = resp.body;
          usersWithoutName.forEach(
            (user) => (user.name = `${user.firstName} ${user.lastName}`)
          );
          this.users = usersWithoutName;
          this.isProjectSelected = true;
        },
        (error: HttpErrorResponse) => {
          if (error.message.includes('getUsersByProject')) {
            const newError = new Error(
              "Team of this project doesn't contain any users so in this project no one can perform any task. Please choose another project."
            );
            this.snackBarService.showErrorMessage(newError);
          } else {
            this.snackBarService.showErrorMessage(error);
          }
        }
      );
  }

  resetSelectedProjectAndUsers(): void {
    this.taskForm.get('performerId').reset();
    this.isProjectSelected = false;
  }

  isInvalidControl(controlName: string): boolean {
    const control = this.taskForm.controls[controlName];
    return control.invalid && control.touched;
  }

  createTask(): void {
    const controls = this.taskForm.controls;

    if (this.isInvalidForm()) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const newTask = this.taskForm.value as NewTask;
    this.taskService
      .create(newTask)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.isCreated = true;
          this.snackBarService.showUsualMessage('Task created!');
          this.router.navigate([`task-details/${resp.body.id}`]);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  isInvalidForm(): boolean {
    if (this.taskForm.invalid) return true;
    if (this.taskForm.get('performerId').value == 0) return true;
    return false;
  }

  backToList(): void {
    this.router.navigate([`tasks`]);
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.taskForm.untouched ||  this.isCreated
      ? true
      : confirm('Do you wanna leave this page? All unsaved data will be lost.');
  }
}

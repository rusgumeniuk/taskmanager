import { Component, OnInit, Input } from '@angular/core';
import { NewTask } from 'src/app/models/task/new-task';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Router, ActivatedRoute, CanDeactivate } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
import { User } from 'src/app/models/user/user';
import { UserService } from 'src/app/services/user.service';
import { Project } from 'src/app/models/project/project';
import { HttpErrorResponse } from '@angular/common/http';
import { Task } from 'src/app/models/task/task';
import { ComponentCanDeactivate } from 'src/app/guards/exit.about.guard';
import { TaskState } from 'src/app/models/task/task-state';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css'],
})
export class EditTaskComponent implements OnInit, ComponentCanDeactivate {
  @Input()
  task: Task = {} as Task;
  projects: Project[];
  users: User[];
  taskStates: TaskState[];

  isUpdated: boolean = false;
  taskForm: FormGroup;

  isProjectSelected: boolean = true;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private taskService: TaskService,
    private projectService: ProjectService,
    private userService: UserService,
    private snackBarService: SnackBarService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let taskId: number = Number(this.route.snapshot.paramMap.get('id'));

    this.taskForm = new FormGroup({
      name: new FormControl(this.task.name, [
        Validators.required,
        Validators.minLength(3),
      ]),
      description: new FormControl(this.task.description, [
        Validators.required,
      ]),
      projectId: new FormControl(this.task.projectId, [Validators.required]),
      performerId: new FormControl(this.task.performerId, [
        Validators.required,
      ]),
      finishedAt: new FormControl(this.task.finishedAt, [Validators.required]),
      state: new FormControl(this.task.state, [Validators.required]),
    });

    this.onChanges();
    this.updateInfo(taskId);
  }

  updateAvailableStates(): void {
    const keys = Object.keys(TaskState);

    this.taskStates = keys
      .splice(0, keys.length / 2)
      .map((key) => Number(key))
      .filter((key) => key >= this.task.state?.valueOf());

    this.taskForm.get('state').setValue(this.task.state);
  }

  onChanges() {
    const projectFormControl = this.taskForm.get('projectId');

    projectFormControl.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => {
        this.resetSelectedProjectAndUsers();

        const projectId = +value;
        projectFormControl.setValue(projectId);
        this.getUsersByProjectId(projectId);
      });

    const performerFormControl = this.taskForm.get('performerId');

    performerFormControl.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => performerFormControl.setValue(+value));
  }

  //#region Retieve data
  updateInfo(taskId: number): void {
    this.getProjects();
    this.getTask(taskId);
  }

  public getProjects() {
    this.projectService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.projects = resp.body;
          if (
            this.taskForm.get('projectId').value == null &&
            this.task?.projectId
          ) {
            this.taskForm.get('projectId').setValue(this.task.projectId);
          }
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }

  public getUsersByProjectId(projectId: number) {
    this.userService
      .getUsersByProjectId(projectId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          const usersWithoutName = resp.body;
          usersWithoutName.forEach(
            (user) => (user.name = `${user.firstName} ${user.lastName}`)
          );
          this.users = usersWithoutName;
          this.isProjectSelected = true;

          this.taskForm.get('performerId').setValue(this.task.performerId);
        },
        (error: HttpErrorResponse) => {
          if (error.message.includes('getUsersByProject')) {
            const newError = new Error(
              "Team of this project doesn't contain any users so in this project no one can perform any task. Please choose another project."
            );
            this.snackBarService.showErrorMessage(newError);
          } else {
            this.snackBarService.showErrorMessage(error);
          }
        }
      );
  }

  resetSelectedProjectAndUsers(): void {
    this.taskForm.get('performerId').reset();
    this.isProjectSelected = false;
  }

  getTask(taskId: number) {
    this.taskService.get(taskId).subscribe(
      (response) => {
        this.task = response.body;

        this.taskForm.get('name').setValue(this.task.name);
        this.taskForm.get('description').setValue(this.task.description);

        const date = new Date(this.task.finishedAt);
        const offsetInMiliseconds = date.getTimezoneOffset() * 60000;
        const isoDateTime = new Date(date.getTime() - offsetInMiliseconds)
          .toISOString()
          .split('T');
        this.taskForm.get('finishedAt').setValue(isoDateTime[0]);
        this.taskForm.get('projectId').setValue(this.task.projectId);

        this.getUsersByProjectId(this.task.projectId);
        this.updateAvailableStates();
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
  //#endregion

  //#region Validation
  isInvalidControl(controlName: string): boolean {
    const control = this.taskForm?.controls[controlName];
    return !control || control.invalid;
  }

  isInvalidForm(): boolean {
    if (!this.isProjectSelected) {
      return true;
    }
    if (this.taskForm.invalid) return true;
    return false;
  }
  //#endregion

  updateTask(): void {
    const controls = this.taskForm.controls;

    if (this.isInvalidForm()) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const updatedTask = this.taskForm.value as Task;

    updatedTask.createdAt = this.task.createdAt;
    updatedTask.id = this.task.id;
    updatedTask.state = Number(updatedTask.state);

    this.taskService
      .update(updatedTask)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.isUpdated = true;
          this.snackBarService.showUsualMessage('Task updated!');
          this.router.navigate([`task-details/${this.task.id}`]);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  backToList(): void {
    this.router.navigate([`tasks`]);
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.taskForm.untouched || this.isUpdated
      ? true
      : confirm('Do you wanna leave this page? All unsaved data will be lost.');
  }
}

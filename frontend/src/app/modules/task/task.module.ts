import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksComponent } from './components/tasks/tasks.component';
import { EditTaskComponent } from './components/edit-task/edit-task.component';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { TaskDetailsComponent } from './components/task-details/task-details.component';
import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { TaskStatePipe } from '../../pipes/task-state.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { TaskStateDirective } from 'src/app/directives/task-state.directive';

@NgModule({
  declarations: [
    TasksComponent,
    EditTaskComponent,
    AddTaskComponent,
    TaskDetailsComponent,
    TaskStatePipe,
    TaskStateDirective,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,    
  ],
  exports: [TaskStatePipe],
})
export class TaskModule {}

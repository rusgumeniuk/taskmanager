import { Component, OnInit, Input } from '@angular/core';
import { NewUser } from 'src/app/models/user/new-user';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
} from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { takeUntil, distinct } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Router } from '@angular/router';
import { Team } from 'src/app/models/team/team';
import { TeamService } from 'src/app/services/team.service';
import { ComponentCanDeactivate } from 'src/app/guards/exit.about.guard';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
})
export class AddUserComponent implements OnInit, ComponentCanDeactivate {
  @Input()
  user: NewUser = {} as NewUser;

  teams: Team[];

  isCreated: boolean = false;
  userForm: FormGroup;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService,
    private teamService: TeamService,
    private snackBarService: SnackBarService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, [
        Validators.required,
        Validators.minLength(3),
      ]),
      birthDay: new FormControl(this.user.birthDay, [
        Validators.required,
        this.isDateBeforeNowValidator,
      ]),
      email: new FormControl(this.user.email, [
        Validators.required,
        Validators.email,
      ]),
      lastName: new FormControl(this.user.lastName),
      teamId: new FormControl(this.user.teamId),
    });

    const teamFormControl = this.userForm.get('teamId');

    teamFormControl.valueChanges
      .pipe(distinct())
      .subscribe((value) => teamFormControl.setValue(+value || null));

    this.getTeams();
  }

  isDateBeforeNowValidator(control: AbstractControl) {
    if (
      control.value !== null &&
      control.value !== undefined &&
      new Date(control.value).getTime() < Date.now()
    ) {
      return null;
    }
    return { isDateBeforeNow: false };
  }

  public getTeams() {
    this.teamService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = resp.body;
          this.teams.splice(0, 0, null);
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }

  isInvalidControl(controlName: string): boolean {
    const control = this.userForm.controls[controlName];
    return control.invalid && control.touched;
  }

  createUser(): void {
    const controls = this.userForm.controls;

    if (this.userForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const newUser = this.userForm.value as NewUser;

    this.userService
      .create(newUser)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.isCreated = true;
          this.snackBarService.showUsualMessage('User created!');
          this.router.navigate([`user-details/${resp.body.id}`]);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  backToList(): void {
    this.router.navigate([`users`]);
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.userForm.untouched ||  this.isCreated
      ? true
      : confirm('Do you wanna leave this page? All unsaved data will be lost.');
  }
}

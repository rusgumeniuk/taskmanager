import { Component, OnInit, Input } from '@angular/core';
import { NewUser } from 'src/app/models/user/new-user';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { takeUntil, distinct } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Team } from 'src/app/models/team/team';
import { TeamService } from 'src/app/services/team.service';
import { User } from 'src/app/models/user/user';
import { ComponentCanDeactivate } from 'src/app/guards/exit.about.guard';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css'],
})
export class EditUserComponent implements OnInit, ComponentCanDeactivate {
  @Input()
  user: User = {} as User;
  teams: Team[];

  isUpdated: boolean = false;
  userForm: FormGroup;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService,
    private teamService: TeamService,
    private snackBarService: SnackBarService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let userId: number = Number(this.route.snapshot.paramMap.get('id'));
    this.updateInfo(userId);

    this.userForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, [
        Validators.required,
        Validators.minLength(3),
      ]),
      birthDay: new FormControl(this.user.birthDay, [
        Validators.required,
        this.isDateBeforeNowValidator,
      ]),
      email: new FormControl(this.user.email, [
        Validators.required,
        Validators.email,
      ]),
      lastName: new FormControl(this.user.lastName),
      teamId: new FormControl(this.user.teamId),
    });

    const teamFormControl = this.userForm.get('teamId');

    teamFormControl.valueChanges
      .pipe(distinct())
      .subscribe((value) => teamFormControl.setValue(+value || null));

    this.getTeams();
  }

  isDateBeforeNowValidator(control: AbstractControl) {
    if (
      control.value !== null &&
      control.value !== undefined &&
      new Date(control.value).getTime() < Date.now()
    ) {
      return null;
    }
    return { isDateBeforeNow: false };
  }

  public getTeams() {
    this.teamService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = resp.body;
          this.teams.splice(0, 0, null);
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }

  isInvalidControl(controlName: string): boolean {
    const control = this.userForm.controls[controlName];
    return control.invalid && control.touched;
  }

  updateInfo(userId: number) {
    this.getUser(userId);
  }

  getUser(userId: number) {
    this.userService.get(userId).subscribe(
      (response) => {
        this.user = response.body;
        this.userForm.get('firstName').setValue(this.user.firstName);
        this.userForm.get('lastName').setValue(this.user.lastName);
        this.userForm.get('email').setValue(this.user.email);

        const date = new Date(this.user.birthDay);
        const offsetInMiliseconds = date.getTimezoneOffset() * 60000;
        const isoDateTime = new Date(date.getTime() - offsetInMiliseconds)
          .toISOString()
          .split('T');
        this.userForm.get('birthDay').setValue(isoDateTime[0]);

        if (this.user.teamId && this.userForm.get('teamId').value == null) {          
          this.userForm.get('teamId').setValue(this.user.teamId);          
        }
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  updateUser(): void {
    const controls = this.userForm.controls;

    if (this.userForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const updatedUser = this.userForm.value as User;
    updatedUser.id = this.user.id;
    updatedUser.registeredAt = this.user.registeredAt;

    this.userService
      .update(updatedUser)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.isUpdated = true;
          this.snackBarService.showUsualMessage('User updated!');
          this.router.navigate([`user-details/${this.user.id}`]);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  backToList(): void {
    this.router.navigate([`users`]);
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.userForm.untouched || this.isUpdated
      ? true
      : confirm('Do you wanna leave this page? All unsaved data will be lost.');
  }
}

import { Component, OnInit } from '@angular/core';
import { Subject, from } from 'rxjs';
import { NamedEntity } from 'src/app/models/named-entity';
import { takeUntil } from 'rxjs/operators';
import { User } from '../../../../models/user/user';
import { UserService } from '../../../../services/user.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  public users: User[] = [];
  cachedUsers: NamedEntity[] = [];

  public loading = false;
  public loadingUsers = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService,
    private snackBarService: SnackBarService
  ) {}

  public ngOnInit(): void {
    this.getUsers();
  }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public getUsers() {
    this.loadingUsers = true;
    this.userService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.loadingUsers = false;
          let usersWithoutName = resp.body;
          usersWithoutName.forEach(
            (user) => (user.name = `${user.firstName} ${user.lastName}`)
          );
          this.users = this.cachedUsers = usersWithoutName;
        },
        (error) => {
          this.loadingUsers = false;
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  deleteUser(id: number) {
    this.userService
      .delete(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.snackBarService.showUsualMessage('User was deleted!');
          this.cachedUsers = this.cachedUsers.filter((item) => item.id !== id);
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }
}

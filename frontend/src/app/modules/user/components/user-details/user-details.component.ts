import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user/user';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { TeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team/team';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
})
export class UserDetailsComponent implements OnInit {
  user: User;
  team: Team;
  snackBarService: SnackBarService;

  constructor(
    private teamService: TeamService,
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let userId: number = Number(this.route.snapshot.paramMap.get('id'));
    this.updateInfo(userId);
  }

  updateInfo(userId: number) {
    this.userService.get(userId).subscribe(
      (response) => {
        this.user = response.body;
        this.user.name = `${this.user.firstName} ${this.user.lastName}`;

        if (this.user.teamId) {
          this.teamService.get(this.user.teamId).subscribe(
            (response) => {
              this.team = response.body;
            },
            (error) => this.snackBarService.showErrorMessage(error)
          );
        }
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  getUser(userId: number) {
    this.userService.get(userId).subscribe(
      (response) => {
        this.user = response.body;
        this.user.name = `${this.user.firstName} ${this.user.lastName}`;
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  getUserTeam(teamId: number) {
    if (!teamId) return;
    this.teamService.get(teamId).subscribe(
      (response) => {
        this.team = response.body;
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/services/project.service';
import { takeUntil, distinct } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Team } from 'src/app/models/team/team';
import { TeamService } from 'src/app/services/team.service';
import { Project } from 'src/app/models/project/project';
import { ComponentCanDeactivate } from 'src/app/guards/exit.about.guard';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css'],
})
export class EditProjectComponent implements OnInit, ComponentCanDeactivate {
  @Input()
  project: Project = {} as Project;
  teams: Team[];

  isUpdated: boolean = false;
  projectForm: FormGroup;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private projectService: ProjectService,
    private teamService: TeamService,
    private snackBarService: SnackBarService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let projectId: number = Number(this.route.snapshot.paramMap.get('id'));

    this.projectForm = new FormGroup({
      name: new FormControl(this.project.name, [
        Validators.required,
        Validators.minLength(3),
      ]),
      deadline: new FormControl(this.project.deadline, [Validators.required]),
      description: new FormControl(this.project.description, [
        Validators.required,
      ]),
      teamId: new FormControl(this.project.teamId, [Validators.required]),
    });

    const teamFormControl = this.projectForm.get('teamId');

    teamFormControl.valueChanges
      .pipe(distinct())
      .subscribe((value) => teamFormControl.setValue(+value));

    this.updateInfo(projectId);
  }

  updateInfo(projectId: number): void {
    this.getTeams();
    this.getProject(projectId);
  }

  getProject(projectId: number) {
    this.projectService.get(projectId).subscribe(
      (response) => {
        this.project = response.body;
        this.projectForm.get('name').setValue(this.project.name);
        this.projectForm.get('description').setValue(this.project.description);
        const date = new Date(this.project.deadline);
        const offsetInMiliseconds = date.getTimezoneOffset() * 60000;
        const isoDateTime = new Date(date.getTime() - offsetInMiliseconds)
          .toISOString()
          .split('T');
        this.projectForm.get('deadline').setValue(isoDateTime[0]);
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  public getTeams() {
    this.teamService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = resp.body;
          if (this.projectForm.get('teamId').value == null) {
            this.projectForm.get('teamId').setValue(this.project.teamId);
          }
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }

  isInvalidControl(controlName: string): boolean {
    const control = this.projectForm.controls[controlName];
    return control.invalid && control.touched;
  }

  updateProject(): void {
    const controls = this.projectForm.controls;

    if (this.projectForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const updatedProject = this.projectForm.value as Project;
    updatedProject.authorId = this.project.authorId;
    updatedProject.id = this.project.id;
    this.projectService
      .update(updatedProject)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.isUpdated = true;
          this.snackBarService.showUsualMessage('Project updated!');
          this.router.navigate([`project-details/${this.project.id}`]);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  backToList(): void {
    this.router.navigate([`projects`]);
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.projectForm.untouched || this.isUpdated
      ? true
      : confirm('Do you wanna leave this page? All unsaved data will be lost.');
  }
}

import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project/project';
import { Team } from 'src/app/models/team/team';
import { User } from 'src/app/models/user/user';
import { ProjectService } from 'src/app/services/project.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css'],
})
export class ProjectDetailsComponent implements OnInit {
  project: Project;
  team: Team;
  author: User;
  snackBarService: SnackBarService;

  constructor(
    private projectService: ProjectService,
    private teamService: TeamService,
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let projectId: number = Number(this.route.snapshot.paramMap.get('id'));
    this.updateInfo(projectId);
  }

  updateInfo(projectId: number) {
    this.projectService.get(projectId).subscribe(
      (response) => {
        this.project = response.body;

        this.teamService.get(this.project.teamId).subscribe(
          (response) => {
            this.team = response.body;
          },
          (error) => this.snackBarService.showErrorMessage(error)
        );

        this.userService.get(this.project.authorId).subscribe(
          (response) => {
            this.author = response.body;
            this.author.name = `${this.author.firstName} ${this.author.lastName}`;
          },
          (error) => this.snackBarService.showErrorMessage(error)
        );
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  getProject(projectId: number) {
    this.projectService.get(projectId).subscribe(
      (response) => {
        this.project = response.body;        
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
  getProjectTeam(teamId: number) {    
    this.teamService.get(teamId).subscribe(
      (response) => {
        this.team = response.body;        
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
  getProjectAuthor(authorId: number) {    
    this.userService.get(authorId).subscribe(
      (response) => {
        this.author = response.body;
        this.author.name = `${this.author.firstName} ${this.author.lastName}`;        
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
}

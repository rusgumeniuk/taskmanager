import { Component, OnInit, OnDestroy } from '@angular/core';
import { Project } from '../../../../models/project/project';
import { switchMap, takeUntil } from 'rxjs/operators';
import { ProjectService } from 'src/app/services/project.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Subject } from 'rxjs';
import { NamedEntity } from 'src/app/models/named-entity';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
})
export class ProjectsComponent implements OnInit, OnDestroy {
  public projects: Project[] = [];
  cachedProjects: NamedEntity[] = [];

  public loading = false;
  public loadingProjects = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private snackBarService: SnackBarService,
    private projectService: ProjectService
  ) {}

  public ngOnInit(): void {
    this.getProjects();
  }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public getProjects() {
    this.loadingProjects = true;
    this.projectService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.loadingProjects = false;
          this.projects = this.cachedProjects = resp.body;
        },
        (error) => {
          this.loadingProjects = false;
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  deleteProject(id: number) {
    this.projectService
      .delete(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.snackBarService.showUsualMessage('Project was deleted!');
          this.cachedProjects = this.cachedProjects.filter(
            (item) => item.id !== id
          );
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }
}

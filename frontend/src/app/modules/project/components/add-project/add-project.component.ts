import { Component, OnInit, Input } from '@angular/core';
import { NewProject } from 'src/app/models/project/new-project';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/services/project.service';
import { takeUntil, distinct } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Router } from '@angular/router';
import { Team } from 'src/app/models/team/team';
import { TeamService } from 'src/app/services/team.service';
import { User } from 'src/app/models/user/user';
import { UserService } from 'src/app/services/user.service';
import { ComponentCanDeactivate } from 'src/app/guards/exit.about.guard';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css'],
})
export class AddProjectComponent implements OnInit, ComponentCanDeactivate {
  @Input()
  project: NewProject = {} as NewProject;

  teams: Team[];
  users: User[];

  isCreated: boolean = false;
  projectForm: FormGroup;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private projectService: ProjectService,
    private teamService: TeamService,
    private userService: UserService,
    private snackBarService: SnackBarService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.projectForm = new FormGroup({
      name: new FormControl(this.project.name, [
        Validators.required,
        Validators.minLength(3),
      ]),
      deadline: new FormControl(this.project.deadline, [Validators.required]),
      description: new FormControl(this.project.description, [
        Validators.required,
      ]),
      teamId: new FormControl(this.project.teamId, [Validators.required]),
      authorId: new FormControl(this.project.authorId, [Validators.required]),
    });

    const teamFormControl = this.projectForm.get('teamId');

    teamFormControl.valueChanges
      .pipe(distinct())
      .subscribe((value) => teamFormControl.setValue(+value));

    const authorFormControl = this.projectForm.get('authorId');

    authorFormControl.valueChanges
      .pipe(distinct())
      .subscribe((value) => authorFormControl.setValue(+value));

    this.updateInfo();
  }

  updateInfo(): void {
    this.getTeams();
    this.getUsers();
  }

  public getTeams() {
    this.teamService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = resp.body;
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }

  public getUsers() {
    this.userService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          const usersWithoutName = resp.body;
          usersWithoutName.forEach(
            (user) => (user.name = `${user.firstName} ${user.lastName}`)
          );
          this.users = usersWithoutName;
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }

  isInvalidControl(controlName: string): boolean {
    const control = this.projectForm.controls[controlName];
    return control.invalid && control.touched;
  }

  createProject(): void {
    const controls = this.projectForm.controls;

    if (this.projectForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const newProject = this.projectForm.value as NewProject;
    this.projectService
      .create(newProject)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.isCreated = true;
          this.snackBarService.showUsualMessage('Project created!');
          this.router.navigate([`project-details/${resp.body.id}`]);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  backToList(): void {
    this.router.navigate([`projects`]);
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.projectForm.untouched || this.isCreated
      ? true
      : confirm('Do you wanna leave this page? All unsaved data will be lost.');
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { NewTeam } from 'src/app/models/team/new-team';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TeamService } from 'src/app/services/team.service';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Team } from 'src/app/models/team/team';
import { ComponentCanDeactivate } from 'src/app/guards/exit.about.guard';

@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.css'],
})
export class EditTeamComponent implements OnInit, ComponentCanDeactivate {
  @Input()
  team: Team = {} as Team;
  isUpdated: boolean = false;

  teamForm: FormGroup;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private teamService: TeamService,
    private snackBarService: SnackBarService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let teamId: number = Number(this.route.snapshot.paramMap.get('id'));
    this.updateInfo(teamId);

    this.teamForm = new FormGroup({
      name: new FormControl(this.team.name, [
        Validators.required,
        Validators.minLength(3),
      ]),
    });
  }

  updateInfo(teamId: number) {
    this.getTeam(teamId);
  }

  getTeam(teamId: number) {
    this.teamService.get(teamId).subscribe(
      (response) => {
        this.team = response.body;
        this.teamForm.get('name').setValue(this.team.name);
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  isInvalidControl(controlName: string): boolean {
    const control = this.teamForm.controls[controlName];
    return control.invalid && control.touched;
  }

  updateTeam(): void {
    const controls = this.teamForm.controls;

    if (this.teamForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const updatedTeam = this.teamForm.value as Team;
    updatedTeam.id = this.team.id;
    updatedTeam.createdAt = this.team.createdAt;

    this.teamService
      .update(updatedTeam)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.isUpdated = true;
          this.snackBarService.showUsualMessage('Team updated!');
          this.router.navigate([`team-details/${this.team.id}`]);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  backToList(): void {
    this.router.navigate([`teams`]);
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.teamForm.untouched ||  this.isUpdated
      ? true
      : confirm('Do you wanna leave this page? All unsaved data will be lost.');
  }
}

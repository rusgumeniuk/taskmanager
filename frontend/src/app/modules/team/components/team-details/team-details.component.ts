import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/models/team/team';
import { Project } from 'src/app/models/project/project';
import { User } from 'src/app/models/user/user';
import { TeamService } from 'src/app/services/team.service';
import { ActivatedRoute } from '@angular/router';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css'],
})
export class TeamDetailsComponent implements OnInit {
  team: Team;
  snackBarService: SnackBarService;

  constructor(
    private teamService: TeamService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let teamId: number = Number(this.route.snapshot.paramMap.get('id'));
    this.updateInfo(teamId);
  }

  updateInfo(teamId: number) {
    this.getTeam(teamId);
  }

  getTeam(teamId: number) {
    this.teamService.get(teamId).subscribe(
      (response) => {
        this.team = response.body;
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { NewTeam } from 'src/app/models/team/new-team';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TeamService } from 'src/app/services/team.service';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Router } from '@angular/router';
import { ComponentCanDeactivate } from '../../../../guards/exit.about.guard';

@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.css'],
})
export class AddTeamComponent implements OnInit, ComponentCanDeactivate {
  @Input()
  team: NewTeam = {} as NewTeam;

  isCreated: boolean = false;
  teamForm: FormGroup;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private teamService: TeamService,
    private snackBarService: SnackBarService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.teamForm = new FormGroup({
      name: new FormControl(this.team.name, [
        Validators.required,
        Validators.minLength(3),
      ]),
    });
  }

  isInvalidControl(controlName: string): boolean {
    const control = this.teamForm.controls[controlName];
    return control.invalid && control.touched;
  }

  createTeam(): void {
    const controls = this.teamForm.controls;

    if (this.teamForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const newTeam = this.teamForm.value as NewTeam;

    this.teamService
      .create(newTeam)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.isCreated = true;
          this.snackBarService.showUsualMessage('Team created!');
          this.router.navigate([`team-details/${resp.body.id}`]);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  backToList(): void {    
    this.router.navigate([`teams`]);
  }

  canDeactivate(): boolean | Observable<boolean> {
    return this.teamForm.untouched ||  this.isCreated
      ? true
      : confirm('Do you wanna leave this page? All unsaved data will be lost.');
  }
}

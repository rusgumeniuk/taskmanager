import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, from } from 'rxjs';
import { NamedEntity } from 'src/app/models/named-entity';
import { takeUntil } from 'rxjs/operators';
import { Team } from '../../../../models/team/team';
import { TeamService } from '../../../../services/team.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css'],
})
export class TeamsComponent implements OnInit, OnDestroy {
  public teams: Team[] = [];
  public cachedTeams: NamedEntity[] = [];

  public loading = false;
  public loadingTeams = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private teamService: TeamService,
    private snackBarService: SnackBarService
  ) {}

  public ngOnInit(): void {
    this.getTeams();
  }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public getTeams() {
    this.loadingTeams = true;
    this.teamService
      .getAll()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.loadingTeams = false;
          this.teams = resp.body;
          this.cachedTeams = this.teams.map((team) => team as NamedEntity);
        },
        (error) => {
          this.loadingTeams = false;
          this.snackBarService.showErrorMessage(error);
        }
      );
  }

  deleteTeam(id: number) {
    this.teamService
      .delete(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.snackBarService.showUsualMessage('Team was deleted!');
          this.cachedTeams = this.cachedTeams.filter((item) => item.id !== id);
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }
}

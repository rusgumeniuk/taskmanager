import { Directive, Input, ElementRef, OnInit } from '@angular/core';
import { TaskState } from '../models/task/task-state';
import { TaskStateColor } from '../models/task/task-state-colors';

@Directive({
  selector: '[taskState]',
})
export class TaskStateDirective implements OnInit {
  @Input()
  taskState: TaskState;

  constructor(private el: ElementRef) {}

  ngOnInit() {
    this.el.nativeElement.style.backgroundColor =
      TaskStateColor[this.taskState];
  }
}

import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError, empty } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor() {}

  public intercept(
    req: HttpRequest<object>,
    next: HttpHandler
  ): Observable<HttpEvent<object>> {
    return next.handle(req).pipe(
      catchError((response: HttpErrorResponse) => {
        console.log(response);
        const error: string = response.error
          ? response.message
          : `${response.status} ${response.status}`;

        return throwError(new Error(error));
      })
    );
  }
}

import { TaskState } from './task-state';
import { NamedEntity } from '../named-entity';

export interface Task extends NamedEntity {  
  description: string;
  state: TaskState;
  createdAt: Date;
  finishedAt: Date;
  projectId: number;
  performerId: number;
}

export interface NewTask {
  name: string;
  description: string;
  projectId: number;
  performerId: number;
}

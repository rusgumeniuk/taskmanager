import { NamedEntity } from '../named-entity';
export interface Project extends NamedEntity {  
  description: string;
  deadline: Date;
  teamId: number;
  authorId: number;
}

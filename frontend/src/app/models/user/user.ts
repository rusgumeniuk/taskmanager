import { NamedEntity } from '../named-entity';

export interface User extends NamedEntity {
  firstName: string;
  lastName: string;
  email: string;
  registeredAt: Date;
  birthDay: Date;
  teamId?: number;
}

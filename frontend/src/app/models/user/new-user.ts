export interface NewUser {
  firstName: string;
  lastName: string;
  birthDay: Date;
  teamId?: number;
  email: string;
}

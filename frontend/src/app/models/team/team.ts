import { NamedEntity } from '../named-entity';

export interface Team extends NamedEntity {
  createdAt: Date;
}

﻿using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.Console.IO;
using TaskManager.Console.IO.Abstract;
using TaskManager.Console.Services;

namespace TaskManager.Console
{
    internal static class Program
    {
        private static IConsoleMenu _console;

        private static async Task Main()
        {
            var httpClient = new HttpClient();
            var taskManagerHttpClient = new TaskManagerHttpClient(httpClient);
            _console = TaskManagerConsole
                .GetInstance(
                    new ConsoleReader(),
                    new TeamHttpService(taskManagerHttpClient),
                    new UserHttpService(taskManagerHttpClient),
                    new ProjectHttpService(taskManagerHttpClient),
                    new ProjectTaskHttpService(taskManagerHttpClient)
                    );

            System.Console.SetWindowSize((int)(0.75 * System.Console.LargestWindowWidth), System.Console.WindowHeight);
            while (true)
            {
                await _console.DisplayMainMenuAsync();
            }
        }
    }
}

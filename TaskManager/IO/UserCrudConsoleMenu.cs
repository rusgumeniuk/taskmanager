﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;
using TaskManager.Console.IO.Abstract;

namespace TaskManager.Console.IO
{
    public sealed class UserCrudConsoleMenu : BaseCrudConsoleMenu, ICrudConsoleMenu
    {
        private readonly IUserService _userService;
        private readonly ITeamService _teamService;

        public UserCrudConsoleMenu(IUserService userService, ITeamService teamService, IReader reader) : base("User", reader)
        {
            _userService = userService;
            _teamService = teamService;
        }

        public override async Task AddAsync()
        {
            var teams = await _teamService.GetAllAsync();
            if (!teams.Any())
            {
                await PrintDangerAsync("There is no teams, so we can't create a user");
                return;
            }

            var firstName = await InputStringAsync("user First name");
            if (firstName == null)
            {
                return;
            }
            var lastName = await InputStringAsync("user Last name");
            if (lastName == null)
            {
                return;
            }

            var email = await InputEmailAsync();
            if (email == null)
            {
                return;
            }

            var bday = await InputDateAsync("user birth day", DateTime.MinValue, DateTime.Now);
            if (bday == default)
            {
                return;
            }

            await PrintAlertMessageAsync("Select a team (or 0 if user without team):");
            var sb = new StringBuilder();

            foreach (var team in teams)
            {
                sb.Append("Team ID [").Append(team.Id).Append("], name: ").AppendLine(team.Name);
            }

            await PrintMessageAsync(sb.ToString());

            int? teamId = -1;
            while (teamId != 0 && !teams.Any(team => team.Id == teamId))
            {
                teamId = await GetIdAsync();
            }

            if (teamId == 0)
            {
                teamId = null;
            }

            var newUser = new UserCreateDTO()
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                BirthDay = bday,
                TeamId = teamId
            };

            var addedUser = await _userService.AddAsync(newUser);
            await PrintSuccessfulMessageAsync($"Added User with ID [{addedUser.Id}] and name {addedUser.FirstName} {addedUser.LastName}");
        }

        public override async Task ShowAsync()
        {
            var users = await _userService.GetAllAsync();

            if (!users.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var userId = -1;
            while (userId != 0 && !users.Any(user => user.Id == userId))
            {
                userId = await GetIdAsync();
            }

            if (userId == 0)
            {
                return;
            }

            var user = await _userService.GetAsync(userId);
            await PrintSuccessfulMessageAsync($"User with ID [{user.Id}] and name '{user.FirstName} {user.LastName}'," +
                                         $" Team ID [{user.TeamId}], b-day: {user.BirthDay.ToShortDateString()}, register at {user.RegisteredAt}");
        }

        public override async Task ShowAllAsync()
        {
            var users = await _userService.GetAllAsync();
            var stringBuilder = new StringBuilder();
            foreach (var user in users)
            {
                stringBuilder
                    .Append("User with ID [").Append(user.Id)
                    .Append("] and name ").Append(user.FirstName).Append(" ").AppendLine(user.LastName);
            }
            await PrintSuccessfulMessageAsync("Users:");
            await PrintAlertMessageAsync(stringBuilder.ToString());
        }

        public override async Task UpdateAsync()
        {
            var users = await _userService.GetAllAsync();

            if (!users.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var userId = -1;
            while (userId != 0 && !users.Any(user => user.Id == userId))
            {
                userId = await GetIdAsync();
            }

            if (userId == 0)
            {
                return;
            }

            var userToUpdate = users.First(team => team.Id == userId);

            const string title = "What do you wanna change:";
            const string content = "1 : User First name"
                                   + "\n" + "2 : User Second Name"
                                   + "\n" + "3 : User Birth day"
                                   + "\n" + "4 : User team"
                                   + "\n" + "5 : User email"
                                   + "\n" + "6 : Save"
                ;
            var operationNumber = 0;
            do
            {
                try
                {
                    await DisplayMenuAsync(title, content);
                    operationNumber = await Reader.ParseIntNumberAsync(0, 6);
                    switch (operationNumber)
                    {
                        case 0:
                            {
                                return;
                            }
                        case 1:
                            {
                                var firstName = await InputStringAsync("First name");
                                if (firstName == null)
                                {
                                    return;
                                }

                                userToUpdate.FirstName = firstName;
                                break;
                            }
                        case 2:
                            {
                                var secondName = await InputStringAsync("Last name");
                                if (secondName == null)
                                {
                                    return;
                                }

                                userToUpdate.LastName = secondName;
                                break;
                            }
                        case 3:
                            {
                                var bday = await InputDateAsync("birth day", DateTime.MinValue, DateTime.Now);
                                if (bday == default)
                                {
                                    return;
                                }

                                userToUpdate.BirthDay = bday;
                                break;
                            }
                        case 4:
                            {
                                await PrintMessageAsync($"Current team ID [{userToUpdate.TeamId ?? -1}]");
                                var teams = await _teamService.GetAllAsync();
                                await PrintAlertMessageAsync("Select a team:");
                                var sb = new StringBuilder();

                                foreach (var team in teams)
                                {
                                    sb.Append("Team ID [").Append(team.Id).Append("], name: ").AppendLine(team.Name);
                                }

                                await PrintMessageAsync(sb.ToString());

                                var teamId = -1;
                                while (teamId != 0 && !teams.Any(team => team.Id == teamId))
                                {
                                    teamId = await GetIdAsync();
                                }

                                if (teamId == 0)
                                {
                                    return;
                                }

                                userToUpdate.TeamId = teamId;
                                break;
                            }
                        case 5:
                            {
                                var email = await InputEmailAsync();
                                if (email == null)
                                {
                                    return;
                                }

                                userToUpdate.Email = email;
                                break;
                            }
                        case 6:
                            {
                                continue;
                            }
                    }
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            } while (operationNumber > 0 && operationNumber < 6);

            await _userService.UpdateAsync(userToUpdate);

            await PrintSuccessfulMessageAsync($"User with ID [{userToUpdate.Id}] has updated!");
            await PrintAlertMessageAsync($"Updated user name: {userToUpdate.FirstName} {userToUpdate.LastName}," +
                                    $" email: {userToUpdate.Email}, birth day: {userToUpdate.BirthDay.ToShortDateString()}," +
                                    $" Team ID [{userToUpdate.TeamId}]");
        }

        public override async Task RemoveAsync()
        {
            var users = await _userService.GetAllAsync();

            if (!users.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var userId = -1;
            while (userId != 0 && !users.Any(dto => dto.Id == userId))
            {
                userId = await GetIdAsync();
            }

            if (userId == 0)
            {
                return;
            }

            var selectedEntity = users.First(dto => dto.Id == userId);
            await _userService.RemoveAsync(selectedEntity.Id);
            await PrintDangerAsync($"User with ID [{selectedEntity.Id}] and name {selectedEntity.FirstName} {selectedEntity.LastName}  was deleted!");
        }
    }
}

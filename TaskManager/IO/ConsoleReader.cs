﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;

using TaskManager.Console.IO.Abstract;

namespace TaskManager.Console.IO
{
    public class ConsoleReader : IReader
    {
        public async Task<decimal> ParseDecimalNumberAsync(string @string = null)
        {
            var input = @string ?? await GetNotEmptyStringAsync();
            return decimal.TryParse(input, out var number) ? number : throw new FormatException($"We can't parse '{input}' to a decimal.");
        }
        public async Task<decimal> ParseDecimalNumberAsync(decimal min, string @string = null)
        {
            var number = await ParseDecimalNumberAsync(@string);
            return number >= min ? number : throw new ArgumentOutOfRangeException(null, $"Please, input the number more or equal {min}!");
        }
        public async Task<decimal> ParseDecimalNumberAsync(decimal min, decimal max, string @string = null)
        {
            var number = await ParseDecimalNumberAsync(min, @string);
            return number <= max ? number : throw new ArgumentOutOfRangeException(null, $"Please, input the number from the range [{min} - {max}].");
        }

        public async Task<int> ParseIntNumberAsync(string @string = null)
        {
            var input = @string ?? await GetNotEmptyStringAsync();
            return int.TryParse(input, out var number) ? number : throw new FormatException($"We can't parse '{input}' to an integer.");
        }
        public async Task<int> ParseIntNumberAsync(int min, string @string = null)
        {
            var number = await ParseIntNumberAsync(@string);
            return number >= min ? number : throw new ArgumentOutOfRangeException(null, $"Please, input the number more or equal {min}!");
        }
        public async Task<int> ParseIntNumberAsync(int min, int max, string @string = null)
        {
            var number = await ParseIntNumberAsync(min, @string);
            return number <= max ? number : throw new ArgumentOutOfRangeException(null, $"Please, input the number from the range [{min} - {max}].");
        }

        public async Task<DateTime> ParseDateTimeAsync(string format = "dd/MM/yyyy", string @string = null)
        {
            var date = (@string ?? await System.Console.In.ReadLineAsync())?.Trim();
            if (DateTime.TryParseExact(date, format, null, System.Globalization.DateTimeStyles.None, out var parsedDate))
            {
                return parsedDate;
            }

            throw new ArgumentException($"Date format should be {format}.");
        }

        public async Task<DateTime> ParseDateTimeAsync(DateTime min, string format = "dd/MM/yyyy", string @string = null)
        {
            var date = (@string ?? await System.Console.In.ReadLineAsync())?.Trim();
            if (DateTime.TryParseExact(date, format, null, System.Globalization.DateTimeStyles.None, out var parsedDate)
                && parsedDate >= min)
            {
                return parsedDate;
            }

            throw new ArgumentException($"Wrong input or date should be after: {min}.\n Date format should be {format}.");
        }

        public async Task<DateTime> ParseDateTimeAsync(DateTime min, DateTime max, string format = "dd/MM/yyyy", string @string = null)
        {
            var date = (@string ?? await System.Console.In.ReadLineAsync())?.Trim();
            if (DateTime.TryParseExact(date, format, null, System.Globalization.DateTimeStyles.None, out var parsedDate)
                && parsedDate >= min && parsedDate <= max)
            {
                return parsedDate;
            }

            throw new ArgumentException($"Wrong input or date should be after: {min} and before: {max}.\n Date format should be {format}.");
        }

        public async Task<string> GetNotEmptyStringAsync(string @string = null)
        {
            var input = (@string ?? await System.Console.In.ReadLineAsync())?.Trim();
            if (!string.IsNullOrEmpty(input)) return input;
            throw new ArgumentNullException(null, "Please, input something.");
        }

        public async Task<string> GetEmailAsync(string @string = null)
        {
            var value = (@string ?? await System.Console.In.ReadLineAsync())?.Trim();
            new MailAddress(value);
            return value;
        }
    }
}

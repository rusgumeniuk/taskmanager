﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Console.IO.Abstract;

namespace TaskManager.Console.IO
{
    public sealed class LinqQueriesConsoleMenu : BaseConsoleMenu, IConsoleMenu
    {
        private readonly IReader _reader;
        private readonly ITeamService _teamService;
        private readonly IUserService _userService;
        private readonly IProjectService _projectService;
        private readonly IProjectTaskService _taskService;

        public LinqQueriesConsoleMenu(
            IReader reader,
            ITeamService teamService,
            IUserService userService,
            IProjectService projectService,
            IProjectTaskService taskService
            )
        {
            _reader = reader;
            _teamService = teamService;
            _userService = userService;
            _projectService = projectService;
            _taskService = taskService;
        }

        public override async Task DisplayMainMenuAsync()
        {
            const string title = "Please input number of appropriate operation:";
            const string content = "1 : Show count of Tasks in each project for selected User"
                             + "\n" + "2 : Show Tasks for current User where length of task's name less than 45 chars"
                             + "\n" + "3 : Show Tasks that selected User has finished in this year"
                             + "\n" + "4 : Show Teams where Members older than 10 years and ordered by registration time"
                             + "\n" + "5 : Show User list (sorted by First Name) with tasks sorted by descending by task name length"
                             + "\n" + "6 : Show User info (user, last own project, the total count of tasks in the last project, the total count of user unfinished tasks and longest task)"
                             + "\n" + "7 : Show Project info (project, task with the longest description, a task with the shortest name and total count of team members)"
                ;
            await DisplayMenuAsync(title, content);

            switch (await _reader.ParseIntNumberAsync(0, 7))
            {
                case 0:
                    {
                        return;
                    }
                case 1:
                    {
                        await ShowProjectTasksCountDictionaryByUserAsync();
                        break;
                    }
                case 2:
                    {
                        await ShowTasksWithNameLessThan45ForUserAsync();
                        break;
                    }
                case 3:
                    {
                        await ShowFinishedInCurrentYearTaskIdNameListForUserAsync();
                        break;
                    }
                case 4:
                    {
                        await ShowTeamIdNameMembersListWhereMembersOlderThan10OrderedByRegistrationTimeAsync();
                        break;
                    }
                case 5:
                    {
                        await ShowUserListSortedByFirstNameAndTaskListSortedByDescendingByNameAsync();
                        break;
                    }
                case 6:
                    {
                        await ShowUserWithLastProjectAndCountOfTasksInLastProjectAndCountUnfinishedTasksAndLongestTaskAsync();
                        break;
                    }
                case 7:
                    {
                        await ShowProjectWithLongestByDescriptionTaskAndShortestByNameTaskAndCountOfMembersAsync();
                        break;
                    }
            }
        }

        private async Task ShowProjectTasksCountDictionaryByUserAsync()
        {
            var users = await _userService.GetAllAsync();
            if (!users.Any())
            {
                await PrintDangerAsync("There is no user :c");
                return;
            }

            var stringBuilder = new StringBuilder();
            foreach (var user in users)
            {
                stringBuilder
                    .Append("User with ID: [").Append(user.Id)
                    .Append("] and name ").Append(user.FirstName).Append(" ").AppendLine(user.LastName);
            }
            await PrintSuccessfulMessageAsync("All Users:");
            await PrintAlertMessageAsync(stringBuilder.ToString());

            var userId = await GetUserIdAsync();
            if (userId == 0)
            {
                return;
            }
            var projectTaskCountDictionary = await _projectService.GetNumberOfTasksOfUserAsync(userId);
            if (projectTaskCountDictionary == null || projectTaskCountDictionary.Count == 0)
            {
                await PrintAlertMessageAsync($"In projects that authored by User with ID [{userId}] no one task");
                return;
            }

            stringBuilder.Clear();

            foreach (var (project, countOfTasks) in projectTaskCountDictionary)
            {
                stringBuilder
                    .Append("Project ID [").Append(project.Id).Append("], project name '").Append(project.Name)
                    .Append("'. Count of tasks: ").AppendLine(countOfTasks.ToString());
            }

            await PrintSuccessfulMessageAsync(stringBuilder.ToString());
        }

        private async Task ShowTasksWithNameLessThan45ForUserAsync()
        {
            var userId = await GetUserIdAsync();
            if (userId == 0)
            {
                return;
            }
            var tasks = await _taskService.GetTasksOfUserWithTaskNameLessThan45Async(userId);

            if (tasks?.Any() != true)
            {
                await PrintAlertMessageAsync($"User with ID [{userId}] has not any tasks with name length less than 45.");
                return;
            }

            var stringBuilder = new StringBuilder();
            foreach (var task in tasks)
            {
                stringBuilder
                    .Append("Task ID [").Append(task.Id)
                    .Append("], name: '").Append(task.Name)
                    .Append("' (name length = ").Append(task.Name.Length).AppendLine(")");
            }

            await PrintSuccessfulMessageAsync(stringBuilder.ToString());
        }

        private async Task ShowFinishedInCurrentYearTaskIdNameListForUserAsync()
        {
            var userId = await GetUserIdAsync();
            if (userId == 0)
            {
                return;
            }

            var tasks = await _taskService.GetFinishedTasksInTheCurrentYearByUserAsync(userId);

            if (tasks?.Any() != true)
            {
                await PrintAlertMessageAsync($"User with ID [{userId}] has not finished any task in the {DateTime.Now.Year}");
                return;
            }

            var stringBuilder = new StringBuilder();
            foreach (var task in tasks)
            {
                stringBuilder
                    .Append("Task ID [").Append(task.TaskId)
                    .Append("], task name: ").AppendLine(task.TaskName);
            }

            await PrintSuccessfulMessageAsync(stringBuilder.ToString());
        }

        private async Task ShowTeamIdNameMembersListWhereMembersOlderThan10OrderedByRegistrationTimeAsync()
        {
            var teamList = await _teamService.GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTimeAsync();

            if (teamList?.Any() != true)
            {
                await PrintAlertMessageAsync("There is no one team with members older than 10 years");
                return;
            }

            var stringBuilder = new StringBuilder();
            foreach (var team in teamList)
            {
                stringBuilder
                    .Append("\nTeam ID [").Append(team.TeamId)
                    .Append("], Team name: ").AppendLine(team.TeamName);
                stringBuilder.AppendLine("Members:");
                foreach (var member in team.Members)
                {
                    stringBuilder
                        .Append("Member ID [").Append(member.Id)
                        .Append("], BirthDay: ").Append(member.BirthDay.ToShortDateString())
                        .Append(", RegistrationTime: ").Append(member.RegisteredAt)
                        .AppendLine();
                }
            }
            await PrintSuccessfulMessageAsync(stringBuilder.ToString());
        }

        private async Task ShowUserListSortedByFirstNameAndTaskListSortedByDescendingByNameAsync()
        {
            var usersAndTasksList = await _userService.GetUsersOrderedByFirstNameSortedByTaskNameAsync();
            if (usersAndTasksList?.Any() != true)
            {
                await PrintAlertMessageAsync("There is no one user");
                return;
            }

            var stringBuilder = new StringBuilder();
            foreach (var profile in usersAndTasksList)
            {
                stringBuilder
                    .Append("\nUser ID [").Append(profile.User.Id)
                    .Append("], user first name: ").AppendLine(profile.User.FirstName)
                    .AppendLine("Tasks:");
                if (!profile.Tasks.Any())
                {
                    stringBuilder.AppendLine("No one task");
                }
                else
                {
                    profile.Tasks
                        .Aggregate(stringBuilder, (x, y) => x
                            .Append("Task ID [").Append(y.Id)
                            .Append("], task name '").Append(y.Name)
                            .Append("' (name length = ").Append(y.Name.Length).AppendLine(")."));
                }
            }
            await PrintSuccessfulMessageAsync(stringBuilder.ToString());
        }

        private async Task ShowUserWithLastProjectAndCountOfTasksInLastProjectAndCountUnfinishedTasksAndLongestTaskAsync()
        {
            //TODO show user list
            var userId = await GetUserIdAsync();
            if (userId == 0)
            {
                return;
            }
            var userInfo = await _taskService.GetUserInfoAsync(userId);

            var stringBuilder = new StringBuilder();
            stringBuilder
                .Append("User ID [").Append(userInfo.User.Id).Append("], Full Name: ").Append(userInfo.User.FirstName).Append(" ").AppendLine(userInfo.User.LastName)
                .Append("Last project ID [").Append(userInfo.LastProject.Id).Append("], name: '")
                .Append(userInfo.LastProject.Name).AppendLine("'")
                .Append("Count of tasks in the last project: ").AppendLine(userInfo.CountOfTasksInLastProject.ToString())
                .Append("Count of unfinished tasks: ").AppendLine(userInfo.CountOfUnfinishedTasks.ToString())
                .Append("Longest task ID [").Append(userInfo.LongestTask.Id).Append("], name: '").Append(userInfo.LongestTask.Name)
                .Append("', duration: ").Append((userInfo.LongestTask.FinishedAt - userInfo.LongestTask.CreatedAt).Days).Append(" days");

            await PrintSuccessfulMessageAsync(stringBuilder.ToString());
        }

        private async Task ShowProjectWithLongestByDescriptionTaskAndShortestByNameTaskAndCountOfMembersAsync()
        {
            var res = await _projectService.GetProjectInfoAsync();

            var stringBuilder = new StringBuilder();
            foreach (var projectInfo in res)
            {
                stringBuilder
                    .Append("\nProject ID [").Append(projectInfo.Project.Id)
                    .Append("], project name: ").AppendLine(projectInfo.Project.Name);
                if (projectInfo.TaskWithLongestDescription != null)
                {
                    stringBuilder
                        .Append("Task with longest description has ID [").Append(projectInfo.TaskWithLongestDescription.Id)
                        .Append("]. Description length = ")
                        .AppendLine(projectInfo.TaskWithLongestDescription.Description.Length.ToString())
                        .Append("Task with shortest name has ID [").Append(projectInfo.TaskWithShortestName.Id)
                        .Append("]. Name length = ").AppendLine(projectInfo.TaskWithShortestName.Name.Length.ToString());
                }
                else
                {
                    stringBuilder.AppendLine("No task is related to this project.");
                }

                stringBuilder.Append("Count of personal: ").AppendLine(projectInfo.CountOfStaff.ToString());
            }
            await PrintSuccessfulMessageAsync(stringBuilder.ToString());
        }

        private async Task<int> GetUserIdAsync(int min = 0, int max = int.MaxValue)
        {
            while (true)
            {
                await DisplayMenuAsync("Input the User ID (number) or 0 to go to Main menu", null);
                try
                {
                    var parsedId = await _reader.ParseIntNumberAsync(min, max);
                    return parsedId == 0 || await _userService.ExistUserAsync(parsedId)
                        ? parsedId
                        : throw new ArgumentException($"There is no User with ID [{parsedId}]");
                }
                catch (HttpRequestException)
                {
                    await PrintExceptionAsync(new HttpRequestException("Please ensure that you are connected to the Internet"));
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;
using TaskManager.Console.IO.Abstract;
using TaskManager.DAL.Models;

namespace TaskManager.Console.IO
{
    public sealed class ProjectTaskCrudConsoleMenu : BaseCrudConsoleMenu
    {
        private readonly IProjectTaskService _taskService;
        private readonly IProjectService _projectService;
        private readonly IUserService _userService;

        public ProjectTaskCrudConsoleMenu(
            IProjectTaskService taskService,
            IProjectService projectService,
            IUserService userService,
            IReader reader) : base("Task", reader)
        {
            _taskService = taskService;
            _projectService = projectService;
            _userService = userService;
        }

        public override async Task AddAsync()
        {
            var projects = await _projectService.GetAllAsync();
            var users = await _userService.GetAllAsync();
            if (!projects.Any())
            {
                await PrintDangerAsync("There is no projects, so we can't create a task");
                return;
            }
            else if (!users.Any())
            {
                await PrintDangerAsync("There is no users (performers), so we can't create a task");
                return;
            }

            var newName = await InputStringAsync("task name");
            if (newName == null)
            {
                return;
            }
            var description = await InputStringAsync("task description");
            if (description == null)
            {
                return;
            }

            await PrintAlertMessageAsync("Select project:");
            var sb = new StringBuilder();

            foreach (var project in projects)
            {
                sb.Append("Project ID [").Append(project.Id).Append("], name: ").AppendLine(project.Name);
            }

            await PrintMessageAsync(sb.ToString());

            var projectId = -1;
            while (projectId != 0 && !projects.Any(project => project.Id == projectId))
            {
                projectId = await GetIdAsync();
            }
            if (projectId == 0)
            {
                return;
            }

            var selectedProject = projects.First(project => project.Id == projectId);

            var teamMember = users.Where(user => user.TeamId == selectedProject.TeamId);
            if (!teamMember.Any())
            {
                await PrintDangerAsync("Project team is empty so we can't create a task");
                return;
            }

            await PrintAlertMessageAsync("Select a performer:");
            sb.Clear();

            foreach (var user in teamMember)
            {
                sb.Append("User ID [").Append(user.Id).Append("], name: ")
                    .Append(user.FirstName).Append(" ").AppendLine(user.LastName);
            }

            await PrintMessageAsync(sb.ToString());

            var performerId = -1;
            while (performerId != 0 && !teamMember.Any(user => user.Id == performerId))
            {
                performerId = await GetIdAsync();
            }
            if (performerId == 0)
            {
                return;
            }

            var newTask = new ProjectTaskCreateDTO
            {
                Name = newName,
                Description = description,
                ProjectId = projectId,
                PerformerId = performerId
            };

            var addedTask = await _taskService.AddAsync(newTask);
            await PrintSuccessfulMessageAsync($"Added Task with ID [{addedTask.Id}] and name {addedTask.Name}");
        }

        public override async Task ShowAsync()
        {
            var tasks = await _taskService.GetAllAsync();

            if (!tasks.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var taskId = -1;
            while (taskId != 0 && !tasks.Any(task => task.Id == taskId))
            {
                taskId = await GetIdAsync();
            }

            if (taskId == 0)
            {
                return;
            }

            var task = await _taskService.GetAsync(taskId);
            await PrintSuccessfulMessageAsync($"Task with ID [{task.Id}], name '{task.Name}', state {(TaskState)task.State}," +
                                          $" created at {task.CreatedAt}, finished at {task.FinishedAt}." +
                                          $"\nPerformer ID [{task.PerformerId}], Project ID [{task.ProjectId}]" +
                                          $"\nDescription:\n{task.Description}");
        }

        public override async Task ShowAllAsync()
        {
            var tasks = await _taskService.GetAllAsync();
            var stringBuilder = new StringBuilder();
            foreach (var task in tasks)
            {
                stringBuilder
                    .Append("Task with ID [").Append(task.Id)
                    .Append("] and name '").Append(task.Name).AppendLine("'");
            }
            await PrintSuccessfulMessageAsync("Tasks:");
            await PrintAlertMessageAsync(stringBuilder.ToString());
        }

        public override async Task UpdateAsync()
        {
            var tasks = await _taskService.GetAllAsync();

            if (!tasks.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var taskId = -1;
            while (taskId != 0 && !tasks.Any(task => task.Id == taskId))
            {
                taskId = await GetIdAsync();
            }

            if (taskId == 0)
            {
                return;
            }

            var taskToUpdate = tasks.First(team => team.Id == taskId);

            const string title = "What do you wanna change:";
            const string content = "1 : Task Name"
                                   + "\n" + "2 : Task description"
                                   + "\n" + "3 : Task Status"
                                   + "\n" + "4 : Task Project"
                                   + "\n" + "5 : Task Performer"
                                   + "\n" + "6 : Save";
            var operationNumber = 0;
            do
            {
                try
                {
                    await DisplayMenuAsync(title, content);
                    operationNumber = await Reader.ParseIntNumberAsync(0, 6);
                    switch (operationNumber)
                    {
                        case 0:
                            {
                                return;
                            }
                        case 1:
                            {
                                var newName = await InputStringAsync("task name");
                                if (newName == null)
                                {
                                    return;
                                }

                                taskToUpdate.Name = newName;
                                break;
                            }
                        case 2:
                            {
                                var description = await InputStringAsync("task description");
                                if (description == null)
                                {
                                    return;
                                }

                                taskToUpdate.Description = description;
                                break;
                            }
                        case 3:
                            {
                                if (taskToUpdate.State >= (int)TaskState.Finished)
                                {
                                    await PrintDangerAsync("You can't modify state of this task");
                                    break;
                                }

                                var statusList = Enum.GetValues(typeof(TaskState)).Cast<TaskState>();
                                await PrintAlertMessageAsync("Task states:");
                                var sb = new StringBuilder();
                                for (var i = taskToUpdate.State + 1; i < statusList.Count(); ++i)
                                {
                                    sb
                                        .Append("Number: ").Append((int)statusList.ElementAt(i))
                                        .Append(" :").AppendLine(statusList.ElementAt(i).ToString());
                                }

                                await PrintMessageAsync(sb.ToString());

                                var exitNumber = (int)taskToUpdate.State - 1;
                                var newStatus = await InputIntAsync("number of status", (int)taskToUpdate.State, statusList.Count());
                                if (newStatus == exitNumber)
                                {
                                    continue;
                                }
                                taskToUpdate.State = newStatus;
                                if ((int)TaskState.Finished == taskToUpdate.State)
                                {
                                    taskToUpdate.FinishedAt = DateTime.Now;
                                }
                                break;
                            }
                        case 4:
                            {
                                await PrintMessageAsync($"Current project ID [{taskToUpdate.ProjectId }]");
                                var projects = await _projectService.GetAllAsync();
                                await PrintAlertMessageAsync("Select a project:");
                                var sb = new StringBuilder();

                                foreach (var project in projects)
                                {
                                    sb.Append("Project ID [").Append(project.Id).Append("], name: ").AppendLine(project.Name);
                                }

                                await PrintMessageAsync(sb.ToString());

                                var projectId = -1;
                                while (projectId != 0 && !projects.Any(project => project.Id == projectId))
                                {
                                    projectId = await GetIdAsync();
                                }

                                if (projectId == 0)
                                {
                                    return;
                                }

                                taskToUpdate.ProjectId = projectId;
                                break;
                            }

                        case 5:
                            {
                                await PrintMessageAsync($"Current performer ID [{taskToUpdate.PerformerId }]");
                                var currentProject = await _projectService.GetAsync(taskToUpdate.ProjectId);
                                var users = await _userService.GetAllAsync();
                                users = users.Where(user => user.TeamId == currentProject.TeamId);

                                if (!users.Any())
                                {
                                    await PrintDangerAsync("In current project no one user so you can't change a performer " +
                                                       $"until you add some user to Project with ID [{currentProject.AuthorId}] or chose other project");
                                }
                                else
                                {
                                    await PrintAlertMessageAsync("Select a performer:");
                                    var sb = new StringBuilder();

                                    foreach (var user in users)
                                    {
                                        sb
                                            .Append("User ID [").Append(user.Id)
                                            .Append("], name: ").Append(user.FirstName).Append(" ").AppendLine(user.LastName);
                                    }

                                    await PrintMessageAsync(sb.ToString());

                                    var performerId = -1;
                                    while (performerId != 0 && !users.Any(project => project.Id == performerId))
                                    {
                                        performerId = await GetIdAsync();
                                    }

                                    if (performerId == 0)
                                    {
                                        return;
                                    }

                                    taskToUpdate.PerformerId = performerId;
                                }
                                break;
                            }
                        case 6:
                            {
                                continue;
                            }
                    }
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            } while (operationNumber > 0 && operationNumber < 6);

            await _taskService.UpdateAsync(taskToUpdate);

            await PrintSuccessfulMessageAsync($"Task with ID [{taskToUpdate.Id}] has updated!");
            await PrintAlertMessageAsync($"Updated task name:'{taskToUpdate.Name}'\nProject ID [{taskToUpdate.ProjectId}], Performer ID [{taskToUpdate.PerformerId}]" +
                                    $"\nDescription:\n {taskToUpdate.Description}");
        }

        public override async Task RemoveAsync()
        {
            var tasks = await _taskService.GetAllAsync();

            if (!tasks.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var taskId = -1;
            while (taskId != 0 && !tasks.Any(team => team.Id == taskId))
            {
                taskId = await GetIdAsync();
            }

            if (taskId == 0)
            {
                return;
            }

            var selectedTask = tasks.First(dto => dto.Id == taskId);
            await _taskService.RemoveAsync(selectedTask.Id);
            await PrintDangerAsync($"Task with ID [{selectedTask.Id}] and name '{selectedTask.Name}' was deleted!");
        }
    }
}

﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Console.IO.Abstract;

namespace TaskManager.Console.IO
{
    public sealed class TaskManagerConsole : BaseConsoleMenu, IConsoleMenu
    {
        #region Fields
        private readonly IReader _reader;
        private readonly ITeamService _teamService;
        private readonly IUserService _userService;
        private readonly IProjectService _projectService;
        private readonly IProjectTaskService _taskService;
        #endregion

        #region Singletone
        private static TaskManagerConsole _instance;
        private static readonly object _lock = new object();

        private TaskManagerConsole(
            IReader reader,
            ITeamService teamService,
            IUserService userService,
            IProjectService projectService,
            IProjectTaskService taskService)
        {
            _reader = reader;
            _teamService = teamService;
            _userService = userService;
            _projectService = projectService;
            _taskService = taskService;
        }
        public static TaskManagerConsole GetInstance(
            IReader reader,
            ITeamService teamService,
            IUserService userService,
            IProjectService projectService,
            IProjectTaskService taskService)
        {
            if (_instance != null) return _instance;
            lock (_lock)
            {
                _instance ??=
                    new TaskManagerConsole(reader, teamService, userService, projectService, taskService);
            }
            return _instance;
        }
        #endregion

        public override async Task DisplayMainMenuAsync()
        {
            const string title = "Please input number of appropriate operation:";
            const string content = "1 : Show LINQ queries menu"
                                   + "\n" + "2 : Show Team menu"
                                   + "\n" + "3 : Show User menu"
                                   + "\n" + "4 : Show Task menu"
                                   + "\n" + "5 : Show Project menu"
                                   + "\n" + "6 : Finish random task with delay"
                ;
            await DisplayMenuAsync(title, content);
            try
            {
                IConsoleMenu consoleMenu = null;
                switch (await _reader.ParseIntNumberAsync(0, 6))
                {
                    case 0:
                        {
                            return;
                        }
                    case 1:
                        {
                            consoleMenu =
                                new LinqQueriesConsoleMenu(_reader, _teamService, _userService, _projectService, _taskService);
                            break;
                        }
                    case 2:
                        {
                            consoleMenu = new TeamCrudConsoleMenu(_teamService, _reader);
                            break;
                        }
                    case 3:
                        {
                            consoleMenu = new UserCrudConsoleMenu(_userService, _teamService, _reader);
                            break;
                        }
                    case 4:
                        {
                            consoleMenu = new ProjectTaskCrudConsoleMenu(_taskService, _projectService, _userService, _reader);
                            break;
                        }
                    case 5:
                        {
                            consoleMenu = new ProjectCrudConsoleConsoleMenu(_projectService, _teamService, _userService, _reader);
                            break;
                        }
                    case 6:
                        {
                            LaunchDelayedTaskAsync(1000);
                            break;
                        }
                }

                if (consoleMenu != null)
                {
                    await consoleMenu.DisplayMainMenuAsync();
                }
            }
            catch (HttpRequestException)
            {
                await PrintExceptionAsync(new HttpRequestException("Please ensure that you are connected to the Internet"));
            }
            catch (Exception ex)
            {
                await PrintExceptionAsync(ex);
            }
        }

        private async void LaunchDelayedTaskAsync(int delayTime = 1000)
        {
            try
            {
                var finishedTaskId = await FinishRandomTaskWithDelay(delayTime);
                await PrintSuccessfulMessageAsync($"\n==================== Task with ID [{finishedTaskId}] has finished! ====================");
            }
            catch (Exception ex)
            {
                await PrintExceptionAsync(ex);
            }
        }

        private Task<int> FinishRandomTaskWithDelay(int delayTime)
        {
            var tcs = new TaskCompletionSource<int>();

            var timer = new Timer(delayTime) { AutoReset = false };
            timer.Elapsed += async (sender, args) =>
            {
                try
                {
                    var finishedTaskId = await _taskService.FinishRandomTask();
                    tcs.SetResult(finishedTaskId);
                }
                catch (Exception ex)
                {
                    var message = $"\n==================== {ex.Message} ====================";
                    tcs.SetException(new Exception(message));
                }
            };
            timer.Start();
            return tcs.Task;
        }
    }
}

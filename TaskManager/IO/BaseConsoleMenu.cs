﻿using System;
using System.Threading.Tasks;

using TaskManager.Console.IO.Abstract;

namespace TaskManager.Console.IO
{
    public abstract class BaseConsoleMenu : IConsoleMenu
    {
        public abstract Task DisplayMainMenuAsync();

        public async Task DisplayMenuAsync(string title, string content)
        {
            System.Console.ForegroundColor = ConsoleColor.Cyan;
            await System.Console.Out.WriteLineAsync("\n" + title);
            System.Console.ForegroundColor = ConsoleColor.Yellow;
            await DisplayBackToMainMenuAsync();
            if (content != null)
                System.Console.WriteLine(content);
            System.Console.ForegroundColor = ConsoleColor.White;
        }

        public async Task DisplayBackToMainMenuAsync()
        {
            await System.Console.Out.WriteLineAsync("0 : Back to Main menu");
        }

        public async Task PrintSuccessfulMessageAsync(string message)
        {
            await PrintMessageAsync(message, ConsoleColor.Green);
        }
        public async Task PrintAlertMessageAsync(string message)
        {
            await PrintMessageAsync(message, ConsoleColor.Yellow);
        }
        public async Task PrintExceptionAsync(Exception exception)
        {
            await PrintDangerAsync(exception.Message);
        }
        public async Task PrintDangerAsync(string message)
        {
            await PrintMessageAsync(message, ConsoleColor.Red);
        }

        public async Task PrintMessageAsync(string message, ConsoleColor textColor = ConsoleColor.White)
        {
            System.Console.ForegroundColor = textColor;
            await System.Console.Out.WriteLineAsync(message);
            System.Console.ForegroundColor = ConsoleColor.White;
        }
    }
}

﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;
using TaskManager.Console.IO.Abstract;

namespace TaskManager.Console.IO
{
    public sealed class ProjectCrudConsoleConsoleMenu : BaseCrudConsoleMenu
    {
        private readonly IProjectService _projectService;
        private readonly ITeamService _teamService;
        private readonly IUserService _userService;

        public ProjectCrudConsoleConsoleMenu(
            IProjectService projectService,
            ITeamService teamService,
            IUserService userService,
            IReader reader)
            : base("Project", reader)
        {
            _projectService = projectService;
            _teamService = teamService;
            _userService = userService;
        }

        public override async Task AddAsync()
        {
            var teams = await _teamService.GetAllAsync();
            var users = await _userService.GetAllAsync();

            if (!users.Any())
            {
                await PrintDangerAsync("There is no users, so we can't create a project (because should be an author)");
                return;
            }
            else if (!teams.Any())
            {
                await PrintDangerAsync("There is no teams, so we can't create a project without executive team");
                return;
            }

            var name = await InputStringAsync("project name");
            if (name == null)
            {
                return;
            }
            var description = await InputStringAsync("project description");
            if (description == null)
            {
                return;
            }

            var deadline = await InputDateAsync("project deadline", DateTime.Now, DateTime.MaxValue);
            if (deadline == default)
            {
                return;
            }

            await PrintAlertMessageAsync("Select a author:");
            var sb = new StringBuilder();

            foreach (var user in users)
            {
                sb.Append("User ID [").Append(user.Id).Append("], name: ")
                    .Append(user.FirstName).Append(" ").AppendLine(user.LastName);
            }

            await PrintMessageAsync(sb.ToString());

            var authorId = -1;
            while (authorId != 0 && !users.Any(user => user.Id == authorId))
            {
                authorId = await GetIdAsync();
            }

            if (authorId == 0)
            {
                return;
            }

            await PrintAlertMessageAsync("Select a team:");
            sb.Clear();

            foreach (var team in teams)
            {
                sb.Append("Team ID [").Append(team.Id).Append("], name: ").AppendLine(team.Name);
            }

            await PrintMessageAsync(sb.ToString());

            var teamId = -1;
            while (teamId != 0 && !teams.Any(team => team.Id == teamId))
            {
                teamId = await GetIdAsync();
            }

            if (teamId == 0)
            {
                return;
            }

            var newProject = new ProjectCreateDTO()
            {
                Name = name,
                Description = description,
                Deadline = deadline,
                AuthorId = authorId,
                TeamId = teamId
            };

            var addedProject = await _projectService.AddAsync(newProject);
            await PrintSuccessfulMessageAsync($"Added Project with ID [{addedProject.Id}] and name {addedProject.Name}. Executive Team ID [{addedProject.TeamId}]");
        }

        public override async Task ShowAsync()
        {
            var projects = await _projectService.GetAllAsync();

            if (!projects.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var projectId = -1;
            while (projectId != 0 && !projects.Any(project => project.Id == projectId))
            {
                projectId = await GetIdAsync();
            }

            if (projectId == 0)
            {
                return;
            }

            var project = await _projectService.GetAsync(projectId);
            await PrintSuccessfulMessageAsync($"Project with ID [{project.Id}] and name '{project.Name}', executive Team ID [{project.TeamId}]");
        }

        public override async Task ShowAllAsync()
        {
            var projects = await _projectService.GetAllAsync();
            var stringBuilder = new StringBuilder();
            foreach (var project in projects)
            {
                stringBuilder
                    .Append("Project with ID [").Append(project.Id)
                    .Append("] and name '").Append(project.Name).Append("'")
                    .Append(", executive Team Id [").Append(project.TeamId).AppendLine("].");
            }
            await PrintSuccessfulMessageAsync("All projects:");
            await PrintAlertMessageAsync(stringBuilder.ToString());
        }

        public override async Task UpdateAsync()
        {
            var projects = await _projectService.GetAllAsync();

            if (!projects.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var projectId = -1;
            while (projectId != 0 && !projects.Any(project => project.Id == projectId))
            {
                projectId = await GetIdAsync();
            }

            if (projectId == 0)
            {
                return;
            }

            var projectToUpdate = projects.First(project => project.Id == projectId);

            const string title = "What do you wanna change:";
            const string content = "1 : Project Name"
                                   + "\n" + "2 : Project description"
                                   + "\n" + "3 : Executive Team ID"
                                   + "\n" + "4 : Project deadline"
                                   + "\n" + "5 : Save"
                ;
            var operationNumber = 0;
            do
            {
                try
                {
                    await DisplayMenuAsync(title, content);
                    operationNumber = await Reader.ParseIntNumberAsync(0, 5);
                    switch (operationNumber)
                    {
                        case 0:
                            {
                                return;
                            }
                        case 1:
                            {
                                var newName = await InputStringAsync("project name");
                                if (newName == null)
                                {
                                    return;
                                }

                                projectToUpdate.Name = newName;
                                break;
                            }
                        case 2:
                            {
                                var description = await InputStringAsync("project description");
                                if (description == null)
                                {
                                    return;
                                }

                                projectToUpdate.Description = description;
                                break;
                            }
                        case 3:
                            {
                                var teams = await _teamService.GetAllAsync();
                                await PrintAlertMessageAsync("Select a team:");
                                var sb = new StringBuilder();

                                foreach (var team in teams)
                                {
                                    sb.Append("Team ID [").Append(team.Id).Append("], name: ").AppendLine(team.Name);
                                }

                                await PrintMessageAsync(sb.ToString());

                                var teamId = -1;
                                while (teamId != 0 && !teams.Any(team => team.Id == teamId))
                                {
                                    teamId = await GetIdAsync();
                                }

                                if (teamId == 0)
                                {
                                    return;
                                }

                                projectToUpdate.TeamId = teamId;
                                break;
                            }
                        case 4:
                            {
                                var deadline = await InputDateAsync("project deadline", DateTime.Now, DateTime.MaxValue);
                                if (deadline == default)
                                {
                                    return;
                                }

                                projectToUpdate.Deadline = deadline;
                                break;
                            }
                        case 5:
                            {
                                continue;
                            }
                    }
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            } while (operationNumber > 0 && operationNumber < 5);

            await _projectService.UpdateAsync(projectToUpdate);

            await PrintSuccessfulMessageAsync($"Project with ID [{projectToUpdate.Id}] has updated!");
            await PrintAlertMessageAsync($"Project name: {projectToUpdate.Name}, deadline {projectToUpdate.Deadline}," +
                                    $" executive Team ID [{projectToUpdate.TeamId}].\nDescription:\n{projectToUpdate.Description}");
        }

        public override async Task RemoveAsync()
        {
            var projects = await _projectService.GetAllAsync();

            if (!projects.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var projectId = -1;
            while (projectId != 0 && !projects.Any(dto => dto.Id == projectId))
            {
                projectId = await GetIdAsync();
            }

            if (projectId == 0)
            {
                return;
            }

            var selectedEntity = projects.First(dto => dto.Id == projectId);
            await _projectService.RemoveAsync(selectedEntity.Id);
            await PrintDangerAsync($"Project with ID [{selectedEntity.Id}] and name '{selectedEntity.Name}' was deleted!");
        }
    }
}

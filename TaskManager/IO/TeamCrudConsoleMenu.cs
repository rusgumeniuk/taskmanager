﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;
using TaskManager.Console.IO.Abstract;

namespace TaskManager.Console.IO
{
    public sealed class TeamCrudConsoleMenu : BaseCrudConsoleMenu
    {
        private readonly ITeamService _teamService;
        public TeamCrudConsoleMenu(ITeamService teamService, IReader reader) : base("Team", reader)
        {
            _teamService = teamService;
        }

        public override async Task AddAsync()
        {
            var newTeam = new TeamCreateDTO();
            var newTeamName = string.Empty;
            await PrintMessageAsync("Input new team name or 0 to exit");

            while (string.IsNullOrWhiteSpace(newTeamName))
            {
                try
                {
                    newTeamName = await Reader.GetNotEmptyStringAsync();
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            }

            if (int.TryParse(newTeamName, out var res) && res == 0)
            {
                return;
            }

            newTeam.Name = newTeamName;
            var addedTeam = await _teamService.AddAsync(newTeam);
            await PrintSuccessfulMessageAsync($"Added Team with ID [{addedTeam.Id}] and name '{addedTeam.Name}'");
        }

        public override async Task ShowAsync()
        {
            var teams = await _teamService.GetAllAsync();

            if (!teams.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var teamId = -1;
            while (teamId != 0 && !teams.Any(team => team.Id == teamId))
            {
                teamId = await GetIdAsync();
            }

            if (teamId == 0)
            {
                return;
            }

            var team = await _teamService.GetAsync(teamId);
            await PrintSuccessfulMessageAsync($"Team with ID [{team.Id}] and name '{team.Name}'");
        }

        public override async Task ShowAllAsync()
        {
            var teams = await _teamService.GetAllAsync();
            var stringBuilder = new StringBuilder();
            foreach (var team in teams)
            {
                stringBuilder
                    .Append("Team with ID [").Append(team.Id)
                    .Append("] and name '").Append(team.Name).AppendLine("'");
            }
            await PrintSuccessfulMessageAsync("Teams:");
            await PrintAlertMessageAsync(stringBuilder.ToString());
        }

        public override async Task UpdateAsync()
        {
            var teams = await _teamService.GetAllAsync();

            if (!teams.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var teamId = -1;
            while (teamId != 0 && !teams.Any(team => team.Id == teamId))
            {
                teamId = await GetIdAsync();
            }

            if (teamId == 0)
            {
                return;
            }

            var teamToUpdate = teams.First(team => team.Id == teamId);

            var newTeamName = string.Empty;
            await PrintMessageAsync("Input new team name or 0 to exit");

            while (string.IsNullOrWhiteSpace(newTeamName))
            {
                newTeamName = await Reader.GetNotEmptyStringAsync();
            }

            if (int.TryParse(newTeamName, out var res) && res == 0)
            {
                return;
            }

            teamToUpdate.Name = newTeamName;
            await _teamService.UpdateAsync(teamToUpdate);

            await PrintSuccessfulMessageAsync($"Team with ID [{teamToUpdate.Id}] has updated!");
            await PrintAlertMessageAsync($"Updated team name '{teamToUpdate.Name}'");
        }

        public override async Task RemoveAsync()
        {
            var teams = await _teamService.GetAllAsync();

            if (!teams.Any())
            {
                await PrintMessageAboutEmptyCollection();
                return;
            }

            await ShowAllAsync();

            var teamId = -1;
            while (teamId != 0 && !teams.Any(team => team.Id == teamId))
            {
                teamId = await GetIdAsync();
            }

            if (teamId == 0)
            {
                return;
            }

            var selectedTeam = teams.First(team => team.Id == teamId);
            await _teamService.RemoveAsync(selectedTeam.Id);
            await PrintDangerAsync($"Team with ID [{selectedTeam.Id}] and name '{selectedTeam.Name}' was deleted!");
        }
    }
}

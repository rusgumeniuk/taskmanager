﻿using System;
using System.Threading.Tasks;

namespace TaskManager.Console.IO.Abstract
{
    public interface IReader
    {
        Task<decimal> ParseDecimalNumberAsync(string @string = null);
        Task<decimal> ParseDecimalNumberAsync(decimal min, string @string = null);
        Task<decimal> ParseDecimalNumberAsync(decimal min, decimal max, string @string = null);

        Task<int> ParseIntNumberAsync(string @string = null);
        Task<int> ParseIntNumberAsync(int min, string @string = null);
        Task<int> ParseIntNumberAsync(int min, int max, string @string = null);

        Task<DateTime> ParseDateTimeAsync(string format = "dd/MM/yyyy", string @string = null);
        Task<DateTime> ParseDateTimeAsync(DateTime min, string format = "dd/MM/yyyy", string @string = null);
        Task<DateTime> ParseDateTimeAsync(DateTime min, DateTime max, string format = "dd/MM/yyyy", string @string = null);

        Task<string> GetNotEmptyStringAsync(string @string = null);
        Task<string> GetEmailAsync(string @string = null);
    }
}

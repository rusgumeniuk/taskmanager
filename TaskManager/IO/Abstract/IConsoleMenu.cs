﻿using System;
using System.Threading.Tasks;

namespace TaskManager.Console.IO.Abstract
{
    public interface IConsoleMenu
    {
        Task DisplayMainMenuAsync();
        Task DisplayMenuAsync(string title, string content);
        Task DisplayBackToMainMenuAsync();

        Task PrintSuccessfulMessageAsync(string message);
        Task PrintAlertMessageAsync(string message);
        Task PrintExceptionAsync(Exception exception);
        Task PrintDangerAsync(string message);
        Task PrintMessageAsync(string message, ConsoleColor color = ConsoleColor.White);
    }
}

﻿using System.Threading.Tasks;

namespace TaskManager.Console.IO.Abstract
{
    public interface ICrudConsoleMenu : IConsoleMenu
    {
        Task AddAsync();
        Task ShowAsync();
        Task ShowAllAsync();
        Task UpdateAsync();
        Task RemoveAsync();
    }
}

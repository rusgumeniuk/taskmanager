﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.Console.IO.Abstract;

namespace TaskManager.Console.IO
{
    public abstract class BaseCrudConsoleMenu : BaseConsoleMenu, ICrudConsoleMenu
    {
        private readonly string _entityName;
        protected readonly IReader Reader;

        protected BaseCrudConsoleMenu(string entityName, IReader reader)
        {
            _entityName = entityName;
            Reader = reader;
        }

        public override async Task DisplayMainMenuAsync()
        {
            const string title = "Please input number of appropriate operation:";
            var content = $"1 : Get 1 entity of {_entityName} type"
                          + "\n" + $"2 : Get all {_entityName}s"
                          + "\n" + $"3 : Create a new {_entityName}"
                          + "\n" + $"4 : Update a {_entityName}"
                          + "\n" + $"5 : Remove a {_entityName}"
                ;
            await DisplayMenuAsync(title, content);

            switch (await Reader.ParseIntNumberAsync(0, 5))
            {
                case 0:
                    {
                        return;
                    }
                case 1:
                    {
                        await ShowAsync();
                        break;
                    }
                case 2:
                    {
                        await ShowAllAsync();
                        break;
                    }
                case 3:
                    {
                        await AddAsync();
                        break;
                    }
                case 4:
                    {
                        await UpdateAsync();
                        break;
                    }
                case 5:
                    {
                        await RemoveAsync();
                        break;
                    }
                default:
                    throw new InvalidOperationException("Unknown operation number");
            }
        }

        public abstract Task AddAsync();
        public abstract Task ShowAsync();
        public abstract Task ShowAllAsync();
        public abstract Task UpdateAsync();
        public abstract Task RemoveAsync();

        protected async Task PrintMessageAboutEmptyCollection()
        {
            await PrintDangerAsync(
                $"There is empty collection of {_entityName}s, so you can do you want until some element will be added");
        }

        protected async Task<int> InputIntAsync(string name, int min = 1, int max = int.MaxValue)
        {
            while (true)
            {
                try
                {
                    await PrintMessageAsync($"Input {name} or {min - 1} to exit");

                    return await Reader.ParseIntNumberAsync(min - 1, max);
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            }
        }

        protected async Task<DateTime> InputDateAsync(string name, DateTime min, DateTime max)
        {
            while (true)
            {
                try
                {
                    await PrintMessageAsync($"Input {name} or 0 to exit");

                    var date = await Reader.GetNotEmptyStringAsync();
                    if (int.TryParse(date, out var res) && res == 0)
                    {
                        return default;
                    }

                    return await Reader.ParseDateTimeAsync(min, max, @string: date);
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            }
        }

        protected async Task<string> InputEmailAsync()
        {
            while (true)
            {
                try
                {
                    await PrintMessageAsync("Input email or 0 to exit");

                    var value = await Reader.GetNotEmptyStringAsync();
                    if (int.TryParse(value, out var res) && res == 0)
                    {
                        return null;
                    }

                    return await Reader.GetEmailAsync(value);
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            }
        }

        protected async Task<string> InputStringAsync(string name)
        {
            while (true)
            {
                try
                {
                    await PrintMessageAsync($"Input new {name} or 0 to exit");

                    var newName = await Reader.GetNotEmptyStringAsync();
                    if (int.TryParse(newName, out var res) && res == 0)
                    {
                        return null;
                    }

                    return newName;
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            }
        }

        protected async Task<int> GetIdAsync(int min = 0, int max = int.MaxValue)
        {
            while (true)
            {
                await DisplayMenuAsync("Input the Id (number) or 0 to go to Main menu", null);
                try
                {
                    return await Reader.ParseIntNumberAsync(min, max);
                }
                catch (HttpRequestException)
                {
                    await PrintExceptionAsync(new HttpRequestException("Please ensure that you are connected to the Internet"));
                }
                catch (Exception ex)
                {
                    await PrintExceptionAsync(ex);
                }
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;

namespace TaskManager.Console.Services
{
    internal sealed class ProjectHttpService : BaseService<ProjectDTO, ProjectCreateDTO>, IProjectService
    {
        private const string ProjectsUrl = "https://localhost:44332/api/projects";

        public ProjectHttpService(IHttpClient httpClient) : base(httpClient, ProjectsUrl)
        {
        }

        public async Task<Dictionary<ProjectDTO, int>> GetNumberOfTasksOfUserAsync(int userId)
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Get, $"{ProjectsUrl}/getNumberOfTasks/{userId}", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<IEnumerable<ProjectTaskCountDTO>>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result.ToDictionary(
                item => item.Project,
                item => item.CountOfTasks
                );
        }

        public async Task<IEnumerable<ProjectTasksStaffInfo>> GetProjectInfoAsync()
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Get, $"{ProjectsUrl}/GetProjectInfo", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<IEnumerable<ProjectTasksStaffInfo>>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }
    }
}
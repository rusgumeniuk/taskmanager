﻿using Newtonsoft.Json;

using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager.Console.Services
{
    internal sealed class TaskManagerHttpClient : IHttpClient
    {
        private readonly HttpClient _httpClient;

        public TaskManagerHttpClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task ThrowExceptionIfNotSuccess(HttpResponseMessage response, string message = null)
        {
            if (response.IsSuccessStatusCode) return;

            var errorMessage = message ?? await response.Content.ReadAsStringAsync();

            switch (response.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    {
                        try
                        {
                            var errors = JsonConvert.DeserializeXNode(errorMessage, "response");
                            if (errors?.Root?.Element("errors") == null)
                            {
                                throw new Exception(errorMessage);
                            }
                            else
                            {
                                var stringBuilder = new StringBuilder();
                                foreach (var error in errors.Root.Element("errors").Elements())
                                {
                                    stringBuilder.Append(error.Name).Append(" : ").AppendLine(error.Value);
                                }

                                throw new Exception(stringBuilder.ToString());
                            }
                        }
                        catch (Exception)
                        {
                            throw new Exception(errorMessage);
                        }
                    }
                case HttpStatusCode.InternalServerError:
                    {
                        try
                        {
                            var responseTag = JsonConvert.DeserializeXNode(errorMessage, "response");
                            var exceptionMessage = responseTag?.Root?.Element("error")?.Value;
                            throw new Exception(exceptionMessage ?? errorMessage);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(!string.IsNullOrWhiteSpace(ex.Message) ? ex.Message : errorMessage);
                        }
                    }
                default:
                    throw new Exception(errorMessage);
            }
        }

        public async Task<T> GetResponseResultOrDefaultAsync<T>(HttpResponseMessage responseMessage)
        {
            var result = await GetResponseResultAsync(responseMessage);
            return responseMessage.IsSuccessStatusCode && !string.IsNullOrWhiteSpace(result) ? JsonConvert.DeserializeObject<T>(result) : default;
        }

        public Task<string> GetResponseResultAsync(HttpResponseMessage response)
        {
            return response.Content.ReadAsStringAsync();
        }

        public Task<HttpResponseMessage> SendRequestAsync(HttpMethod httpMethod, string url, object contentToSerialize)
        {
            var jsonObject = JsonConvert.SerializeObject(contentToSerialize);
            var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");

            var message = new HttpRequestMessage(httpMethod, url)
            {
                Content = content
            };

            return _httpClient.SendAsync(message);
        }
    }
}
﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;

namespace TaskManager.Console.Services
{
    public abstract class BaseService<TEntity, TEntityCreate> : ICrudService<TEntity, TEntityCreate, int>
        where TEntity : class
        where TEntityCreate : class
    {
        private readonly string _url;
        protected readonly IHttpClient HttpClient;

        protected BaseService(IHttpClient httpClient, string baseUrl)
        {
            HttpClient = httpClient;
            _url = baseUrl;
        }

        public void Dispose()
        {
        }

        public async Task<TEntity> AddAsync(TEntityCreate dto)
        {
            var response = await HttpClient.SendRequestAsync(HttpMethod.Post, _url, dto);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<TEntity>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }

        public async Task UpdateAsync(TEntity dto)
        {
            var response = await HttpClient.SendRequestAsync(HttpMethod.Put, _url, dto);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
        }

        public async Task RemoveAsync(int id)
        {
            var response = await HttpClient.SendRequestAsync(HttpMethod.Delete, $"{_url}/{id}", null);
            var result = await HttpClient.GetResponseResultAsync(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response, result);
        }

        public async Task<TEntity> GetAsync(int id, bool isNoTracking = false)
        {
            var response = await HttpClient.SendRequestAsync(HttpMethod.Get, $"{_url}/{id}", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<TEntity>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            var response = await HttpClient.SendRequestAsync(HttpMethod.Get, _url, null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<IEnumerable<TEntity>>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }
    }
}
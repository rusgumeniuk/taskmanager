﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;

namespace TaskManager.Console.Services
{
    internal sealed class ProjectTaskHttpService : BaseService<ProjectTaskDTO, ProjectTaskCreateDTO>, IProjectTaskService
    {
        private const string TasksUrl = "https://localhost:44332/api/tasks";

        public ProjectTaskHttpService(IHttpClient httpClient) : base(httpClient, TasksUrl)
        {
        }

        public async Task<IEnumerable<ProjectTaskDTO>> GetTasksOfUserWithTaskNameLessThan45Async(int userId)
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Get, $"{TasksUrl}/getTasksWithSmallName/{userId}", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<IEnumerable<ProjectTaskDTO>>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }

        public async Task<IEnumerable<TaskShortInfo>> GetFinishedTasksInTheCurrentYearByUserAsync(int userId)
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Get, $"{TasksUrl}/getFinishedInThisYearByUser/{userId}", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<IEnumerable<TaskShortInfo>>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }

        public async Task<UserTasksProfile> GetActiveUserTasksAsync(int userId)
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Get, $"{TasksUrl}/getActiveTasks/{userId}", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<UserTasksProfile>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }

        public async Task<UserProfileWithLastProjectAndLongestTaskAndTaskStatistics> GetUserInfoAsync(int userId)
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Get, $"{TasksUrl}/getUserStatistics/{userId}", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<UserProfileWithLastProjectAndLongestTaskAndTaskStatistics>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }

        public async Task<int> FinishRandomTask()
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Put, $"{TasksUrl}/finishRandomTask", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<int>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }
    }
}
﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;

namespace TaskManager.Console.Services
{
    internal sealed class TeamHttpService : BaseService<TeamDTO, TeamCreateDTO>, ITeamService
    {
        private const string TeamUrl = "https://localhost:44332/api/teams";

        public TeamHttpService(IHttpClient httpClient) : base(httpClient, TeamUrl)
        {
        }

        public async Task<IEnumerable<TeamWithMembersDTO>> GetTeamsWithMembersOlderThan10YearsOrderedByRegistrationTimeAsync()
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Get, $"{TeamUrl}/getTeamsWithOldMember", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<IEnumerable<TeamWithMembersDTO>>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }
    }
}
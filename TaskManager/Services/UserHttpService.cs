﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.BLL.Services.Abstract;
using TaskManager.Common.DTO;

namespace TaskManager.Console.Services
{
    internal sealed class UserHttpService : BaseService<UserDTO, UserCreateDTO>, IUserService
    {
        private const string UsersUrl = "https://localhost:44332/api/users";

        public UserHttpService(IHttpClient httpClient) : base(httpClient, UsersUrl)
        {
        }

        public async Task<bool> ExistUserAsync(int userId)
        {
            var response = await HttpClient.SendRequestAsync(HttpMethod.Get, $"{UsersUrl}/exist/{userId}", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<bool>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }

        public async Task<IEnumerable<UserTasksProfile>> GetUsersOrderedByFirstNameSortedByTaskNameAsync()
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Get, $"{UsersUrl}/GetOrderedByFirstNameAndSortedByTaskNameUsers", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<IEnumerable<UserTasksProfile>>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }

        public async Task<IEnumerable<UserDTO>> GetUsersByProjectId(int projectId)
        {
            var response = await HttpClient
                .SendRequestAsync(HttpMethod.Get, $"{UsersUrl}/getUsersByProject", null);
            var result = await HttpClient.GetResponseResultOrDefaultAsync<IEnumerable<UserDTO>>(response);
            await HttpClient.ThrowExceptionIfNotSuccess(response);
            return result;
        }
    }
}
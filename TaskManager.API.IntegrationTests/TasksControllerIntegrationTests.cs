﻿using Newtonsoft.Json;

using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.Common.DTO;
using TaskManager.DAL.Models;

using Xunit;

namespace TaskManager.API.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        #region Set up / tear down
        private readonly HttpClient _httpClient;
        private const string BaseUrl = "/api/tasks";

        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _httpClient = factory.CreateClient();
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
        #endregion

        #region Remove
        [Fact]
        public async Task Remove_WhenValidId_ReturnsNoContent()
        {
            var user = new UserCreateDTO() { BirthDay = DateTime.Now, FirstName = "Amador", Email = "amador@g.c" };
            var team = new TeamCreateDTO() { Name = "Amador team" };

            var createUserResponse = await _httpClient.PostAsync("/api/users", ContentHelper.GetStringContent(user));
            var createTeamResponse = await _httpClient.PostAsync("/api/teams", ContentHelper.GetStringContent(team));

            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(await createTeamResponse.Content.ReadAsStringAsync());
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await createUserResponse.Content.ReadAsStringAsync());

            var newProject = new ProjectCreateDTO()
            {
                Name = "New project",
                Description = "Soooooooooooooooooooooooooooome description",
                Deadline = new DateTime(2030, 10, 10),
                AuthorId = createdUser.Id,
                TeamId = createdTeam.Id
            };
            var createProjectResponse = await _httpClient.PostAsync("/api/projects", ContentHelper.GetStringContent(newProject));
            var createdProject = JsonConvert.DeserializeObject<ProjectDTO>(await createProjectResponse.Content.ReadAsStringAsync());

            var task = new ProjectTaskCreateDTO()
            {
                Name = "New task",
                Description = "Useful description",
                PerformerId = createdUser.Id,
                ProjectId = createdProject.Id
            };

            var createTaskResponse = await _httpClient.PostAsync(BaseUrl, ContentHelper.GetStringContent(task));
            var createdTask = JsonConvert.DeserializeObject<ProjectTaskDTO>(await createTaskResponse.Content.ReadAsStringAsync());

            var requestUri = $"{BaseUrl}/{createdTask.Id}";
            var deleteTaskResponse = await _httpClient.DeleteAsync(requestUri);

            var getTaskResponse = await _httpClient.GetAsync(requestUri);

            Assert.Equal(HttpStatusCode.NoContent, deleteTaskResponse.StatusCode);
            Assert.Equal(HttpStatusCode.NotFound, getTaskResponse.StatusCode);
        }

        [Fact]
        public async Task Remove_WhenNotExistingId_ReturnsNotFound()
        {
            var removeUri = $"{BaseUrl}/1000";
            var removeResponse = await _httpClient.DeleteAsync(removeUri);

            Assert.Equal(HttpStatusCode.NotFound, removeResponse.StatusCode);
        }

        [Theory]
        [InlineData("notId")]
        [InlineData("9223372036854775807")]
        public async Task Remove_WhenInvalidUriParameter_ReturnsNotFound(string uriParameter)
        {
            var removeUri = $"{BaseUrl}/{uriParameter}";
            var removeResponse = await _httpClient.DeleteAsync(removeUri);

            Assert.Equal(HttpStatusCode.NotFound, removeResponse.StatusCode);
        }
        #endregion

        #region GetActiveTasks
        [Theory]
        [InlineData("asdasd")]
        [InlineData("1000000000000000000000000000000000000")]
        public async Task GetActiveTasks_WhenInvalidId_ReturnsNotFound(string id)
        {
            var getActiveTasksUri = $"{BaseUrl}/getActiveTasks/{id}";
            var getResponse = await _httpClient.GetAsync(getActiveTasksUri);

            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }

        [Fact]
        public async Task GetActiveTasks_NotExistingId_ReturnsNotFound()
        {
            var getActiveTasksUri = $"{BaseUrl}/getActiveTasks/{100}";
            var getResponse = await _httpClient.GetAsync(getActiveTasksUri);

            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }

        [Fact]
        public async Task GetActiveTasks_WhenExistUserWith2ActiveTasksAnd1Finished_ReturnsNotEmpty()
        {
            var userCreateDto = new UserCreateDTO() { BirthDay = DateTime.Now, FirstName = "Amador", Email = "amador@g.c" };
            var teamCreateDto = new TeamCreateDTO() { Name = "Amador team" };

            var createUserResponse = await _httpClient.PostAsync("/api/users", ContentHelper.GetStringContent(userCreateDto));
            var createTeamResponse = await _httpClient.PostAsync("/api/teams", ContentHelper.GetStringContent(teamCreateDto));

            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(await createTeamResponse.Content.ReadAsStringAsync());
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await createUserResponse.Content.ReadAsStringAsync());

            var projectCreateDto = new ProjectCreateDTO()
            {
                Name = "New project",
                Description = "Soooooooooooooooooooooooooooome description",
                Deadline = new DateTime(2030, 10, 10),
                AuthorId = createdUser.Id,
                TeamId = createdTeam.Id
            };
            var createProjectResponse = await _httpClient.PostAsync("/api/projects", ContentHelper.GetStringContent(projectCreateDto));
            var createdProject = JsonConvert.DeserializeObject<ProjectDTO>(await createProjectResponse.Content.ReadAsStringAsync());


            var createTaskDto = new ProjectTaskCreateDTO() { Name = "First", Description = "decs", PerformerId = createdUser.Id, ProjectId = createdProject.Id };
            await _httpClient.PostAsync(BaseUrl, ContentHelper.GetStringContent(createTaskDto));

            createTaskDto.Name = "Second";
            var secondTaskResponse = await _httpClient.PostAsync(BaseUrl, ContentHelper.GetStringContent(createTaskDto));

            createTaskDto.Name = "Third";
            await _httpClient.PostAsync(BaseUrl, ContentHelper.GetStringContent(createTaskDto));


            var secondTask = JsonConvert.DeserializeObject<ProjectTaskDTO>(await secondTaskResponse.Content.ReadAsStringAsync());
            secondTask.State = (int)TaskState.Finished;
            secondTask.FinishedAt = new DateTime(2020, 10, 10);
            await _httpClient.PutAsync(BaseUrl, ContentHelper.GetStringContent(secondTask));


            var getActiveTasksUri = $"{BaseUrl}/getActiveTasks/{createdUser.Id}";
            var getActiveTasksResponse = await _httpClient.GetAsync(getActiveTasksUri);
            var result = JsonConvert.DeserializeObject<UserTasksProfile>(await getActiveTasksResponse.Content.ReadAsStringAsync());


            Assert.Equal(HttpStatusCode.OK, getActiveTasksResponse.StatusCode);
            Assert.Equal(createdUser.Id, result.User.Id);
            Assert.Equal(2, result.Tasks.Count());
        }
        #endregion

        #region GetUserInfoAboutProjectAndTasks
        [Theory]
        [InlineData("asdasd")]
        [InlineData("1000000000000000000000000000000000000")]
        public async Task GetUserInfoAboutProjectAndTasks_WhenInvalidId_ReturnsNotFound(string id)
        {
            var getStaticticUri = $"{BaseUrl}/getUserStatistics/{id}";
            var getResponse = await _httpClient.GetAsync(getStaticticUri);

            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }

        [Fact]
        public async Task GetUserInfoAboutProjectAndTasks_NotExistingId_ReturnsNotFound()
        {
            var getStaticticUri = $"{BaseUrl}/getUserStatistics/{100}";
            var getResponse = await _httpClient.GetAsync(getStaticticUri);

            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }

        [Fact]
        public async Task GetUserInfoAboutProjectAndTasks_WhenExistUserWith2ActiveTasksAnd1Finished_ReturnsNotEmpty()
        {
            var userCreateDto = new UserCreateDTO() { BirthDay = DateTime.Now, FirstName = "Amador", Email = "amador@g.c" };
            var teamCreateDto = new TeamCreateDTO() { Name = "Amador team" };

            var createUserResponse = await _httpClient.PostAsync("/api/users", ContentHelper.GetStringContent(userCreateDto));
            var createTeamResponse = await _httpClient.PostAsync("/api/teams", ContentHelper.GetStringContent(teamCreateDto));

            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(await createTeamResponse.Content.ReadAsStringAsync());
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await createUserResponse.Content.ReadAsStringAsync());

            var projectCreateDto = new ProjectCreateDTO()
            {
                Name = "New project",
                Description = "Soooooooooooooooooooooooooooome description",
                Deadline = new DateTime(2030, 10, 10),
                AuthorId = createdUser.Id,
                TeamId = createdTeam.Id
            };
            var createProjectResponse = await _httpClient.PostAsync("/api/projects", ContentHelper.GetStringContent(projectCreateDto));
            var createdProject = JsonConvert.DeserializeObject<ProjectDTO>(await createProjectResponse.Content.ReadAsStringAsync());


            var createTaskDto = new ProjectTaskCreateDTO() { Name = "First", Description = "decs", PerformerId = createdUser.Id, ProjectId = createdProject.Id };
            await _httpClient.PostAsync(BaseUrl, ContentHelper.GetStringContent(createTaskDto));

            createTaskDto.Name = "Second";
            createTaskDto.Description = "LONGEST";
            await _httpClient.PostAsync(BaseUrl, ContentHelper.GetStringContent(createTaskDto));

            createTaskDto.Name = "Last";
            createTaskDto.Description = "deskx";
            await _httpClient.PostAsync(BaseUrl, ContentHelper.GetStringContent(createTaskDto));


            var getActiveTasksUri = $"{BaseUrl}/getUserStatistics/{createdUser.Id}";
            var getActiveTasksResponse = await _httpClient.GetAsync(getActiveTasksUri);
            var result = JsonConvert.DeserializeObject<UserProfileWithLastProjectAndLongestTaskAndTaskStatistics>(await getActiveTasksResponse.Content.ReadAsStringAsync());

            Assert.Equal(HttpStatusCode.OK, getActiveTasksResponse.StatusCode);

            Assert.Equal(userCreateDto.Email, result.User.Email);
            Assert.Equal(projectCreateDto.Name, result.LastProject.Name);
            Assert.Equal("First", result.LongestTask.Name);
            Assert.Equal(3, result.CountOfUnfinishedTasks);
            Assert.Equal(3, result.CountOfTasksInLastProject);
        }

        //getUserStatistics

        #endregion
    }
}

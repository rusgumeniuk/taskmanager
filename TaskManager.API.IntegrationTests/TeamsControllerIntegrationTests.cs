﻿using Newtonsoft.Json;

using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using TaskManager.Common.DTO;

using Xunit;

namespace TaskManager.API.IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _httpClient;
        private const string BaseUrl = "/api/teams";

        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _httpClient = factory.CreateClient();
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }

        [Fact]
        public async Task Create_WhenValidTeam_ReturnsCreated()
        {
            var currentDateTime = DateTime.Now;
            var newTeam = new TeamCreateDTO()
            {
                Name = $"New team {currentDateTime.ToShortTimeString()}"
            };

            var body = ContentHelper.GetStringContent(newTeam);
            var createResponse = await _httpClient.PostAsync(BaseUrl, body);

            var returnedTeam = JsonConvert.DeserializeObject<TeamDTO>(await createResponse.Content.ReadAsStringAsync());

            var getRequestUri = $"{BaseUrl}/{returnedTeam.Id}";
            var getResponse = await _httpClient.GetAsync(getRequestUri);

            var getTeam = JsonConvert.DeserializeObject<TeamDTO>(await getResponse.Content.ReadAsStringAsync());

            Assert.Equal(HttpStatusCode.Created, createResponse.StatusCode);
            Assert.Equal(newTeam.Name, returnedTeam.Name);
            Assert.True(returnedTeam.CreatedAt >= currentDateTime);
            Assert.NotEqual(0, returnedTeam.Id);

            Assert.NotNull(getTeam);
        }

        [Theory]
        [InlineData("")]
        [InlineData("{\"name\": \"\" } ")]
        [InlineData("{\"name\": \"a\" } ")]
        [InlineData("{\"name\": \"tolongnamemaximumis100tolongnamemaximumis100" +
                    "tolongnamemaximumis100tolongnamemaximumis100tolongnamemaximumis100" +
                    "tolongnamemaximumis100tolongnamemaximumis100tolongnamemaximumis100\" } ")]
        public async Task Create_WhenInvalidBody_ReturnBadRequest(string jsonInString)
        {
            var createResponse = await _httpClient
                .PostAsync(BaseUrl, new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, createResponse.StatusCode);
        }
    }
}

﻿using Newtonsoft.Json;

using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.Common.DTO;

using Xunit;

namespace TaskManager.API.IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _httpClient;
        private const string BaseUrl = "/api/projects";

        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _httpClient = factory.CreateClient();
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }

        [Fact]
        public async Task Create_WhenValidProject_ReturnsCreated()
        {
            var user = new UserCreateDTO() { BirthDay = DateTime.Now, FirstName = "Amador", Email = "amador@g.c" };
            var team = new TeamCreateDTO() { Name = "Amador team" };

            var createUserResponse = await _httpClient.PostAsync("/api/users", ContentHelper.GetStringContent(user));
            var createTeamResponse = await _httpClient.PostAsync("/api/teams", ContentHelper.GetStringContent(team));

            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(await createTeamResponse.Content.ReadAsStringAsync());
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await createUserResponse.Content.ReadAsStringAsync());

            var currentDateTime = DateTime.Now;
            var newProject = new ProjectCreateDTO()
            {
                Name = $"New project {currentDateTime.ToShortTimeString()}",
                Description = "Soooooooooooooooooooooooooooome description",
                Deadline = new DateTime(2030, 10, 10),
                AuthorId = createdUser.Id,
                TeamId = createdTeam.Id
            };

            var body = ContentHelper.GetStringContent(newProject);
            var createResponse = await _httpClient.PostAsync(BaseUrl, body);

            var returnedProject = JsonConvert.DeserializeObject<ProjectDTO>(await createResponse.Content.ReadAsStringAsync());

            var getRequestUri = $"{BaseUrl}/{returnedProject.Id}";
            var getResponse = await _httpClient.GetAsync(getRequestUri);

            var getProject = JsonConvert.DeserializeObject<ProjectDTO>(await getResponse.Content.ReadAsStringAsync());

            Assert.Equal(HttpStatusCode.Created, createResponse.StatusCode);
            Assert.Equal(newProject.Name, returnedProject.Name);

            Assert.NotEqual(0, returnedProject.Id);

            Assert.NotNull(getProject);
        }

        [Fact]
        public async Task Create_WhenNotExistingAuthorAndTeam_ReturnsBadRequest()
        {
            var newProject = new ProjectCreateDTO()
            {
                Name = "New project",
                Description = "Desc",
                Deadline = new DateTime(2030, 10, 10),
                AuthorId = 100,
                TeamId = 100
            };

            var body = ContentHelper.GetStringContent(newProject);
            var createResponse = await _httpClient.PostAsync(BaseUrl, body);

            Assert.Equal(HttpStatusCode.BadRequest, createResponse.StatusCode);
        }

        [Fact]
        public async Task Create_WhenNullBody_ReturnsBadRequest()
        {
            var body = ContentHelper.GetStringContent(null);
            var createResponse = await _httpClient.PostAsync(BaseUrl, body);

            Assert.Equal(HttpStatusCode.BadRequest, createResponse.StatusCode);
        }
    }
}

﻿using Newtonsoft.Json;

using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using TaskManager.Common.DTO;

using Xunit;

namespace TaskManager.API.IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _httpClient;
        private const string BaseUrl = "/api/users";

        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _httpClient = factory.CreateClient();
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }

        [Fact]
        public async Task Remove_WhenValidBody_ReturnsNoContent()
        {
            var user = new UserCreateDTO() { BirthDay = DateTime.Now, FirstName = "Amador", Email = "amador@g.c" };
            var createUserResponse = await _httpClient.PostAsync("/api/users", ContentHelper.GetStringContent(user));
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await createUserResponse.Content.ReadAsStringAsync());

            var removeUri = $"{BaseUrl}/{createdUser.Id}";
            var removeResponse = await _httpClient.DeleteAsync(removeUri);

            var uriWithId = $"{BaseUrl}/{createdUser.Id}";
            var getResponse = await _httpClient.GetAsync(uriWithId);

            Assert.Equal(HttpStatusCode.NoContent, removeResponse.StatusCode);
            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }

        [Fact]
        public async Task Remove_WhenNotExistingUser_ReturnsNotFound()
        {
            var removeUri = $"{BaseUrl}/1";
            var removeResponse = await _httpClient.DeleteAsync(removeUri);

            Assert.Equal(HttpStatusCode.NotFound, removeResponse.StatusCode);
        }

        [Theory]
        [InlineData("asdasd")]
        [InlineData("1000000000000000000000000000000000000")]
        public async Task Remove_WhenInvalidUriParameter_ReturnsNotFound(string uriParameter)
        {
            var removeUri = $"{BaseUrl}/{uriParameter}";
            var removeResponse = await _httpClient.DeleteAsync(removeUri);

            Assert.Equal(HttpStatusCode.NotFound, removeResponse.StatusCode);
        }
    }
}
